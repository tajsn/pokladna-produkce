<?php
@define('CLOUD_URL','http://www.manikova-pizza.cz');
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass('DashedRoute');
Router::extensions(['json', 'xml']);

Router::scope('/api/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Apis','action'=>'index']);
    //$routes->connect('/load/', ['controller' => 'Apis','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'Apis']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/api/users/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Users','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'Users']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/api/orders/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Orders','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'Orders']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/api/clients/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Clients','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'Clients']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/api/products/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Products','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'Products']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
   
});
Router::scope('/api/product-groups/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'ProductGroups','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'ProductGroups']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/api/deadlines/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Deadlines','action'=>'index','type_id'=>1]);
    $routes->connect('/:action/*', ['controller' => 'Deadlines']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/api/deadline-deliverys/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Deadlines','action'=>'index','type_id'=>2]);
    $routes->connect('/:action/*', ['controller' => 'Deadlines']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/api/deliverys/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Deliverys','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'Deliverys']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/api/stocks/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Stocks','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'Stocks']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/api/attendances/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Attendances','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'Attendances']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/api/schemas/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Schemas','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'Schemas']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/api/settings/', function (RouteBuilder $routes) { 
    $routes->connect('/', ['controller' => 'Settings','action'=>'index']);
    $routes->connect('/:action/*', ['controller' => 'Settings']);
    //$routes->connect('/json', ['controller' => 'Pages', 'action' => 'ng_layout','_ext'=>'json','method'=>'GET']);
    
});
Router::scope('/api/status/', function (RouteBuilder $routes) { 
    $routes->connect('/*', ['controller' => 'Pages','action'=>'changeStatus']); 
});
Router::scope('/api/hang/', function (RouteBuilder $routes) { 
    $routes->connect('/*', ['controller' => 'Orders','action'=>'sendClientHang']); 
});
Router::scope('/hang/', function (RouteBuilder $routes) { 
    $routes->connect('/*', ['controller' => 'Orders','action'=>'sendClientHang']); 
});
Router::scope('/call/', function (RouteBuilder $routes) { 
    $routes->connect('/*', ['controller' => 'Orders','action'=>'sendClientCall']); 
});
Router::scope('/end/', function (RouteBuilder $routes) { 
    $routes->connect('/*', ['controller' => 'Orders','action'=>'sendClientEnd']); 
});
Router::scope('/api/trash/', function (RouteBuilder $routes) { 
    $routes->connect('/*', ['controller' => 'Pages','action'=>'changeTrash']); 
});
Router::scope('/api/old/', function (RouteBuilder $routes) { 
    $routes->connect('/*', ['controller' => 'Pages','action'=>'oldImport']); 
});

Plugin::routes();
