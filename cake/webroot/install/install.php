<?php 
//die('a');
function pr($val){
        echo '<pre>';
        print_r($val);
        echo  '</pre>';
}

function checkDevVersion(){
    if($_SERVER['HTTP_HOST'] == 'pokladna.localhost'){
        die('neni povoleno na vyvojove verzi');
    }
    
}

class InstallPokladna {
    //var $configFile = '..'.DIRECTORY_SEPARATOR.'cake'.DIRECTORY_SEPARATOR.'webroot'.DIRECTORY_SEPARATOR.'configPokladna.php';
    var $configFile = '..'.DIRECTORY_SEPARATOR.'configPokladna.php';
    
    public function __construct() {
        if (!isset($_GET['method'])) $_GET['method'] = '';
        switch($_GET['method']){
            /**
             * check version and update via GIT PULL
             */
            case 'checkVersion' :
                $this->checkVersion();
            break;
            /**
             * GIT CLONE new repository
             */
            case 'newInstall':
                $this->newInstall();
            break;
        
            /**
             * nastaveni config file
             */
            case 'config':
                $this->setConfig();
            break;
        
			case 'setChmod':
                $this->setChmod();
            break;
           
            
            default:
        
        }
        echo '<style>
        body {
            font-family:arial;
            font-size:12px;
        }
        .alert {
            color:#fff;
            background:#ff0000;
            padding:5px;
            margin:10px auto;
            display:block;
            text-align:center;
        }
        label {
            display:inline-block;
            width:20%;
            margin:2px;
            padding:2px;
        }
        .sll,.slr {
            display:inline-block;
            vertical-align:top;
            width:49%;
        }
        input.text {
            width:50%;
            margin:2px;
            border:1px solid #ccc;
            padding:2px;
        }
        fieldset {
            border:1px solid #ccc;
        }
        .log {
            padding:5px;
            font-family:times;
            border:1px solid #ccc;
        }
        </style>';
        echo '<ul>';
            //echo'<li><a href="install.php?method=newInstall">Nová instalace z GIT (git clone)</li>';
            echo'<li><a href="/api/schemas/updateChmod/true">Nastavení chmod 777 na tmp a logs a uploaded</li>';
            echo'<li><a href="install.php?method=config">Vytvoření konfiguračního souboru</li>';
            echo'<li><a href="/api/schemas/createSql">Vytvoření struktury databaze</li>';
            echo'<li><a href="/api/schemas/setAutoincrement">Nastavení čísla objednávek</li>';
            echo'<li><a href="/cake/webroot/logo.png">Stáhnout logo</li>';
            //echo'<li><a href="install.php?method=checkVersion">Kontrola aktuálni verze z GIT (git pull)</li>';
            echo'<li><a href="/api/old/clients">Převod starých zákazníků</li>';
            echo'<li><a href="/api/old/orders">Převod starých objednávek</li>';
            echo'<li><a href="/api/old/users">Převod starých rozvozců</li>';
        echo '</ul>';
        
        
        
    }

    
   
	public function setChmod(){
		//chmod -R 777
		$script = "chmod -R 777";
        exec($script." 2>&1",$out);
        $msg = (isset($out[0])?$out[0]:'Chyba chmod ');
		die('end');
	}

	// prevod do jineho adresare po rozbaleni z git
	public function removeFolder(){
		$files = scandir("poklInstall");
		$oldfolder = "poklInstall/";
		$newfolder = "";
		  foreach($files as $fname) {
			  if($fname != '.' && $fname != '..') {
				  rename($oldfolder.$fname, $newfolder.$fname);
			  }
		  }
	}


    /**
     * check version and update
     */
    public function checkVersion(){
        if (file_exists($this->configFile)){
            require $this->configFile;
            $fileLoad = true;
        }
        checkDevVersion();
        
        $script = "git pull ".configGitRepository;
        exec($script." 2>&1",$out);
        $msg = (isset($out[0])?$out[0]:'Chyba check version ');
        
        if ($msg == 'Already up-to-date.'){
            $msg = 'Máte aktuální verzi';
        }
        
        echo '<strong class="alert">'.$msg.'</strong>';
        //echo $message;
    }

    // nova instalace
    public function newInstall(){
        if (!empty($_POST)){
            
            //exec("git clone https://tajsn@bitbucket.org/Fastest_cz/pokladna_angular4.git test");
            exec('git clone '.$_POST['git_rep_clone'] ."poklInstall 2>&1",$out);
            echo '<p class="log">'.$out[0].'</p>';
            $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
            $result = 'GIT LOADED '.round($time,2).'s';
            $this->removeFolder();
            echo '<strong class="alert">'.$result.'</strong>';
        } else {
            echo '<form method="post">';
            echo '<fieldset>';    
                echo '<legend>Nastavení</legend>';
                echo '<div class="sll">';
                    echo '<label>GIT repository: </label><input type="text" name="git_rep_clone" class="text" /><br />';
                echo '</div>';
                echo '<div class="slr">';
                echo '</div>';
            echo '</fieldset>';   
    
           
            echo '<input type="submit" value="Stáhnout z GIT" />';
        echo '</form>';
        }
    }

    // nastaveni config    
    public function setConfig(){
            
        if (!empty($_POST)){
            //print_r($_POST);
            $file_data = "<?php\n";
            $file_data .="define(\"configSystemId\",\"".$_POST['system_id']."\");\n";
            $file_data .="define(\"configSqlServer\",\"".$_POST['sql_server']."\");\n";
            $file_data .="define(\"configSqlUser\",\"".$_POST['sql_user']."\");\n";
            $file_data .="define(\"configSqlPass\",\"".$_POST['sql_pass']."\");\n";
            $file_data .="define(\"configSqlDb\",\"".$_POST['sql_db']."\");\n";
            $file_data .="define(\"configSqlUser2\",\"".$_POST['sql_user2']."\");\n";
            $file_data .="define(\"configSqlPass2\",\"".$_POST['sql_pass2']."\");\n";
            $file_data .="define(\"configSqlDb2\",\"".$_POST['sql_db2']."\");\n";
            $file_data .="define(\"configFtpServer\",\"".$_POST['ftp_server']."\");\n";
            $file_data .="define(\"configFtpUser\",\"".$_POST['ftp_user']."\");\n";
            $file_data .="define(\"configFtpPass\",\"".$_POST['ftp_pass']."\");\n";
            $file_data .="define(\"configFtpFolder\",\"".$_POST['system_id']."\");\n";
            $file_data .="define(\"configGitRepository\",\"".$_POST['git_repository']."\");\n";
            
            $file_data .= "?>";
            
            file_put_contents($this->configFile, $file_data);
            echo '<strong class="alert">Uloženo</strong>';
            } 
            //else {
                if (file_exists($this->configFile)){
                    require $this->configFile;
                    $fileLoad = true;
                }

                $operation_list = file_get_contents('http://intr.chachar.cz/api/getProvoz');
                $operation_list = json_decode($operation_list,true);
                //pr($provoz_list);

                echo '<form method="post">';
                    echo '<fieldset>';    
                        echo '<legend>Nastavení</legend>';
                        echo '<div class="sll">';
                            //echo '<label>Pokladna ID: </label><input type="text" name="system_id" class="text" value="'.(defined("configSystemId")?configSystemId:'').'" /><br />';
                            echo '<label>Pokladna ID: </label>';
                            $selected = (defined("configSystemId")?configSystemId:'');
                            echo '<select type="text" name="system_id" class="text" value="'.(defined("configSystemId")?configSystemId:'').'" >';
                                foreach($operation_list['data'] AS $operation_id=>$operation_name){
                                    echo '<option value="'.$operation_id.'" '.(($operation_id == $selected)?'selected':'').'>'.$operation_name.'</option>';
                                }
                            echo '</select>';
                            echo '<br />';
                            
                            //echo '<label>Uživatel: </label><input type="text" name="ftp_user" value="'.(defined("configFtpUser")?configFtpUser:'').'"/><br />';
                        echo '</div>';
                        echo '<div class="slr">';
                            echo '<label>GIT repozitař produkční: </label><input type="text" name="git_repository" class="text" value="'.(defined("configGitRepository")?configGitRepository:'').'"/><br />';
                            echo '<label></label>https://tajsn@bitbucket.org/tajsn/pokladna-produkce.git<br />';
                        echo '</div>';
                    echo '</fieldset>';   
            
                    echo '<fieldset>';    
                        echo '<legend>FTP</legend>';
                        echo '<div class="sll">';
                            echo '<label>Server: </label><input type="text" name="ftp_server"  class="text"value="'.(defined("configFtpServer")?configFtpServer:'109.123.216.75').'" /><br />';
                            echo '<label>Uživatel: </label><input type="text" name="ftp_user" class="text" value="'.(defined("configFtpUser")?configFtpUser:'fastestsolution_backchachar').'"/><br />';
                        echo '</div>';
                        echo '<div class="slr">';
                            echo '<label>Heslo: </label><input type="text" name="ftp_pass"  class="text" value="'.(defined("configFtpPass")?configFtpPass:'ppvKU!31').'"/><br />';
                            //echo '<label>Složka: </label><input type="text" name="ftp_folder"  class="text" value="'.(defined("configFtpFolder")?configFtpFolder:'').'"/><br />';
                            //echo '<label>Složka: </label>';
                            $selectedFolder = (defined("configSystemId")?configSystemId:'');
                            /*
                            echo '<select type="text" name="ftp_folder" class="text" value="'.(defined("configFtpFolder")?configFtpFolder:'').'" >';
                                foreach($operation_list['data'] AS $operation_id=>$operation_name){
                                    echo '<option value="'.$operation_id.'" '.(($operation_id == $selectedFolder)?'selected':'').'>'.$operation_name.'</option>';
                                }
                            echo '</select>';
                            */
                            //pr($operation_list['data']);
                        echo '</div>';
                    echo '</fieldset>';    
                    
                    echo '<fieldset>';    
                        echo '<legend>Mysql</legend>';
                        echo '<div class="sll">';
                            echo '<label>Server: </label><input type="text" name="sql_server"  class="text" value="'.(defined("configSqlServer")?configSqlServer:'localhost').'" /><br />';
                            echo '<label>Uživatel: </label><input type="text" name="sql_user"  class="text" value="'.(defined("configSqlUser")?configSqlUser:'pokladna').'"/><br />';
                        
                        echo '</div>';
                        echo '<div class="slr">';
                            echo '<label>Heslo: </label><input type="text" name="sql_pass" class="text" value="'.(defined("configSqlPass")?configSqlPass:'').'"/><br />';
                            echo '<label></label>chachar951<br />';
                            echo '<label>DB: </label><input type="text" name="sql_db"  class="text" value="'.(defined("configSqlDb")?configSqlDb:'').'"/><br />';
                            
                            echo '<label></label>pokladnaAng';        
                        echo '</div>';
                    echo '</fieldset>';    
                    echo '<fieldset>';    
                        echo '<legend>Mysql OLD</legend>';
                        echo '<div class="sll">';
                            echo '<label>Uživatel: </label><input type="text" name="sql_user2"  class="text" value="'.(defined("configSqlUser2")?configSqlUser2:'').'"/><br />';
                            echo '<label></label>pokladna';
                        echo '</div>';
                        echo '<div class="slr">';
                            echo '<label>Heslo: </label><input type="text" name="sql_pass2" class="text" value="'.(defined("configSqlPass2")?configSqlPass2:'').'"/><br />';
                            
                            echo '<label></label>chachar951<br />';
                            echo '<label>DB: </label><input type="text" name="sql_db2"  class="text" value="'.(defined("configSqlDb2")?configSqlDb2:'').'"/><br />';
                        
                            echo '<label></label>pokladna';        
                        echo '</div>';
                    echo '</fieldset>';    
                    echo '<input type="submit" value="Uložit konfiguraci" />';
                echo '</form>';
            
        
        }
    
}
$InstallPokladna = new InstallPokladna();

?>