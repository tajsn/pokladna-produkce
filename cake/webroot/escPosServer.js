
//var WebSocketServer = require('websocket').server;
//if (typeof window == 'undefined') window = {}

// maximalni delka textu 15 znaku
var http = require('http');
var Moo = require('mootools');
var os = require("os");
var fs = require("fs");
var node_printer = require("printer");
var stringTable = require("string-table");
var WebSocketServer = require('websocket').server;
var exec = require('child_process').exec,child;

var printer = require("printer");
var Printjob = require('../../assets/lib/printjob');
var Image = require('../../assets/lib/image');
var PNG = require('node-png').PNG;		
//var removeDiacritics = require('diacritics').remove;	


var FstPokladnaServer = new Class({
    printQueu : [],
    isPrinting : false,
	debug: false,
	domain: 'http://'+os.hostname(),
	//domain: '127.0.0.1',
	logo_file: 'logo_uctenka.png',
	
	Implements: [process.EventEmitter],
	
	ordersList : {}, 
	connectedDeliverys : {}, 
	socket_clients : {}, 
	connectionIDCounter : [],
	options: {
		table_header_row : 2, 
		
	},
	
	initialize: function(options){
		//this.create_server();
		//var printers_list = node_printer.getPrinters();
		//console.log(printers_list);
		this.create_websocket_server();
		
		
		//this.print_data();
		
	},
	/** PRICE FORMAT */
	price: function(price,params){
			if (!params){
			params = {
				'symbol_before':'',
				'kurz':'1',
				'count':'1',
				'decimal': 2,
				'symbol_after':',-'
			}
			}
			price = (price/params.kurz)* params.count;		
			return Number(price.toFixed(2)).toLocaleString();
			//return params.symbol_before + number_format(price, params.decimal, '.', ' ') + params.symbol_after;	
	},
	
	getDataFromJson: function(data){
		if (!data){
			/*
		this.json_data = {
		  'printer_name' :'PRP-301',
		  'is_logo' :true,
		  'header':[
				{
					'font':'B',
					'align':'center',
					'format':'bold',
					'data':'Ollies Cukrárna',
				},
				{
					'font':'A',
					'align':'center',
					'format':'',
					'data':' ',
				},
		  ],
		  'footer':[
				{
					
					'align':'left',
					'format':'normal',
					'data':'Paticka textu',
				},
		  ],
		  'table_data':[
			  {
				'format':'',
				'data':{ Produkt: 'Dort malinovy', Ks: '1', 'Cena': '1529.00','Celkem':'1529.00' },
			  },
			  {
				'format':'',  
				'data':{ Produkt: 'Dort červený', Ks: '2', 'Cena': '31.00' ,'Celkem':'62.00'},
			  },
			  {
				'format':'',  
				'data':{ Produkt: 'Zřčš cvjfdvhbfd', Ks: '1', 'Cena': '33.00','Celkem':'33.00' },
			  },
			  {
				'font':'B',  
				'format':'bold',  
				'data':{ Produkt: 'Celkem', Ks: '', 'Cena': '','Celkem':'1833.00' },
			  },
			],
			'table_align':{
				'Produkt':'left',
				'Cena':'right',
				'Celkem':'right',	
			}
		};
		*/
		
		} else {
			this.json_data = data;
		}
	},
	
	
	// naformatovani tabulky pro tisk z JSON
	create_bill_table: function(){
		if (typeof this.json_data == 'undefined'){
			return false;
		}
		
		var format_table = {
			//number: function(value, header) { return value.toFixed(2); }
		}
		
		// table align from JSON data
		if (typeof this.json_data.table_align != 'undefined'){
			var format_table_align = {};
			Object.each(this.json_data.table_align,function(align,item){
				format_table_align[item] = function(value, header) {
				  //console.log(this.EscPos.text(value));
					return {
					value: value,
					// value: this.EscPos.text(value),
					format: {
					  alignment: align
					}
				  };
				}.bind(this);
			}.bind(this));
		}
		//console.log('aaa');
		// var test  = {
		// 	"tst":true,
		// 	"tst2":1,
		// }
		//console.log(test);
		//console.log(this.json_data);
		//console.log('bbb');
		//return false
		// table data from JSON table_data
		if (typeof this.json_data.table_data != 'undefined'){
			table_data = [];
			this.json_data.table_data.each(function(item,row_id){
				table_data.push(item.data);
			});
		}
		//console.log(table_data);return false;
		
		// vytvoreni objektu tabulka
		table = stringTable.create(table_data, {
		  outerBorder: ' ',
		  innerBorder: ' ',
		  //headers: ['age', 'name'],
		  typeFormatters: format_table,
		  formatters: format_table_align,
		  
		});
		this.table = table;
		this.table_rows = this.table.split("\n");
		//console.log(this.table);return false;
		if (this.isDeadline){
			this.table_rows.splice(0,2);
		}
		//console.log('tables',this.table_rows);
		//console.log('tables2',this.json_data.table_data);
		//console.log(this.table_rows);return false;
		
		// formatovani radku a prevod na EscPos
		this.table_rows.each(function(row,i){	
			if (!this.isDeadline){
				i = i-this.options.table_header_row;
			}
			if (typeof this.json_data.table_data[i] != 'undefined'){
				//console.log('a',this.json_data.table_data[i]); 
				//console.log('row data format: ',i,row);
				this.format_esc(this.json_data.table_data[i]);			
				//this.EscPos.fontSize();
	
				//console.log('aa',i,this.printQueu);
				this.EscPos.text(row).newLine(1);
			} else {
				this.EscPos.setBold(false);

				//console.log('row data: ',i,row);
				this.EscPos.text(row).newLine(1);
			
			}
		}.bind(this));
	},
	
	// print data 
	print_data: function(data){
		//console.log('data',this.isPrinting,data)
		if (this.isPrinting){
			this.printQueu.push(data);
			//console.log('fronta',this.printQueu);
		} else {
			this.isPrinting = true;
		
			// pokud nejsou data
			this.getDataFromJson(data);
			//console.log(data);
			//console.log(this.json_data);return false;
			
			
			// stringBytesData
			this.stringBytesData();
			
			if (this.json_data.deadline){
				this.isDeadline = true;
			}

			//console.log('LOGO',this.json_data.is_logo);
			// pokud je logo nacti logo z localhost
			if (this.json_data.is_logo == true){
				this.print_logo();
			} else {
				this.create_template();
			}
		}
			
	},
	
	// nacti logo z localhost
	print_logo: function(){
		stream = fs.createReadStream(__dirname + '/uploaded/'+this.logo_file).pipe(new PNG({
			  filterType: 4
		})).on('parsed', function(logo) {
			this.create_template(stream); 	
		}.bind(this));
	},
	
	// prevod na bytove data
	stringBytesData: function(){
		String.prototype.toBytes = function() {
			var arr = [];
			for (var i=0; i < this.length; i++) {
				arr.push(this[i].charCodeAt(0))
			}
			return arr;
		}
		
	},
	
	// format esc
	format_esc: function(data){
			//console.log('bold',data); 
		// zarovnani
		if (typeof data.align != 'undefined'){
			this.EscPos.setTextAlignment(data.align);
		} else {
			this.EscPos.setTextAlignment('left');
			
		}
		
		
		// velikost fontu
		if (typeof data.font != 'undefined'){
			if (data.font == 'A'){
				this.EscPos.setFont('A');
			} else {
				this.EscPos.setFont('B');
			}
		} else {
			this.EscPos.setFont('A');
		}
		
		// tucny font
		if (typeof data.format != 'undefined'){
			if (data.format == 'bold'){
				this.EscPos.setBold(true);
			} else {
				this.EscPos.setBold(false);
			}
		} else {
			//this.EscPos.setBold(false);
		}
				
	},
	
	// print template 
	create_template: function(stream){
		this.EscPos = new Printjob();
		//console.log('SET ENCODE');
		this.EscPos.setEncode();
		
		// pokud je logo tiskni
		if (this.json_data.is_logo == true){
			this.EscPos.setTextAlignment('center').imageAdd(stream).newLine(2);
		}
		
		// print header
		if (typeof this.json_data.header != 'undefined'){
			this.json_data.header.each(function(header_row){
				this.format_esc(header_row);
				this.EscPos.text(header_row.data);
				this.EscPos.newLine(1);
		
			}.bind(this));
		}
		
		this.EscPos.newLine(1);
		//console.log('a');
		//this.EscPos.fontSize();
	
		// table content
		this.create_bill_table();
		//console.log(this.EscPos);
		this.EscPos.newLine(1);
		
		// print footer
		if (typeof this.json_data.footer != 'undefined'){
			this.json_data.footer.each(function(footer_row){
				this.format_esc(footer_row);
				this.EscPos.text(footer_row.data);
				this.EscPos.newLine(1);;
				
			}.bind(this));
		}
		
		this.sendToPrinter();
		
		
		
	},
	
	// send to printer
	sendToPrinter: function(){
		//console.log('a');
		// pridani 6 radku na konec a orez
		this.EscPos.newLine(6).cut();
		
		var print_data_buffer = this.EscPos.printData();
		
		if (this.debug){
			console.log('Tiskarna: ',this.json_data.printer_name);
			
			console.log('EscPos data: ',this.EscPos);
			console.log('Buffer print: ',print_data_buffer);
			return false;
			
		} else {
			//console.log('EscPos data: ',this.EscPos);
			// console.log('Buffer print: ',print_data_buffer);
	
		}
		
		
		printer.printDirect({
			data: print_data_buffer
			//, printer:'PRP-300'
			, printer:this.json_data.printer_name
			, type: 'RAW'
			, success:function(jobID){
				this.isPrinting = false;
				this.sendQueu();
				console.log("poslano na tiskarnu "+this.json_data.printer_name+" ID: "+jobID);
			}.bind(this)
			, error:function(err){console.log(err);}
		});	
		return false;
	},

	sendQueu: function(){
		//console.log('fronta tisk',this.printQueu.length+'ks');
		if (this.printQueu.length > 0){
			//let this.printQueu.splice(0, 1);
			this.print_data(this.printQueu.shift());
			
		}
		//console.log('fronta tisk',this.printQueu.length+'ks',this.isPrinting);
		//print_data
	},
	
	
	
	// receive data from client
	socket_receive: function(connection,message){
		if (message.type === 'utf8') {
			//console.log('Received Message: ' + message.utf8Data);
			message = message.utf8Data;
			if (message){
				var mdata = JSON.parse(message);
				
				if (mdata.type == 'getOrders' || mdata.type == 'sendOrders'){
					//console.log(mdata.type);
				} else {
					//console.log(mdata);
				}		

				// pokud je tisk
				if (mdata.type == 'print_data'){
					this.print_data(mdata.json_data);
				}
				
				// get Orders
				if (mdata.type == 'getOrders'){
					//console.log('CON ID',connection.id);
					this.getOrders(mdata.system_id,connection.id,mdata.connectedDeliverys);
				}
				
				// send Orders
				if (mdata.type == 'sendOrders'){
                    this.sendOrders(mdata.system_id,mdata.ordersList,mdata.connectedDeliverys,connection.id);
				}
				
				
				// get printer list
				if (mdata.type == 'get_printer'){
                    //console.log('GET PRINTER');
                    this.get_printer(mdata.system_id);
				}
				
				// hang
				if (mdata.type == 'hang'){
                    
                    this.hangPhone(mdata.data);
				}
				
				// init connection list
				if (mdata.type == 'init'){
					system_id = mdata.system_id;
							
					if (!this.connectionIDCounter[system_id]) this.connectionIDCounter[system_id] = 0;
					if (!this.connections[system_id]) {
						this.connections[system_id] = {
							'system_id':system_id,
							'id':null,
							'pokladna':{}
						}
					}
					connection.id = this.connectionIDCounter[system_id] ++;
					this.connections[system_id]['pokladna'][connection.id] = connection;
					//console.log('connections',this.connections[system_id]['pokladna'].l);
					//console.log('id',connection.id);
					
					this.create_socket_client(system_id,connection.id,this.connections);
					//if (this.socket_clients[system_id])
					//console.log('count websocket clients system ID'+system_id+': '+Object.keys(this.socket_clients[system_id]).length+' ks');
							
				}
						
			} 
		} else if (message.type === 'binary') {
			//console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
			//connection.sendBytes(message.binaryData);
		}
	},

	hangPhone: function(phone){
		//console.log('phone',phone);
		system_id = 1;
		if (system_id) {
			Object.each(this.connections[system_id]['pokladna'],(function(cc,k){
                //console.log(this.connections[system_id]['pokladna'][k]); 
                this.connections[system_id]['pokladna'][k].send(JSON.stringify({'phone':phone,'type':'call'}));
			}).bind(this));
		}
	},
	
	
	// create socket client before send to CLIENT.JS
	create_socket_client: function(system_id,connection_id,connections){
		connections = connections[system_id];
		system_id = system_id.toInt();
		//console.log('aaa',this.connections);
		// if (!this.socket_clients[system_id]) this.socket_clients[system_id] = {};
		if (!this.socket_clients[system_id]) this.socket_clients[system_id] = [];
		
		// this.socket_clients[system_id] = {
		// 	connection_id : {
		// 		'id':connection_id
		// 	}
		
		// };
		this.socket_clients[system_id].push({
			connection_id : {
				'id':connection_id
			}
		
		});
		//console.log(this.socket_clients);
		if (system_id) {
			Object.each(this.connections[system_id]['pokladna'],(function(cc,k){
                //console.log(this.connections[system_id]['pokladna'][k]);
                this.connections[system_id]['pokladna'][k].send(JSON.stringify({'type':'connectedResult','connected':true,'countUser':Object.keys(this.socket_clients[system_id]).length}));
			}).bind(this));
		}
		
		
		
	},
	
	// get order list
	getOrders: function(system_id,con_id,connectedDeliverys){
		//console.log('orders list');
		if (connectedDeliverys){
			this.connectedDeliverys = connectedDeliverys;
		
		}
		//console.log(this.connectedDeliverys);
		
		if (system_id) {
			if (typeof this.connections[system_id] != 'undefined' && typeof this.connections[system_id]['pokladna'] != 'undefined')
			Object.each(this.connections[system_id]['pokladna'],(function(cc,k){
                //console.log(this.connections[system_id]['pokladna'][k]);
				if (con_id == k)
				this.connections[system_id]['pokladna'][k].send(JSON.stringify({'type':'ordersList','ordersList':this.ordersList,'connectedDeliverys':this.connectedDeliverys}));
			}).bind(this));
		}
		//console.log(this.ordersList);
	},
	
	// send order list
	sendOrders: function(system_id,ordersList,connectedDeliverys,con_id){
		this.ordersList = ordersList;
		this.connectedDeliverys = connectedDeliverys;
		//console.log('send orders list');
		if (system_id) {
			//console.log(this.connections);
			Object.each(this.connections[system_id]['pokladna'],(function(cc,k){
                //console.log(this.connections[system_id]['pokladna'][k]);
				if (con_id != k)
				this.connections[system_id]['pokladna'][k].send(JSON.stringify({'type':'ordersList','ordersList':this.ordersList,'connectedDeliverys':this.connectedDeliverys}));
			}).bind(this));
		}
		//console.log(this.ordersList);
	},


	// get printer list from PC
	get_printer: function(system_id){
		
		var printers_list = node_printer.getPrinters();
		var printers_name = [];
		
		printers_list.each(function(printer){
			printers_name.push(printer.name);
		}.bind(this));
		
		this.printers_name = printers_name;
		if (system_id) {
			Object.each(this.connections[system_id]['pokladna'],(function(cc,k){
                //console.log(this.connections[system_id]['pokladna'][k]);
                this.connections[system_id]['pokladna'][k].send(JSON.stringify({'type':'printerList','printer_list':printers_name}));
			}).bind(this));
		}
		
	},
	
	
	// create websocket server
	create_websocket_server: function(){
		var server = http.createServer(function(request, response) {
			console.log((new Date()) + ' Received request for ' + request.url);
			response.writeHead(404); 
			response.end();
		});
		var hostname = fs;
				
		const interfaces = require('os').networkInterfaces();

		const addresses = Object.keys(interfaces)
		.reduce((results, name) => results.concat(interfaces[name]), [])
		.filter((iface) => iface.family === 'IPv4' && !iface.internal)
		.map((iface) => iface.address);
		console.log('address IP',addresses);


		if (addresses[0] != '109.123.216.45'){
            var port = 8080;
        } else {
            var port = 8899;
        }
		server.listen(port, function() {
			console.log((new Date()) + ' Server is domain '+this.domain);
			console.log((new Date()) + ' Server is listening on port '+port);
		}.bind(this));

		wsServer = new WebSocketServer({ 
			httpServer: server,
			maxReceivedFrameSize: 0x100000000,
			maxReceivedMessageSize: 0x100000000,
			fragmentOutgoingMessages: true,
			fragmentationThreshold: 0x4000,
			 
		});

		
		function originIsAllowed(origin) {
		  // put logic here to detect whether the specified origin is allowed.
		  return true;
		}

		this.connections = {};
		
		
		var _this = this;
		
		wsServer.on('request', (function(request) {
			
			if (!originIsAllowed(request.origin)) {
			  // Make sure we only accept requests from an allowed origin
			  request.reject();
			  console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
			  return;
			}
			
			var connection = request.accept(null, request.origin);

			connection.on('message', (function(message) {
				//console.log(message);
				this.socket_receive(connection,message);
			}).bind(this));
			
			
			
			connection.on('close', (function(reasonCode, description) {
				console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected. ' + "Connection ID: " + connection.id);

				// Make sure to remove closed connections from the global pool
					if (system_id && this.connections[system_id] && this.socket_clients[system_id][connection.id]){
					
						delete this.connections[system_id]['pokladna'][connection.id];
						delete (this.socket_clients[system_id][connection.id]);
						
						//this.update_pocet_orders(connections,system_id,count_orders);
						
						console.log('pocet klientu pokladny '+system_id+': '+Object.keys(this.socket_clients[system_id]).length);
						if (system_id) {
							Object.each(this.connections[system_id]['pokladna'],(function(cc,k){
								//console.log(this.connections[system_id]['pokladna'][k]);
								this.connections[system_id]['pokladna'][k].send(JSON.stringify({'type':'connectedResult','connected':true,'countUser':Object.keys(this.socket_clients[system_id]).length}));
							}).bind(this));
						}
					
					}
					
				
				
			}).bind(this));
			
		}).bind(this));
	},
	
	// create server
	create_server: function(){
				
		var http = require("http");
		var server = http.createServer(function(request, response) {
		  response.writeHead(200, {"Content-Type": "text/html"});
		  response.write("<!DOCTYPE \"html\">");
		  response.write("<html>");
		  response.write("<head>");
		  response.write("</head>");
		  response.write("<body>");
		  response.write("</body>");
		  response.write("</html>");
		  response.end();
		});

		server.listen(80);
		console.log("Server is listening");
	}
	
	
});
var pokladna_server = new FstPokladnaServer();	

