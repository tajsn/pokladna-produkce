<div class="login-box row">
    <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
        <div class="login-logo">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 img-responsive">
                    <img src="/css/public/fastest-logo.png" alt="logo" />
                </div>
                <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
                      <div class="row">
                          <div class="col-lg-12"> &nbsp;</div>
                      </div>
                      <div class="row">
                          <div class="col-lg-12">Fastest <b>admin</b></div>
                      </div>
                </div>
            </div>
        </div>
        <div class="login-body">
            <?= $this->Form->create(); ?>
                <div class="row">
                    <div class="col-lg-12"> 
                        <?php if($fMessage = $this->Flash->render('auth')): ?>
                            <div class="alert alert-warning fade in alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                <strong><?= $fMessage ?></strong>
                            </div>
                        <?php endif; ?>
                        
                        <?php if($superadmin): ?>
                                <div class="form-group">
                                    <?= $this->Form->input('domain_id', ['label' => false, 'empty' => 'Doména', 'class' => 'form-control', 'type' => 'select', 'options'=> $domain_list]); ?>
                                </div>
                        <?php else: ?>
                            <div class="form-group">
                                <div class="form-control">
                                    <?php echo $domain; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        
                        <div class="form-group has-feedback">
                            <?= $this->Form->input('username', ['label' => false, 'placeholder' => 'Uživatel', 'class' => 'form-control']); ?>
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group has-feedback">
                            <?= $this->Form->input('password', ['label' => false, 'placeholder' => 'Heslo', 'class' => 'form-control', 'type' => 'password']); ?>
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <?= $this->Form->button('<span class="fa fa-chevron-right"></span> Přihlásit', ['class' => 'btn btn-success btn-block btn-flat', 'escape'=>false]); ?>
                    </div>
                </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
