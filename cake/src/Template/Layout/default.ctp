<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>   
        <?= ( $this->fetch('title') != '' ? $this->fetch('title') . ' ' : '' ); ?>
        <?= $titleBrand; ?>
    </title>
   <?php // $this->Html->meta('icon') ?>

    <meta name = "description" content="<?= $this->fetch('meta-description') ?>" />
    <meta name = "keywords" content="<?= $this->fetch('meta-keywords') ?>" />

    <?= $this->Html->css('../libs/bootstrap/css/bootstrap.min.css') ?>
    <?= $this->Html->css('../libs/bootstrap/css/bootstrap-theme.css') ?>
    
    <?= $this->element('Layout/LTE/head'); ?>
    
    <?= $this->fetch('css') ?>
</head>
<body class="hold-transition login-page">
    
    <?= $this->Flash->render() ?>
    <div class="container clearfix m-t-50">
        <?= $this->fetch('content') ?>
    </div>

    <?= $this->fetch('script') ?>
</body>
</html>
