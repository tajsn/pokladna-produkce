<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>   
        <?= ( isset($title) && $title != '' ? $title .' - ': '' ); ?> Fastest admin
    </title>
    <?php // $this->Html->meta('icon') ?>

    <meta name = "description" content="<?= $this->fetch('meta-description') ?>" />
    <meta name = "keywords" content="<?= $this->fetch('meta-keywords') ?>" />

    <?= $this->Html->css('../js/bootstrap/css/bootstrap.min.css') ?>
    <?= $this->Html->css('font-awesome.css') ?>
    <?= $this->Html->css('public-theme.css', ['rel'=>"stylesheet/less", 'media'=>"screen"]) ?>
    <?= $this->Html->script('less.js') ?>
    
    <?= $this->fetch('css') ?>
</head>
<body>
    <div class="container">
        <?= $this->fetch('content') ?>
    </div>

    <footer class="footer">
         <div class="container">
            <div class="text-center">
                <a href="http://www.fastest.cz">© 2008-<?= date('Y'); ?> Fastest Solution s.r.o.</a>
            </div>
         </div>
    </footer>
    
    <?= $this->Html->script('jquery/jquery.min.js'); ?>
    <?= $this->Html->script('bootstrap/js/bootstrap.js'); ?>
</body>
</html>
