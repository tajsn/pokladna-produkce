<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\View\Helper\HtmlHelper;

class FastestHelper extends Helper {

    public $helpers = ['Html'];
    public $tableth = "";
    private $cname = [];

    function langToUrl($url, $lang){
        return ($lang != 'cz' ? '/'.$lang : '') . $url;
    }
    
    // generate menu from threaded
    function pageTitle($breadcrumb) {
        $out = implode($breadcrumb, ' | ');
        return $out;
    }

    function breadcrumb($data, $path) {
        $out = '<ul id="breadcrumb">';
        $count = count($data);
        $i = 1;
        foreach ($data AS $k => $d) {
            if ($i == $count)
                $out .= '<li>' . $d . '</li>';
            else
                $out .= '<li>' . $this->Html->link($d . ' ', $path[$k], ['title' => $d]) . '</li>';
            $i++;
        }
        $out .= '</ul>';
        return $out;
        //pr($out);
        //pr($path);
    }

    // generate menu from threaded
    function generateMenu($arr, $opt = null) {
        //pr($this->request['here']);
        if ($opt != null) {

            $options = [
                'class' => 'nav',
                'prefix' => '',
                'submenu_pref' => 'submenu',
                'submenu_class' => '',
                //  'submenu_class' => 'collapse', 
                'href_class' => '',
                'id' => '',
            ];
            $this->options = array_merge($options, $opt);
        }
        if (!isset($this->options['prefix_default'])) {
            $this->options['prefix_default'] = $this->options['prefix'];
        }

        //pr($this->options);
        //pr($arr);
        $out = '<ul class="' . $this->options['class'] . '" id="' . $this->options['id'] . '" aria-labelledby="btn-' . $this->options['id'] . '">';
        foreach ($arr as $k => $val) {
            if (!empty($val->name)) {

                //pr($this->options);
                if (isset($this->options['path']) && strpos($this->options['path'][$val->id], 'http://') !== false) {
                    $alias = $this->options['path'][$val->id];
                } else {
                    $alias = $this->options['prefix_default'] . $this->options['path'][$val->id];
                }
                $current = ltrim($this->request->here, '/');

                if ($alias == $current || ltrim($alias, '/') == $current || rtrim($alias, '/') == $current) {
                    $active = 'active';
                } else {
                    $active = '';
                }

                if (isset($this->options['open_cat_id']) && $this->options['open_cat_id'] > 0) {
                    if (!empty($this->options['open_cat_id']) && $this->options['open_cat_id'] == $val->id) {
                        $active = 'active';
                    } else {
                        $active = '';
                    }
                }

                if (!empty($val->children)) {
                    $out .= "<li>";

                    if (!isset($this->options['sitemap']) && isset($this->options['enable_arrow'])) {
                        $out .= '<i class="fa fa-caret-down" ' . (!isset($opt['bootstrap_js']) || $opt['bootstrap_js'] !== false ? 'data-toggle="' . $this->options['submenu_class'] . '" data-target="#' . $this->options['submenu_pref'] . $val->id . '" aria-expanded="false"' : '') . '></i>';
                    }



                    //pr('alias '.$alias);
                    //pr('current '.$current);


                    $href_opt = [
                        'title' => $val->name,
                        'data-toggle' => $this->options['submenu_class'],
                        'aria-expanded' => 'false',
                        'id' => 'btn-' . $val->id,
                        'escape' => false,
                        'class' => $active . ' ' . $this->options['href_class'],
                        'aria-haspopup' => "true",
                        'aria-expanded' => "false"
                    ];

                    if (isset($this->options['data-target']))
                        $href_opt['data-target'] = '#' . $this->options['submenu_pref'] . $val->id;

                    if (isset($opt['bootstrap_js']) && $opt['bootstrap_js'] == false) {
                        unset($href_opt['data-toggle']);
                        unset($href_opt['expanded']);
                        unset($href_opt['aria-haspopup']);
                        unset($href_opt['aria-expanded']);
                        unset($href_opt['data-target']);
                    }

                    $out .= $this->Html->link($val->name . ' ', $alias, $href_opt);
                    if (!isset($this->options['sitemap'])) {
                        //pr($this->options);
                        $this->options = array_merge($this->options, [
                            'id' => $this->options['submenu_pref'] . $val->id,
                            'class' => $this->options['class'] . ' ' . $this->options['submenu_class'] . '-menu ' . $this->options['submenu_class'],
                        ]);
                        //$this->options['class'] = $this->options['submenu_class'].'-menu '.$this->options['submenu_class'];
                    } else {
                        $this->options = array_merge($this->options, [
                            'class' => '',
                        ]);
                    }

                    //pr($this->options);
                    $out .= $this->generateMenu($val->children);

                    $out .= "</li>";
                } else {
                    //pr($options['prefix']);
                    $out .= "<li>" . $this->Html->link($val->name, $alias, ['title' => $val->name, 'class' => $active . ' ' . $this->options['href_class'],]) . "</li>";
                }
            }
        }
        $out .= "</ul>";
        return $out;
    }

    /* generate image from params with resize */

    function img($img_set) {
        // path noimg
        $noimg_path = '/css/layout/noimg.png';
        $noimg = false;
        $img_link = '/img/';
        
        if (!isset($img_set['nosize'])) {
            $img_set['nosize'] = false;
        }
        if (isset($img_set['imgs'])) {
            $imgs = json_decode($img_set['imgs'], true);
        }
        
        if (!isset($imgs) || !is_array($imgs)) {
            $imgs = [
                0 => ['file' => '#']
            ];
        }
        
        if (isset($img_set['first'])) {
            if (is_array($imgs))
                foreach ($imgs AS $key => $img) {
                    if ($key > 0) {
                        unset($imgs[$key]);
                    }
                }
        }
        
        // ostatni fotky pro shop
        if (isset($img_set['other_imgs'])) {
            $other_imgs = array();
            foreach ($imgs AS $key => $img) {
                if ($key > 0) {
                    $other_imgs[] = $img;
                    unset($imgs[$key]);
                }
            }
        }
        
        foreach ($imgs AS $img) {
            $img_title = ((isset($img['title']) && !empty($img['title'])) ? $img['title'] : (isset($img_set['title']) && !empty($img_set['title'])) ? $img_set['title'] : 'Foto');
            $img_params = [];

            if (isset($img_set['class']))
                $img_class = $img_set['class'];
            if (isset($img_set['bg']))
                $img_params['bg'] = $img_set['bg'];
            if (isset($img_set['width']))
                $img_params['w'] = $img_set['width'];
            if (isset($img_set['height']))
                $img_params['h'] = $img_set['height'];
            
            if (isset($img_set['path'])) {
                $img['file'] = $img_set['path'] . $img['file'];
            }
            
            if (strpos($img['file'], '://') !== false) {
                $img_params['file'] = $img['file'];
                if (!$this->url_exists($img_params['file'])) {
                    $noimg = true;
                    $img_params['file'] = '.' . $noimg_path;
                }
            } else {
                $img_params['file'] = '.' . $img['file'];
                if (!file_exists($img_params['file'])) {
                    $noimg = true;
                    $img_params['file'] = '.' . $noimg_path;
                }
            }
            $img_params_other['w'] = (isset($img_set['width']) ? $img_set['width'] : 120);
            $img_params_other['h'] = (isset($img_set['height']) ? $img_set['height'] : 120);
            $img_code = $img_link . '?params=' . $this->encode_long_url($img_params);
            
            if (isset($img_set['only_url'])) {
                return $img_code;
            }
            if (isset($img_set['zoom2'])) {
                $img_code_zoom = ltrim($img_params['file'], '.');
            }

            $out = '';
            if (isset($img_set['other_imgs'])) {
                $out .= '<div class="solo_image" id="main_image">';
                if (!isset($img_set['disable_arrows']) || $img_set['disable_arrows'] == false) {
                    $out .= '<div class="img_arrow arrow-right"></div>';
                    $out .= '<div class="img_arrow arrow-left"></div>';
                }
            }
           
            if (isset($img_set['zoom'])) {
                if ($noimg == false) {
                    $image = $this->Html->link('<div data-zoom="' . (isset($img_code_zoom) ? $img_code_zoom : '') . '" '
                                                  . 'data-original="' . $img_code . '" '
                                                  . 'class="zoom_foto img zoom2 lazy ' . (isset($img_class) ? $img_class : '') . ' ' . (isset($img_set['zoom2']) ? 'zoom2' : '') . '">'
                                             . '</div>', 
                                            $img_params['file'], 
                                            array('title' => $img_title, 'class' => 'swipebox','escape' => false)
                             ); 
                } else {
                    $image = '<div data-original="' . $img_code . '" '
                                 . 'class="img lazy ' . (isset($img_class) ? $img_class : '') . '">'
                           . '</div> ';
                }

               // link to url
            } elseif (isset($img_set['link'])) {
                $image = $this->Html->link('<div data-original="' . $img_code . '" class="zoom_foto lazy img ' . (isset($img_class) ? $img_class : '') . '"></div>', $img_set['link'], array('title' => $img_title, 'escape' => false));
                // only img
            } else {
                $image = '<div data-original="' . $img_code . '" class="img lazy ' . (isset($img_class) ? $img_class : '') . '"></div> ';
            }

            $out .= $image;

            if (isset($img_set['other_imgs'])) {
                $out .= '</div>';
            }
          
            if (isset($img_set['imgs_variants']) && !empty($img_set['imgs_variants'])) {
                if(!is_array($img_set['imgs_variants'])){
                    $img_set['imgs_variants'] = json_decode($img_set['imgs_variants'], true);
                }
                $other_imgs = $img_set['imgs_variants'];
            }
          //  pr($other_imgs);
            if (isset($other_imgs) && count($other_imgs) > 0 && (!isset($img_set['var_id']) || $img_set['var_id'] == 0)) {
                $out .= '<div class="other_imgs" id="other_imgs">';
                foreach ($other_imgs AS $imgKey => $img2) {
                    $img_link = '/img/';
                    $img_params = [];
                    if (isset($img_set['path'])) {
                        $img2['file'] = $img_set['path'] . $img2['file'];
                    }

                    $other_img_title = ((isset($img2['title']) && !empty($img2['title'])) ? $img2['title'] : 'Varianta '.($imgKey + 1));

                    if (strpos($img2['file'], '://') !== false) {
                        $img2['file'] = $img2['file'];
                        if (!$this->url_exists($img2['file'])) {
                            $noimg = true;
                            $img2['file'] = $noimg_path;
                        }
                    } else {
                        $img2['file'] = '.' . $img2['file'];
                        if (!file_exists($img2['file'])) {
                            $noimg = true;
                            $img2['file'] = $noimg_path;
                        }
                    }
                    $img_params['file'] = $img2['file'];

                    $img_params['bg'] = $img_set['bg'];
                    $img_params['w'] = $img_params_other['w'];
                    $img_params['h'] = $img_params_other['h'];

                    $img_code = $img_link . '?params=' . $this->encode_long_url($img_params);
                    $out .= $this->Html->link('<div data-original="' . $img_code . '" class=" lazy img zoom_foto ' . (isset($img_class) ? $img_class : '') . '"></div>','#'.$imgKey , array('title' => $other_img_title, 'class' => 'link img_other imgOther' . $imgKey . '', 'escape' => false));
                    //$out .= $this->Html->link('<div data-original="' . $img_code . '" class=" lazy img zoom_foto ' . (isset($img_class) ? $img_class : '') . '"></div>', $img_params['file'], array('title' => $img_title, 'data-rel' => 'lightbox-atomium ', 'class' => 'link img_other imgOther' . $imgKey . '', 'escape' => false));
                }
                $out .= '</div>';
            }
        }
        echo $out;
    }

    function img_slider($img_set) {
        // path noimg
        $noimg_path = '/css/layout/noimg.png';

        if (!isset($img_set['nosize'])) {
            $img_set['nosize'] = false;
        }

        $noimg = false;
        if (isset($img_set['imgs'])) {
            $imgs = json_decode($img_set['imgs'], true);
        }
        //pr($imgs);
        if (isset($img_set['first'])) {
            if (is_array($imgs))
                foreach ($imgs AS $key => $img) {
                    //if ($key > 0){
                    //unset($imgs[$key]);
                    //}
                }
        }

        // ostatni fotky pro shop
        if (isset($img_set['other_imgs'])) {
            $other_imgs = array();
            foreach ($imgs AS $key => $img) {
                //if ($key > 0){
                $other_imgs[] = $img;
                //unset($imgs[$key]);
                //}
            }
        }
        //pr($other_imgs);
        if (!is_array($imgs)) {
            $imgs = [
                0 => ['file' => '#']
            ];
        }
        //pr($imgs);
        foreach ($imgs AS $img) {

            //pr($img);
            $img_title = ((isset($img['title']) && !empty($img['title'])) ? $img['title'] : (isset($img_set['title']) && !empty($img_set['title'])) ? $img_set['title'] : 'Foto');

            $img_link = '/img/';

            $img_params = [];
            //pr($img_set);
            if (isset($img_set['path'])) {
                $img['file'] = $img_set['path'] . $img['file'];
            }
            //pr($img_set);
            //pr($img['file']);
            if (strpos($img['file'], '://') !== false) {
                $img_params['file'] = $img['file'];
                if (!$this->url_exists($img_params['file'])) {
                    $noimg = true;
                    $img_params['file'] = '.' . $noimg_path;
                }
            } else {
                $img_params['file'] = '.' . $img['file'];
                if (!file_exists($img_params['file'])) {
                    $noimg = true;
                    $img_params['file'] = '.' . $noimg_path;
                }
            }
            //pr($img_params);pr($noimg);die();
            if (isset($img_set['class']))
                $img_class = $img_set['class'];
            if (isset($img_set['bg']))
                $img_params['bg'] = $img_set['bg'];
            if (isset($img_set['width']))
                $img_params['w'] = $img_set['width'];
            if (isset($img_set['height']))
                $img_params['h'] = $img_set['height'];


            $img_params_other['w'] = (isset($img_set['width_other']) ? $img_set['width_other'] : 120);
            $img_params_other['h'] = (isset($img_set['height_other']) ? $img_set['height_other'] : 120);

            $img_code = $img_link . '?params=' . $this->encode_long_url($img_params);

            $out = '';
            if (isset($img_set['other_imgs'])) {
                //$out .= '<div class="solo_image" id="main_image">';
            }
            if (isset($img_set['only_url'])) {
                $out = $img_code;
                return $out;
            }
            // zoom foto
            if (isset($img_set['zoom'])) {
                if ($noimg == false) {
                    $image = $this->Html->link('<div data-original="' . $img_code . '" class="zoom_foto img lazy ' . (isset($img_class) ? $img_class : '') . '"></div>', $img_params['file'], array('title' => $img_title, 'data-rel' => 'lightbox-atomium', 'escape' => false));
                    //$image = $this->Html->link($this->Html->image('#',array('data-original'=>$img_code,'data-width'=>$img_set['width'],'data-height'=>$img_set['height'],'data-nosize'=>$img_set['nosize'],'title'=>$img_title,'alt'=>$img_title,'class'=>'zoom_foto lazy '.(isset($img_class)?$img_class:'')),null,false),$img_params['file'],array('title'=>$img_title,'data-rel'=>'lightbox-atomium','escape'=>false));
                } else {
                    $image = '<div data-original="' . $img_code . '" class="img lazy ' . (isset($img_class) ? $img_class : '') . '"></div> ';
                    //$image = $this->Html->image('#',array('data-original'=>$img_code,'data-width'=>$img_set['width'],'data-height'=>$img_set['height'],'data-nosize'=>$img_set['nosize'],'title'=>$img_title,'alt'=>$img_title,'class'=>'lazy '.(isset($img_class)?$img_class:'')),null,false);
                }

                // link to url
            } elseif (isset($img_set['link'])) {
                //pr($img_set['link']);die();
                //$image = $this->Html->link($this->Html->image('#',array('title'=>$img_title,'data-original'=>$img_code,'data-width'=>$img_set['width'],'data-height'=>$img_set['height'],'data-nosize'=>$img_set['nosize'],'title'=>$img_title,'alt'=>$img_title,'class'=>'zoom_foto lazy '.(isset($img_class)?$img_class:'')),null,false),$img_set['link'],array('title'=>$img_title,'escape'=>false));
                $image = $this->Html->link('<div data-original="' . $img_code . '" class="zoom_foto lazy img ' . (isset($img_class) ? $img_class : '') . '"></div>', $img_set['link'], array('title' => $img_title, 'escape' => false));

                // only img
            } else {
                $image = '<div data-original="' . $img_code . '" class="img lazy ' . (isset($img_class) ? $img_class : '') . '"></div> ';

                //$image = $this->Html->image('#',array('data-original'=>$img_code,'data-width'=>$img_set['width'],'data-height'=>$img_set['height'],'data-nosize'=>$img_set['nosize'],'title'=>$img_title,'alt'=>$img_title,'class'=>'lightbox-atomium  lazy '.(isset($img_class)?$img_class:'')),null,false);
            }

            $out .= $image;

            if (isset($img_set['other_imgs'])) {
                //$out .= '</div>';
            }
            if (isset($other_imgs) && count($other_imgs) > 0) {
                $out .= '<div class="slide-imgs" class="detail_imgs"><div class="img_wrap">';
                foreach ($other_imgs AS $i => $img2) {
                    $img_link = '/img/';
                    $img_params = [];
                    
                    $other_img_title = ((isset($img2['title']) && !empty($img2['title'])) ? $img2['title'] : 'Foto '.($i + 1));

                    if (isset($img_set['path'])) {
                        $img2['file'] = $img_set['path'] . $img2['file'];
                    }

                    if (strpos($img2['file'], '://') !== false) {
                        $img2['file'] = $img2['file'];
                        if (!$this->url_exists($img2['file'])) {
                            $noimg = true;
                            $img2['file'] = $noimg_path;
                        }
                    } else {
                        $img2['file'] = '.' . $img2['file'];
                        if (!file_exists($img2['file'])) {
                            $noimg = true;
                            $img2['file'] = $noimg_path;
                        }
                    }
                    $img_params['file'] = $img2['file'];

                    $img_params['bg'] = $img_set['bg'];
                    $img_params['w'] = $img_params_other['w'];
                    $img_params['h'] = $img_params_other['h'];

                    $img_code = $img_link . '?params=' . $this->encode_long_url($img_params);
                    $out .= $this->Html->link('<div data-original="' . $img_code . '" class="reference lazy img zoom_foto ' . (isset($img_class) ? $img_class : '') . '"></div>', $img_params['file'], array('title' => $other_img_title, 'data-rel' => 'lightbox-atomium', 'class' => 'link', 'escape' => false));
                }
                $out .= '</div></div>';
            }
        }
        echo $out;
    }

    /*     * * overeni zda url je dostupne * */

    function url_exists($url) {
        // Version 4.x supported
        $handle = curl_init($url);
        if (false === $handle) {
            return false;
        }
        curl_setopt($handle, CURLOPT_HEADER, false);
        curl_setopt($handle, CURLOPT_FAILONERROR, true);  // this works
        curl_setopt($handle, CURLOPT_HTTPHEADER, Array("User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.15) Gecko/20080623 Firefox/2.0.0.15")); // request as if Firefox   
        curl_setopt($handle, CURLOPT_NOBODY, true);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, false);
        $connectable = curl_exec($handle);
        curl_close($handle);
        return $connectable;
    }

    // gen seo text
    function seoText($seo_data, $setting, $type) {
        $result = '';

        //die('a');
        switch ($type) {
            case 'title': $result = $this->orez((isset($seo_data[$type]) && !empty($seo_data[$type]) ? $seo_data[$type] . ' | ' . $setting['page_title'] : $setting['page_title']), 70);
                break;
            case 'keywords': $result = $this->orez((isset($seo_data[$type]) && !empty($seo_data[$type]) ? $seo_data[$type] : $setting['keywords']), 50);
                break;
            case 'description': $result = $this->orez((isset($seo_data[$type]) && !empty($seo_data[$type]) ? $seo_data[$type] : $setting['description']), 150);
                break;
            case 'img': $result = (isset($seo_data[$type]) ? $seo_data[$type] : 'http://' . $_SERVER['HTTP_HOST'] . '/css/layout/logo.png');
                break;
            default: $result = $seo_data[$type];
                break;
        }
        return $result;
    }

    // orez textu	
    function orez($text, $limit) {
        $in = array("&nbsp;", "&", "\n", "\t", "    ");
        $out = array(" ", "&amp;", " ", "", " ");
        $replace = array(
            '&nbsp;' => ' ',
            '&' => '&amp;',
            "\n" => ' ',
            "\t" => ' ',
            '  ' => ' ',
            '   ' => ' ',
            '?' => '? ',
        );

        if (mb_strlen($text) <= $limit) {
            $output = strip_tags($text);
        } else {
            mb_internal_encoding("UTF-8");
            $output = strip_tags($text);
            $output = mb_substr($output, 0, $limit);
            // $output = str_replace($in,$out,$output);
            $output = strtr($output, $replace);
            $pos = mb_strrpos($output, " ");
            $output = trim(mb_substr($output, 0, $pos) . "...");
        }
        $tra = array(
            '"' => ' ',
            '\'' => ' ',
            "\n" => ' ',
        );
        $output = htmlspecialchars("$output", ENT_QUOTES);
        $output = strtr($output, $tra);
        return $output;
    }

    /*     * * ENCODE LONG URL * */

    public function encode_long_url($data) {
        return $output = strtr(base64_encode(addslashes(gzcompress(json_encode($data), 9))), '+/=', '-_,');
    }

    /*     * * DECODE LONG URL * */

    public function decode_long_url($data) {
        $data2 = explode('?', $data);
        $data = $data2[0];
        return json_decode(gzuncompress(stripslashes(base64_decode(strtr($data, '-_,', '+/=')))), true);
    }

    /*     * * FORMAT PRICE * */

    public function price($price, $mena_suf = array()) {

        //pr($mena_suf);
        $symbol_before = '';
        $kurz = null;
        $count = 1;
        $decimal = 0;
        $symbol_after = '';
        extract($mena_suf);

        if (isset($this->_View->viewVars['currency'])) {
            $symbol_after = ' ' . $this->_View->viewVars['currency'];
        }
        $white = array(" " => "");
        if ($kurz != null)
            $price = ($price / strtr($kurz, ',', '.')) * $count;
        if ($price != null)
            return $symbol_before . number_format(strtr($price, $white), $decimal, '.', ' ') . $symbol_after;
        else
            return $symbol_before . number_format(strtr(0.00, $white), $decimal, '.', ' ') . $symbol_after;
    }

    function CzechDate($date) {
        if (!empty($date) && $date != '0000-00-00') {
            $date = strtotime($date);
            $mesice = array("ledna", "února", "března", "dubna", "května", "června", "července", "srpna", "září", "října", "listopadu", "prosince");
            //return date('d',$date).'. '.$mesice[date ("n",$date) - 1].' '.date('Y',$date);
            return date('d', $date) . '.' . date("m", $date) . '.' . date('Y', $date);
        }
    }

    public function textOffset($text_position_id){
        $offset = '';
        switch($text_position_id){
            case 1:
                $offset = 0;
                break;
            case 2: 
                $offset = 3;
                break;
            case 3:
                $offset = 7;
                break;
        }
        return $offset;
    }
}
