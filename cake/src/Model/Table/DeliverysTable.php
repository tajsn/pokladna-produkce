<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class DeliverysTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        //$this->hasMany("Products");
        
        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');

    }

    public function beforeSave(Event $event)
    {
        //$event->data['entity']->name = $event->data['entity']->last_name.' '.$event->data['entity']->first_name; 
        //pr($event);die();
        
        return $event;
    }

  
    public function getDistributor($code,$order_id=null){
		
		$query = $this->find()
		  //->contain(['ZakazkaConnects',])
		  ->where([
              'code'=>$code
          ])
		  ->select([
			'id',
			'name',
			'code',
          ])
          ->cache(function ($query) {
			return 'distributor-id' .md5(serialize($query->clause('where')));
            })
          
        ;
		  
		$data =   $query->first();
        
        //pr($data);
        return $data;  
	}	
    
    public function deliveryList(){
		
		$query = $this->find('list',['keyField' => 'id','valueField' => 'name'])
		  //->contain(['ZakazkaConnects',])
		  ->where([])
		  ->select([
			'id',
			'name',
          ])
          ->cache(function ($query) {
			return 'delivery_data-list' . md5(serialize($query->clause('where')));
            })
        ;
		  
		$data_list =   $query->toArray();
        
        return $data_list;  
    }
    
    public function deliveryListCode($id){
		
		$query = $this->find()
		  //->contain(['ZakazkaConnects',])
		  ->where(['id'=>$id])
		  ->select([
			'id',
			'code',
          ])
          ->cache(function ($query) {
			return 'delivery_data-list-code' . md5(serialize($query->clause('where')));
            })
        ;
		$code =   $query->first();
        
		//   pr($id);
		//   pr($code);
        return $code->code;  
	}
    
    
    

    public function validationDefault(Validator $validator){

        $validator
            ->requirePresence('name', true,   __("Musíte zadat jméno"))
            ->notEmpty('name',__("Musíte zadat jméno"))
            
        ;

        /*$validator

          ->requirePresence('email', true,   __("Email musí být vyplněn"))

          ->notEmpty('email');*/

        return $validator;

    }

}