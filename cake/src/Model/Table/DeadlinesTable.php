<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class DeadlinesTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        // $this->hasMany("Products");
        
        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');

    }

    public function beforeSave(Event $event)
    {
        //$event->data['entity']->name = $event->data['entity']->last_name.' '.$event->data['entity']->first_name; 
        //pr($event);die();
        
        return $event;
    }

  
    public function getDistributorOrders($delivery_id,$order_id){
		
		$query = $this->find()
		  ->where([
              'type_id'=>2,
              'delivery_id'=>$delivery_id,
          ])
		  ->select([
			'id',
			'order_id_from',
			'order_id_to',
          ])
          ->order('id DESC')
        ;
		  
		$data =   $query->first();
        
        if ($data && $order_id < $data->order_id_to){
            return  false;
        } else {
            return true;
        }
        //pr($data);
          
	}	
    
  
    public function groupList(){
		
		$query = $this->find('list',['keyField' => 'id','valueField' => 'name'])
		  //->contain(['ZakazkaConnects',])
		  ->where([])
		  ->select([
			'id',
			'name',
          ])
          ->cache(function ($query) {
			return 'product_group_data-list' . md5(serialize($query->clause('where')));
            })
        ;
		  
		$data_list =   $query->toArray();
        
        return $data_list;  
	}	
    
    public function loadGroupProducts(){
		
		$query = $this->find()
		  ->contain(['Products'=>['fields'=>[
              'id',
              'product_group_id',
              'web_group_id',
              'name',
              'code',
              'price',
              'amount',
              'num',
            ],'sort'=>['num'=>'ASC']]
            ])
		  ->where(['ProductGroups.status'=>1])
		  ->select([
            'ProductGroups.id',
            'ProductGroups.name',
            'ProductGroups.color',
          ])
          ->order('ProductGroups.order_num ASC')
          ->cache(function ($query) {
			return 'product_data-order' . md5(serialize($query->clause('where')));
            })
        ;
		  
		$data_list =   $query->toArray();
        //pr($data_list);die();
        return $data_list;  
	}	

    

    public function validationDefault(Validator $validator){

        $validator
            //->requirePresence('name', true,   __("Musíte zadat jméno"))
            //->notEmpty('name',__("Musíte zadat jméno"))
            
        ;

        /*$validator

          ->requirePresence('email', true,   __("Email musí být vyplněn"))

          ->notEmpty('email');*/

        return $validator;

    }

}