<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ClientsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->hasMany("ClientAddresses");
        $this->hasMany("Orders");
        
        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');

    }

    public function beforeSave(Event $event)
    {
        $event->data['entity']->name = $event->data['entity']->last_name.' '.$event->data['entity']->first_name; 
        //pr($event);die();
        
        return $event;
    }

    /**
     * nacteni klient data z DB dle telefonu a prefix telefonu
     */
    public function getClient($phonePrefix,$phone,$client_id=null){
        if (isset($client_id) && $client_id != 'null'){
            $conditions = [
                'Clients.id'=>$client_id
            ];
        } else {
            $conditions = [
                'Clients.phone_pref'=>$phonePrefix,
                'Clients.phone'=>$phone
            ];
        }

        $this->mapperOrder = function ($d, $key, $mapReduce) {
            $d->created = $d->created->format('d.m.Y H:i:s');
            $d->pay_points_name = $this->selectList['yes_no'][$d->pay_points];
            $d->payment_id_name = (isset($this->selectList['payment_id'][$d->payment_id])?$this->selectList['payment_id'][$d->payment_id]:1);
                
            
            $mapReduce->emit($d);
        };

        $this->selectList = [
            'yes_no'=>[0=>'Ne',1=>'Ano'],
            'payment_id'=>[1=>'Hotově',2=>'Gopay'],
        ];
        

        $data = $this->find()
        ->contain([
            'ClientAddresses'=>function($q) {
                return $q
                ->select([
                    'id',
                    'client_id',
                    'type_id',
                    'street',
                    'city',
                    'zip',
                    'area_id',
                    'lat',
                    'lng',
                    'company',
                    
                ])
                ->order(['modified' =>'DESC'])
                ->limit(6);
            },
            'Orders'=>function($q) {
                return $q
                ->select([
                    'id',
                    'created',
                    'client_id',
                    'payment_id',
                    'pay_points',
                    'total_price',
                    
                ])
                ->order(['id' =>'DESC'])
                ->mapReduce($this->mapperOrder)
                ;
            },
            'Orders.OrderItems'=>function($q) {
                return $q
                ->select([
                    'id',
                    'name',
                    'order_id',
                    'price',
                    'count',
                           
                ])
                ->order(['id' =>'DESC'])
                ->limit(6);
            },
        
        ])
        ->select([
            'id',
            'name',
            'first_name',
            'last_name',
            'email',
            'phone',
            'operating',
            'problem',
            'problem_message',
            'phone_pref',
        ])
        ->order('Clients.id DESC')
        ->where($conditions)
        ->first();
        
        //pr($data);die();
        
        if ($data){
            return $data;
        } else {
            return false;
        }
          
	}

    
    /**
     * nacteni klient data z DB dle autocomplete
     */
    public function getClientAutocomplete($type,$value){
        
        switch($type){

            case 'phone': 
                $conditions = [
                    'Clients.phone LIKE '=>$value.'%',
                ];
            break;
            case 'name': 
                $conditions = [
                    'Clients.name LIKE '=>$value.'%',
                ];
            break;
            default :
                die(json_encode(['result'=>false]));
        }


        $data = $this->find()
        ->contain(['ClientAddresses'=>
            [
            'fields'=>[
                'id',
                'client_id',
                'type_id',
                'street',
                'city',
                'zip',
                'area_id',
                'lat',
                'lng',
                'company',
            ],
            'sort'=>['modified'=>'DESC']
            ]
        ])
        ->select([
            'id',
            'name',
            'first_name',
            'last_name',
            'email',
            'phone',
            'operating',
            'problem',
            'problem_message',
            'phone_pref',
        ])
        ->where($conditions)
        ->limit(10)
        ->order('id DESC')
        ->toArray();
        //pr($conditions);die();
        //pr($data);die();
        
        if ($data){
            return $data;
        } else {
            return false;
        }
          
	}

    

    public function validationDefault(Validator $validator){

        $validator
            ->requirePresence('first_name', true,   __("Musíte zadat jméno"))
            ->notEmpty('first_name',__("Musíte zadat jméno"))
            ->requirePresence('last_name', true,   __("Musíte zadat příjmení"))
            ->notEmpty('last_name',__("Musíte zadat příjmení"))
        
            ->requirePresence('phone_pref', true,   __("Musíte zadat telefon prefix"))
            ->notEmpty('phone_pref',__("Musíte zadat telefon prefix"))
            
            ->requirePresence('phone', true,   __("Musíte zadat telefon"))
            ->notEmpty('phone',__("Musíte zadat telefon"))
            ->add('phone', [
                'length' => [
                    'rule' => ['minLength', 9],
                    'message' => 'Telefon musí mít 9 číslic a mezinárodní kód země',
                ]
            ])
            ->allowEmpty('email')
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => 'E-mail je ve špatném formátu'
            ])
            
        ;

        /*$validator

          ->requirePresence('email', true,   __("Email musí být vyplněn"))

          ->notEmpty('email');*/

        return $validator;

    }

}