<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class OldClientsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('fastest__pokladnas');
        $this->hasMany('OldClientAddressas',['foreignKey' => 'pokladna_id']);
        //$this->addBehavior('Timestamp');
        //$this->addBehavior('Trash');

    }
    public static function defaultConnectionName() {
        
        $conn = 'default2';
        return $conn;
    }

    public function beforeSave(Event $event)
    {
       
        return true;
    }


}