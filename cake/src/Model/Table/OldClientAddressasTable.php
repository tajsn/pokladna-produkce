<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class OldClientAddressasTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('fastest__pokladna_adresas');
        // $this->hasMany('OldClientAddressas');
        //$this->addBehavior('Timestamp');
        //$this->addBehavior('Trash');

    }
    public static function defaultConnectionName() {
        
        $conn = 'default2';
        return $conn;
    }

    public function beforeSave(Event $event)
    {
       
        return true;
    }


}