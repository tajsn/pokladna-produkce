<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        //$this->hasMany("ClientAddresses");
        
        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');

    }

    public function beforeSave(Event $event)
    {
        //$event->data['entity']->name = $event->data['entity']->last_name.' '.$event->data['entity']->first_name; 
        //pr($event);die();
        
        return $event;
    }

    public function getProduct($code){
		
		$query = $this->find()
		  //->contain(['ZakazkaConnects',])
		  ->where(['code'=>$code])
		  ->select([
			'id',
          ])
        ;
		  
		$data =   $query->first();
        //pr($data);die();
        if ($data){
            return $data->id;
        }  else {
            return false;
        }
	}	
    
    public function findProduct($code){
		
		$query = $this->find()
		  //->contain(['ZakazkaConnects',])
		  ->where(['code'=>$code])
		  ->select([
          ])
        ;
		  
		$data =   $query->first();
        //pr($data);die();
        if ($data){
            return $data;
        }  else {
            return false;
        }
	}	
  

  

    

    public function validationDefault(Validator $validator){

        $validator
            ->requirePresence('name', true,   __("Musíte zadat jméno"))
            ->notEmpty('name',__("Musíte zadat jméno"))
            
            ->requirePresence('code', true,   __("Musíte zadat kód"))
            ->notEmpty('code',__("Musíte zadat kód"))
            
            ->requirePresence('product_group_id', true,   __("Musíte zadat skupinu produktu"))
            ->notEmpty('product_group_id',__("Musíte zadat skupinu produktu"))
            
            ->requirePresence('price', true,   __("Musíte zadat cenu"))
            ->notEmpty('price',__("Musíte zadat cenu"))
            
            
        ;

        /*$validator

          ->requirePresence('email', true,   __("Email musí být vyplněn"))

          ->notEmpty('email');*/

        return $validator;

    }

}