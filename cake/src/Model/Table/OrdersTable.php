<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class OrdersTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->belongsTo("Clients");
        
        $this->hasMany("OrderItems");

        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');

    }

    public function beforeSave(Event $event)
    {
       
        return true;
    }

    
    
    public function getOrdersList(){
        $mapperOrder = function ($d, $key, $mapReduce) {
            //$d->time = $d->created->format('H:i');
            //$d->created = $d->created->format('d.m.Y H:i:s');
            //pr($d->created);    
            
            $mapReduce->emit($d);
        };
    
        $conditions = [
            // 'date(created)'=>date('Y-m-d'),
            'distributor_id IS'=>null,
            'storno'=>0,
            'close_order'=>0,
        ];
        $data = $this->find()
        ->contain('OrderItems')
        ->select()
        ->where($conditions)
        ->mapReduce($mapperOrder)
        ->order('Orders.id DESC')
        ->toArray();
        //pr($data);
        return $data;
    }

    public function checkOpenOrder(){
        
        $conditions = [
            'distributor_id IS'=>null,
            'storno'=>0,
            'close_order'=>0,
        ];
        $data = $this->find()
        ->select(['id'])
        ->where($conditions)
        ->order('Orders.id DESC')
        ->toArray();
        
        return count($data);
    }
  
    public function getOrderId($web_id){
        
        $conditions = [
            'web_id'=>$web_id,
        ];
        $data = $this->find()
        ->select(['id'])
        ->where($conditions)
        ->first();
        if ($data){
            return $data;
        } else {
            return false;
        }
    }
  

    

    public function validationDefault(Validator $validator){

        $validator
            //->requirePresence('name', true,   __("Jméno musí být vyplněno"))
            //->notEmpty('name',__('Jméno musí být vyplněno'))
            
            ->requirePresence('source_id', true,   __("Zdroj musí být vyplněn"))
            ->notEmpty('source_id',__('Zdroj musí být vyplněn'))
        ;


        /*$validator

          ->requirePresence('email', true,   __("Email musí být vyplněn"))

          ->notEmpty('email');*/

        return $validator;

    }

}