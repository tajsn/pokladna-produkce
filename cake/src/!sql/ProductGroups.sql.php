<?php 
 $sql = [
    'product_groups'=>[
        'cols'=>[
            'id' => [
                'type' => 'int',
                'length' => 11,
                'null' => false,
                'default' => null,
            ],
            'name'=>[
                'type' => 'varchar',
                'length' => 100,
            ],
            'web_id'=>[
                'type' => 'int',
                'length' => 5,
                'null'=>true,
            ],
            'order_num'=>[
                'type' => 'int',
                'length' => 5,
                'null'=>true,
            ],
            'color'=>[
                'type' => 'varchar',
                'length' => 10,
                'null'=>true,
            ],
            'created'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'modified'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'status'=>[
                'type' => 'tinyint',
                'default' => 1,
                'length' => 1,
            ],
            'nodelete'=>[
                'type' => 'tinyint',
                'default' => 0,
                'length' => 1,
            ],
            'trash'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            
        ],
        'indexs'=>[
            'name'=>['col'=>['name']],
            'trash'=>['col'=>['trash']],
            'status'=>['col'=>['status']],
            'web_id'=>['col'=>['web_id']],
        ],
        // 'defaultValues'=>[
        //     [
        //         'id'=>1,
        //         'title'=>'name',
        //         'title2'=>'name2',
        //     ]
        // ]
    ],
    
    

];
?>
