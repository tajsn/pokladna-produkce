<?php 
 $sql = [
    'logs'=>[
        'cols'=>[
            'id' => [
                'type' => 'int',
                'length' => 11,
                'null' => false,
                'default' => null,
            ],
            'system_id' => [
                'type' => 'int',
                'length' => 11,
                'null' => false,
                'default' => null,
            ],
            'url' => [
                'type' => 'varchar',
                'length' => 200,
                'null' => false,
                'default' => null,
            ],
            'message' => [
                'type' => 'text',
            ],
            'created' => [
                'type' => 'datetime',
            ],
        ],
    ],
    'users'=>[
        'cols'=>[
            'id' => [
                'type' => 'int',
                'length' => 11,
                'null' => false,
                'default' => null,
            ],
            'name'=>[
                'type' => 'varchar',
                'length' => 50,
            ],
            'username'=>[
                'type' => 'varchar',
                'length' => 50,
            ],
            'password'=>[
                'type' => 'varchar',
                'length' => 255,
            ],
            'user_group_id'=>[
                'type' => 'int',
                'length' => 5,
                'null'=>true,
            ],
            'created'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'modified'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'status'=>[
                'type' => 'tinyint',
                'default' => 1,
                'length' => 1,
            ],
            'nodelete'=>[
                'type' => 'tinyint',
                'default' => 0,
                'length' => 1,
            ],
            'trash'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            
        ],
        'indexs'=>[
            'name'=>['col'=>['name']],
            'user_group_id'=>['col'=>['user_group_id']],
            'trash'=>['col'=>['trash']],
            'status'=>['col'=>['status']],
        ],
        // 'defaultValues'=>[
        //     [
        //         'id'=>1,
        //         'title'=>'name',
        //         'title2'=>'name2',
        //     ]
        // ]
    ],
    
    

];
?>
