<?php 
 $sql = [
    'orders'=>[
        'cols'=>[
            'id' => [
                'type' => 'int',
                'length' => 11,
                'null' => false,
                'default' => null,
            ],
            'name'=>[
                'type' => 'varchar',
                'length' => 50,
            
            ],
           
            'client_id'=>[
                'type' => 'int',
                'length' => 11,
                'null' => true,
            ],
            'close_order'=>[
                'type' => 'tinyint',
                'length' => 1,
                'default' => 0,
                'null' => true,
            ],
            'client_addresses_id'=>[
                'type' => 'int',
                'length' => 11,
                'null' => true,
            ],
            'web_id'=>[
                'type' => 'int',
                'length' => 11,
                'null' => true,
            ],
            'phone_call'=>[
                'type' => 'varchar',
                'length' => 12,
            
            ],
            'street'=>[
                'type' => 'varchar',
                'length' => 50,
            
            ],
            'city'=>[
                'type' => 'varchar',
                'length' => 50,
            
            ],
            'zip'=>[
                'type' => 'varchar',
                'length' => 10,
            
            ],
            'company'=>[
                'type' => 'varchar',
                'length' => 30,
            
            ],
            'note'=>[
                'type' => 'text',
                'length' => 500,
            
            ],
            'lat'=>[
                'type' => 'varchar',
                'length' => 50,
            
            ],
            'lng'=>[
                'type' => 'varchar',
                'length' => 50,
            
            ],
            'area_id'=>[
                'type' => 'varchar',
                'length' => 11,
                'null' => true,
            ],
            'area_name'=>[
                'type' => 'varchar',
                'length' => 20,
                'null' => true,
            ],
            'send_time_id'=>[
                'type' => 'int',
                'length' => 5,
                'null' => true,
            ],
            'gopay_id'=>[
                'type' => 'varchar',
                'length' => 25,
                'null' => true,
            ],
            'total_price'=>[
                'type' => 'decimal',
                'length' => '10,2',
                'null' => true,
            ],
            'shipping_price'=>[
                'type' => 'decimal',
                'length' => '10,2',
                'null' => true,
            ],
            'pay_points'=>[
                'type' => 'tinyint',
                'default' => 0,
                'length' => 1,
            ],
            'source_id'=>[
                'type' => 'int',
                'length' => 11,
                'null' => true,
                'default' => 1,
            ],
            'xml_type_id'=>[
                'type' => 'int',
                'length' => 11,
                'null' => true,
                'default' => 1,
            ],
            'paymentSessionId'=>[
                'type' => 'int',
                'length' => 11,
                'null' => true,
            ],
            'dj_id'=>[
                'type' => 'int',
                'length' => 11,
                'null' => true,
            ],
            'payment_id'=>[
                'type' => 'int',
                'length' => 5,
                'null' => true,
                'default' => 1,
            ],
            'distributor_id'=>[
                'type' => 'int',
                'length' => 5,
                'null' => true,
            ],
            'fik'=>[
                'type' => 'varchar',
                'length' => 250,
                'null' => true,
            ],
            'bkp'=>[
                'type' => 'varchar',
                'length' => 250,
                'null' => true,
            ],
            'eet_time'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'eet_offline'=>[
                'type' => 'tinyint',
                'default' => 0,
                'length' => 1,
            ],
            'delivery_to_time'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'created'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'createdTime'=>[
                'type' => 'int',
                'length' => 10,
                'null' => true,
            ],
            'transfer_id'=>[
                'type' => 'int',
                'length' => 10,
                'null' => true,
            ],
            'modified'=>[
                'type' => 'datetime',
                'null' => true,
            ], 
            'status'=>[
                'type' => 'tinyint',
                'default' => 1,
                'length' => 1,
            ],
            'storno'=>[
                'type' => 'tinyint',
                'default' => 0,
                'length' => 1,
            ],
            'later'=>[
                'type' => 'tinyint',
                'default' => 0,
                'length' => 1,
            ],
            'operating' => [
                'type' => 'tinyint',
                'length' => 1,
                'null' => true,
                'default' => 0,
            ],
            'sendCloud' => [
                'type' => 'tinyint',
                'length' => 1,
                'null' => true,
                'default' => 0,
            ],
            'storno_type'=>[
                'type' => 'int',
                'default' => 0,
                'length' => 4,
            ],
            'trash'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'system_id'=>[
                'type' => 'int',
                'length' => 10,
                'null' => true,
            ],
        ],
        'indexs'=>[
            'name'=>['col'=>['name']],
            'status'=>['col'=>['status']],
            'trash'=>['col'=>['trash']],
            'source_id'=>['col'=>['source_id']],
            'client_id'=>['col'=>['client_id']],
            'web_id'=>['col'=>['web_id']],
            'close_order'=>['col'=>['close_order']],
            'client_addresses_id'=>['col'=>['client_addresses_id']],
            'distributor_id'=>['col'=>['distributor_id']],
            'payment_id'=>['col'=>['payment_id']],
            'pay_points'=>['col'=>['pay_points']],
            'storno'=>['col'=>['storno']],
            'created'=>['col'=>['created']],
            'createdTime'=>['col'=>['createdTime']],
            'xml_type_id'=>['col'=>['xml_type_id']],
            'system_id'=>['col'=>['system_id']],
            'operating'=>['col'=>['operating']],
            'storno_type'=>['col'=>['storno_type']],
            'operating'=>['col'=>['operating']],
            'sendCloud'=>['col'=>['sendCloud']],
            'later'=>['col'=>['later']],
            //'trash'=>['col'=>['trash']],
        ],
        // 'defaultValues'=>[
        //     [
        //         'id'=>1,
        //         'title'=>'name',
        //         'title2'=>'name2',
        //     ]
        // ]
    ],
    'order_items'=>[
        'cols'=>[
            'id' => [
                'type' => 'int',
                'length' => 11,
                'null' => false,
                'default' => null,
            ],
            'order_id' => [
                'type' => 'int',
                'length' => 11,
                'null' => false,
            ],
            'product_id' => [
                'type' => 'int',
                'length' => 11,
                'null' => false,
            ],
            'name' => [
                'type' => 'varchar',
                'length' => 150,
                'null' => true,
            ],
            'amount' => [
                'type' => 'varchar',
                'length' => 11,
                'null' => true,
            ],
            'code' => [
                'type' => 'varchar',
                'length' => 11,
                'null' => true,
            ],
            'num' => [
                'type' => 'varchar',
                'length' => 11,
                'null' => true,
            ],
            'price' => [
                'type' => 'decimal',
                'length' => '10,2',
                'null' => true,
                'disable_UNSIGNED'=>true
            ],
            'price_addon' => [
                'type' => 'decimal',
                'length' => '10,2',
                'null' => true,
            ],
            'addon' => [
                'type' => 'int',
                'length' => '5',
                'null' => true,
            ],
            'price_default' => [
                'type' => 'decimal',
                'length' => '10,2',
                'null' => true,
            ],
            'free' => [
                'type' => 'tinyint',
                'length' => 1,
                'null' => true,
                'default' => 0,
            ],
            
            'count' => [
                'type' => 'int',
                'length' => 5,
                'null' => true,
            ],
            'product_group_id' => [
                'type' => 'int',
                'length' => 5,
                'null' => true,
            ],
            'web_group_id' => [
                'type' => 'int',
                'length' => 5,
                'null' => true,
            ],
            'system_id' => [
                'type' => 'int',
                'length' => 5,
                'null' => true,
            ],
            
            'created'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'modified'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'trash'=>[
                'type' => 'datetime',
                'null' => true,
            ],
        ],
        'indexs'=>[
            'order_id'=>['col'=>['order_id']],
            'product_group_id'=>['col'=>['product_group_id']],
            'web_group_id'=>['col'=>['web_group_id']],
            'product_id'=>['col'=>['product_id']],
            'trash'=>['col'=>['trash']],
            'system_id'=>['col'=>['system_id']],
            'free'=>['col'=>['free']],
        ]
    ]

];
?>
