<?php 
 $sql = [
    'deadlines'=>[
        'cols'=>[
            'id' => [
                'type' => 'int',
                'length' => 11,
                'null' => false,
                'default' => null,
            ],
            'order_id_from' => [
                'type' => 'int',
                'length' => 11,
                'default' => null,
            ],
            'delivery_id' => [
                'type' => 'int',
                'length' => 5,
                'default' => null,
            ],
            'order_id_to' => [
                'type' => 'int',
                'length' => 11,
                'default' => null,
            ],
            'type_id' => [
                'type' => 'int',
                'length' => 2,
                'default' => null,
            ],
            'created'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'createdTime'=>[
                'type' => 'int',
                'length' => 10,
                'null' => true,
            ],
            'system_id'=>[
                'type' => 'int',
                'length' => 10,
                'null' => true,
            ],
            'modified'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'sended'=>[
                'type' => 'tinyint',
                'default' => 1,
                'length' => 1,
                'null' => true,
                'default' => 0,
            ],
            'status'=>[
                'type' => 'tinyint',
                'default' => 1,
                'length' => 1,
            ],
            'nodelete'=>[
                'type' => 'tinyint',
                'default' => 0,
                'length' => 1,
            ],
            'trash'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'open_date'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'close_date'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            
        ],
        'indexs'=>[
            'order_id_from'=>['col'=>['order_id_from']],
            'order_id_to'=>['col'=>['order_id_to']],
            'type_id'=>['col'=>['type_id']],
            'trash'=>['col'=>['trash']],
            'status'=>['col'=>['status']],
            'created'=>['col'=>['created']],
            'createdTime'=>['col'=>['createdTime']],
            'delivery_id'=>['col'=>['delivery_id']],
            'sended'=>['col'=>['sended']],
        ],
        // 'defaultValues'=>[
        //     [
        //         'id'=>1,
        //         'title'=>'name',
        //         'title2'=>'name2',
        //     ]
        // ]
    ],
    
    

];
?>
