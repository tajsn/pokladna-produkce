<?php 
 $sql = [
    'deliverys'=>[
        'cols'=>[
            'id' => [
                'type' => 'int',
                'length' => 11,
                'null' => false,
                'default' => null,
            ],
            'name'=>[
                'type' => 'varchar',
                'length' => 100,
            ],
            'code'=>[
                'type' => 'varchar',
                'length' => 5,
                'null'=>true,
            ],
            'created'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'modified'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'status'=>[
                'type' => 'tinyint',
                'default' => 1,
                'length' => 1,
            ],
            'nodelete'=>[
                'type' => 'tinyint',
                'default' => 0,
                'length' => 1,
            ],
            'trash'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            
        ],
        'indexs'=>[
            'name'=>['col'=>['name']],
            'code'=>['col'=>['code']],
            'trash'=>['col'=>['trash']],
            'status'=>['col'=>['status']],
        ],
        // 'defaultValues'=>[
        //     [
        //         'id'=>1,
        //         'title'=>'name',
        //         'title2'=>'name2',
        //     ]
        // ]
    ],
    
    

];
?>
