<?php 
 $sql = [
    'settings'=>[
        'cols'=>[
            'id' => [
                'type' => 'int',
                'length' => 11,
                'null' => false,
                'default' => null,
            ],
            'name'=>[
                'type' => 'varchar',
                'length' => 50,
            ],
            'printer'=>[
                'type' => 'varchar',
                'length' => 200,
            ],
            'data'=>[
                'type' => 'text',
            ],
            'logo'=>[
                'type' => 'text',
            ],
            'operations'=>[
                'type' => 'text',
            ],
            'created'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'modified'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'status'=>[
                'type' => 'tinyint',
                'default' => 1,
                'length' => 1,
            ],
            'nodelete'=>[
                'type' => 'tinyint',
                'default' => 0,
                'length' => 1,
            ],
            'trash'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            
        ],
        'indexs'=>[
        ],
        // 'defaultValues'=>[
        //     [
        //         'id'=>1,
        //         'title'=>'Provozovna',
        //     ]
        // ]
    ],
    
    

];
?>
