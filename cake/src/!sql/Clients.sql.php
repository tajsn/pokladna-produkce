<?php 
 $sql = [
    'clients'=>[
        'cols'=>[
            'id' => [
                'type' => 'int',
                'length' => 11,
                'null' => false,
                'default' => null,
            ],
            'name'=>[
                'type' => 'varchar',
                'length' => 100,
            ],
            'first_name'=>[
                'type' => 'varchar',
                'length' => 48,
                'null' => true,
            ],
            'last_name'=>[
                'type' => 'varchar',
                'length' => 48,
                'null' => true,
            ],
            'email'=>[
                'type' => 'varchar',
                'length' => 80,
                'null' => true,
            ],
            'phone'=>[
                'type' => 'varchar',
                'length' => 9,
                'null' => true,
            ],
            'email'=>[
                'type' => 'varchar',
                'length' => 50,
                'null' => true,
            ],
            'operating'=>[
                'type' => 'tinyint',
                'length' => 1,
                'default' => 0,
            ],
            'problem'=>[
                'type' => 'tinyint',
                'length' => 1,
                'default' => 0,
            ],
            'problem_message'=>[
                'type' => 'varchar',
                'length' => 20,
                'null'  => true
            ],
            'phone_pref'=>[
                'type' => 'varchar',
                'length' => 6,
                'null' => true,
            ],
            'created'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'modified'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'status'=>[
                'type' => 'tinyint',
                'default' => 1,
                'length' => 1,
            ],
            'trash'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            
        ],
        'indexs'=>[
            'name'=>['col'=>['name']],
            'phone_pref'=>['col'=>['phone_pref']],
            'phone'=>['col'=>['phone']],
            'problem'=>['col'=>['problem']],
            'operating'=>['col'=>['operating']],
            'email'=>['col'=>['email']],
            'status'=>['col'=>['status']],
            'trash'=>['col'=>['trash']],
        ],
        // 'defaultValues'=>[
        //     [
        //         'id'=>1,
        //         'title'=>'name',
        //         'title2'=>'name2',
        //     ]
        // ]
    ],
    'client_addresses'=>[
        'cols'=>[
            'id' => [
                'type' => 'int',
                'length' => 11,
                'null' => false,
                'default' => null,
            ],
            'client_id' => [
                'type' => 'int',
                'length' => 11,
                'null' => false,
            ],
            'type_id' => [
                'type' => 'int',
                'length' => 11,
                'null' => false,
            ],
            'street' => [
                'type' => 'varchar',
                'length' => 150,
                'null' => true,
            ],
            'city' => [
                'type' => 'varchar',
                'length' => 50,
                'null' => true,
            ],
            'zip' => [
                'type' => 'varchar',
                'length' => 10,
                'null' => true,
            ],
            'company' => [
                'type' => 'varchar',
                'length' => 30,
                'null' => true,
            ],
            'lat' => [
                'type' => 'decimal',
                'length' => '10,7',
                'null' => true,
                'disable_UNSIGNED'=>true,
            ],
            'lng' => [
                'type' => 'decimal',
                'length' => '10,7',
                'null' => true,
                'disable_UNSIGNED' => true,
            ],
            'area_id' => [
                'type' => 'int',
                'length' => 11,
                'null' => true,
            ],
            'created'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'modified'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'trash'=>[
                'type' => 'datetime',
                'null' => true,
            ],
        ],
        'indexs'=>[
            'client_id'=>['col'=>['client_id']],
            'type_id'=>['col'=>['type_id']],
            'area_id'=>['col'=>['area_id']],
            'trash'=>['col'=>['trash']],
        ]
    ]

];
?>
