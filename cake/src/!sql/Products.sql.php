<?php 
 $sql = [
    'products'=>[
        'cols'=>[
            'id' => [
                'type' => 'int',
                'length' => 11,
                'null' => false,
                'default' => null,
            ],
            'name'=>[
                'type' => 'varchar',
                'length' => 150,
            ],
            'code'=>[
                'type' => 'varchar',
                'length' => 10,
            ],
            'web_id'=>[
                'type' => 'int',
                'length' => 5,
                'null'=>true,
            ],
            'product_group_id'=>[
                'type' => 'int',
                'length' => 6,
                'null'=>true,
            ],
            'web_group_id'=>[
                'type' => 'int',
                'length' => 6,
                'null'=>true,
            ],
            'num'=>[
                'type' => 'int',
                'length' => 6,
                'null'=>true,
            ],
            'price'=>[
                'type' => 'decimal',
                'length' => '10,2',
                'null'=>true,
            ],
            'amount'=>[
                'type' => 'varchar',
                'length' => 10,
            ],
            'created'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'modified'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            'status'=>[
                'type' => 'tinyint',
                'default' => 1,
                'length' => 1,
            ],
            'nodelete'=>[
                'type' => 'tinyint',
                'default' => 0,
                'length' => 1,
            ],
            'trash'=>[
                'type' => 'datetime',
                'null' => true,
            ],
            
        ],
        'indexs'=>[
            'name'=>['col'=>['name']],
            'trash'=>['col'=>['trash']],
            'status'=>['col'=>['status']],
            'product_group_id'=>['col'=>['product_group_id']],
            'web_group_id'=>['col'=>['web_group_id']],
            'code'=>['col'=>['code']],
            'web_id'=>['col'=>['web_id']],
        ],
        // 'defaultValues'=>[
        //     [
        //         'id'=>1,
        //         'title'=>'name',
        //         'title2'=>'name2',
        //     ]
        // ]
    ],
    
    

];
?>
