<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

class ClientsController extends AppController{
    
    /**
     * vytvoreni select listu
     */
    public function getSelectList($check=true){
        if ($check == false || isset($this->request->query['firstLoad'])){
            
            return $select_list = [
                'source_list'=>$this->source_list,
                'no_yes'=>$this->no_yes,
            ];
        } else {
            return false;
        }  
        
        
    }

    /**
     * INDEX orders
     */
    public function index()
    {
        $this->loadComponent('vI');

        $select_list = $this->getSelectList();
        //pr($select_list);die();
        $conditions = [];
        $fields_defined = [
            0=>['col'=>'id','title'=>'ID','type'=>'text'],
            1=>['col'=>'name','title'=>'Jméno','type'=>'text'],
            2=>['col'=>'phone','title'=>'Telefon','type'=>'text'],
            3=>['col'=>'email','title'=>'Email','type'=>'text'],
            4=>['col'=>'operating','title'=>'Zaměstnanec','type'=>'list','list_data'=>$select_list['no_yes']],
            5=>['col'=>'problem','title'=>'Problémový','type'=>'list','list_data'=>$select_list['no_yes']],
            6=>['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
            //3=>['col'=>'zdroj_id','title'=>'Zdroj','type'=>'list','list_data'=>'zdroj_list'],
        ];

        $this->filtration_defined = [
            'id'=>['col'=>'id','title'=>'ID','type'=>'text'],
            'name'=>['col'=>'name','title'=>'Jméno','type'=>'text_like'],
            'phone'=>['col'=>'phone','title'=>'Telefon','type'=>'text_like'],
            'email'=>['col'=>'email','title'=>'Email','type'=>'text_like'],
            'problem'=>['col'=>'problem','title'=>'Problémový','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['no_yes'])],
            'operating'=>['col'=>'operating','title'=>'Zaměstnanec','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['no_yes'])],
            //'created'=>['col'=>'created','title'=>'Vytvořeno','type'=>'date_range'],
            //'modified'=>['col'=>'modified','title'=>'Upraveno','type'=>'date'],
        ];
        
        if (isset($this->request->data['conditions'])){
            //pr($this->request->data['conditions']);
            $conditions = $this->vI->convertConditions($conditions,$this->request->data['conditions'],$this->filtration_defined);
            //pr($conditions);die();
        }
        
       
        $posibility = [
            0=>['link'=>'/api/status/Clients/','title'=>__('Status'),'class'=>'fa-check-circle-o','type'=>'ajax','params'=>'status'],
            1=>['link'=>'./edit/','title'=>__('Editovat'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
            2=>['link'=>'/api/trash/Clients/','title'=>__('Smazat'),'class'=>'fa-trash','type'=>'ajax','params'=>'trash','confirm'=>__('Opravdu smazat?')],
        ];
        
        $top_actions = [
            0=>['link'=>'./edit/','title'=>__('Nová položka'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
        ];

        
        $fields = $this->vI->fieldsConvert($fields_defined);
        //pr($fields);die();
        
        $mapper = function ($data, $key, $mapReduce) {
            $data->operating = (($data->operating == '')?0:1);
            $data->problem = (($data->problem == '')?0:1);

            
            $mapReduce->emit($data);  
        };

        $query = $this->Clients->find()
            ->select($fields)
            ->where($conditions)
            ->cache(function ($query) {
				return 'clients_data-' . md5(serialize($query->clause('where')));
            })
            ->mapReduce($mapper)
        ;
        
        //$data = $query;
        //pr($query->toArray());die();
        
        
        $this->loadComponent('Paginator');
        $data_list = $this->paginate($query);
        //pr($data_list);die();
        
        
        $pagination = $this->vI->convertPagination();
        //pr($pagination['page']);
        $results = [
            'result'=>true,
            'data'=>$data_list->toArray(),
            'data_count'=>count($data_list->toArray()),
            'pagination'=>$pagination,
            'table_th'=>$fields_defined,
            'filtration'=>array_values($this->filtration_defined),
            'select_list'=>$select_list,
            'posibility'=>$posibility,
            'top_actions'=>$top_actions,
            'conditions'=>(object) $conditions,
        ];
        
        $this->setJsonResponse($results);
    }

    /**
     * editace
     */
    public function edit($id=null){
        $select_list = $this->getSelectList(false);
        $this->loadComponent('vI');
        //pr($select_list);die();
        
        // save data
        if (isset($this->request->data['saveData'])){
            $saveData = $this->request->data['saveData'];
            $saveData = $this->vI->convertTime($saveData);
            $save_entity = $this->Clients->newEntity($saveData);
            //pr($save_entity); die();
            
            $this->vI->checkErrors($save_entity);

            if (!$resultDb = $this->Clients->save($save_entity)){
                $results = [
                    'result'=>false,
                    'message'=>__('Chyba uložení')
                ];    
            } else {
                if ($resultDb['client_addresses']){
                    foreach($resultDb['client_addresses'] AS $adr){
                        $adrIds[$adr->id] = $adr->id;
                    }
                    $this->loadModel('ClientAddresses');
                    $this->ClientAddresses->deleteAll(['id NOT IN' => $adrIds,'client_id'=>$resultDb->id]);
                    //pr($adrIds);
                }
                $results = [
                    'result'=>true,
                    'message'=>__('Uloženo'),
                    'data'=>$resultDb
                ];  
            }
            
            

            $this->setJsonResponse($results);
        
        // load data
        } else {
            $defaultValues = [
                'problem'=>0,
                'operating'=>0,
            ];
            if ($id != null){
                // pokud je edit
                if ($id != 'new'){
                    $conditions = ['id'=>$id];
                    $data = $this->Clients->find()
                    ->contain(['ClientAddresses'])
                    ->select()
                    ->where($conditions)
                    ->first()
                ;
                // pokud je nova polozka
                } else {
                   
                    $data = $this->vI->emptyEntity('Clients',['hasMany'=>'ClientAddresses']); 
                    
                }
                $data = $this->vI->convertLoadData($data,$defaultValues);        
                //pr($data);die();
                $validations = $this->vI->getValidations('Clients');
            } else {

            }

            $results = [
                'result'=>true,
                'validations'=>(isset($validations)?$validations:null),
                'data'=>(isset($data)?$data:''),
                'select_list'=>$this->vI->SelectList($select_list),
            ];  
            //pr($results);die();  
            $this->setJsonResponse($results);
        }
        $this->clearCache('clients_data');
    }
}