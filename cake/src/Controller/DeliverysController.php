<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

class DeliverysController extends AppController{
    
    /**
     * vytvoreni select listu
     */
    public function getSelectList($check=true){
        if ($check == false || isset($this->request->query['firstLoad'])){
            
            return $select_list = [
                'no_yes'=>$this->no_yes,
            ];
        } else {
            return false;
        }  
        
        
    }

    /**
     * INDEX orders
     */
    public function index()
    {
        $this->loadComponent('vI');

        $select_list = $this->getSelectList();
        //pr($select_list);die();
        $conditions = [];
        $fields_defined = [
            0=>['col'=>'id','title'=>'ID','type'=>'text'],
            1=>['col'=>'name','title'=>'Jméno','type'=>'text'],
            2=>['col'=>'code','title'=>'Kod','type'=>'text'],
            3=>['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
            //3=>['col'=>'zdroj_id','title'=>'Zdroj','type'=>'list','list_data'=>'zdroj_list'],
        ];

        $this->filtration_defined = [
            'id'=>['col'=>'id','title'=>'ID','type'=>'text'],
            'name'=>['col'=>'name','title'=>'Jméno','type'=>'text_like'],
            'code'=>['col'=>'code','title'=>'Kod','type'=>'text'],
            //'created'=>['col'=>'created','title'=>'Vytvořeno','type'=>'date_range'],
            //'modified'=>['col'=>'modified','title'=>'Upraveno','type'=>'date'],
        ];
        
        if (isset($this->request->data['conditions'])){
            //pr($this->request->data['conditions']);
            $conditions = $this->vI->convertConditions($conditions,$this->request->data['conditions'],$this->filtration_defined);
            //pr($conditions);die();
        }
        
       
        $posibility = [
            0=>['link'=>'/api/status/Deliverys/','title'=>__('Status'),'class'=>'fa-check-circle-o','type'=>'ajax','params'=>'status'],
            1=>['link'=>'./edit/','title'=>__('Editovat'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
            2=>['link'=>'/api/trash/Deliverys/','title'=>__('Smazat'),'class'=>'fa-trash','type'=>'ajax','params'=>'trash','confirm'=>__('Opravdu smazat?')],
            
        ];
        
        $top_actions = [
            0=>['link'=>'./edit/','title'=>__('Nová položka'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
            1=>['link'=>'/api/deliverys/sendApp/','title'=>__('Poslat do mobilní aplikace'),'class'=>'fa-edit','type'=>'ajax','params'=>'sendApp'],
        ];

        
        $fields = $this->vI->fieldsConvert($fields_defined);
        //pr($fields);die();
        
        $mapper = function ($data, $key, $mapReduce) {
            //$data->operating = (($data->operating == '')?0:1);
            //$data->problem = (($data->problem == '')?0:1);

            
            $mapReduce->emit($data);  
        };

        $query = $this->Deliverys->find()
            ->select($fields)
            ->where($conditions)
            ->cache(function ($query) {
				return 'deliverys_data-' . md5(serialize($query->clause('where')));
            })
            ->mapReduce($mapper)
        ;

        
        if (isset($this->request->query['firstLoad'])){
            $this->request->query['sort'] = 'order_num';
            $this->request->query['direction'] = 'ASC';
        }
        //$data = $query;
        //pr($query->toArray());die();
        
        
        $this->loadComponent('Paginator');
        $data_list = $this->paginate($query);
        
        
        
        $pagination = $this->vI->convertPagination();
        //pr($pagination['page']);
        $results = [
            'result'=>true,
            'data'=>$data_list->toArray(),
            'data_count'=>count($data_list->toArray()),
            'pagination'=>$pagination,
            'table_th'=>$fields_defined,
            'filtration'=>array_values($this->filtration_defined),
            'select_list'=>$select_list,
            'posibility'=>$posibility,
            'top_actions'=>$top_actions,
            'conditions'=>(object) $conditions,
        ];
        
        $this->setJsonResponse($results);
    }

    /**
     * editace
     */
    public function edit($id=null){
        $select_list = $this->getSelectList(false);
        $this->loadComponent('vI');
        //pr($select_list);die();
        
        // save data
        if (isset($this->request->data['saveData'])){
            $saveData = $this->request->data['saveData'];
            $saveData = $this->vI->convertTime($saveData);
            
            $this->checkCode();
            
            $save_entity = $this->Deliverys->newEntity($saveData);
            //pr($save_entity); die();
            
            $this->vI->checkErrors($save_entity);

            if (!$resultDb = $this->Deliverys->save($save_entity)){
                $results = [
                    'result'=>false,
                    'message'=>__('Chyba uložení')
                ];    
            } else {
                $results = [
                    'result'=>true,
                    'message'=>__('Uloženo'),
                    'data'=>$resultDb
                ];  
            }
            
            

            $this->setJsonResponse($results);
        
        // load data
        } else {
            $defaultValues = [
                'nodelete'=>0,
            ];
            if ($id != null){
                // pokud je edit
                if ($id != 'new'){
                    $conditions = ['id'=>$id];
                    $data = $this->Deliverys->find()
                    ->select()
                    ->where($conditions)
                    ->first()
                ;
                // pokud je nova polozka
                } else {
                   
                    $data = $this->vI->emptyEntity('Deliverys'); 
                    
                }
                $data = $this->vI->convertLoadData($data,$defaultValues);        
                //pr($data);die();
                $validations = $this->vI->getValidations('Deliverys');
            } else {

            }

            $results = [
                'result'=>true,
                'validations'=>(isset($validations)?$validations:null),
                'data'=>(isset($data)?$data:''),
                'select_list'=>$this->vI->SelectList($select_list),
            ];  
            //pr($results);die();  
            $this->setJsonResponse($results);
        }
        $this->clearCache('delivery_data');
    }

    /**
     * kontrola existence kodu
     */
    private function checkCode(){
        //pr($this->request);
        $con = [
            'code'=>$this->request->data['saveData']['code'],
        ];
        if (!empty($this->request->data['saveData']['id'])){
            $con['id !='] = $this->request->data['saveData']['id'];
        }

        $find = $this->Deliverys->find()
        ->select([])
        ->where($con)
        ->first();
        if ($find){
            $results = [
                'result'=>false,
                'message'=>__('Kód produktu je již použit'),
            ];
            die(json_encode($results));  
    
        }
    }

    function sendApp(){
		$this->autoRender = false;
		$this->getSettings();
        $rozvozce_list_load = $this->Deliverys->find()
        ->toArray();
        $rozvozce_list = [];
        foreach($rozvozce_list_load AS $r){
            $rozvozce_list[] = [
                'Rozvozce'=>[
                    'id'=> $r->id,
                    'name'=> $r->name,
                    'code'=> $r->code,
                ]
            ];
        }
		
		$ch = curl_init();
		$post = array(
			'list'=>$rozvozce_list,
			'pokladna_id'=>$this->settings->data->system_id,
        );
        //pr($post);die();
		
		curl_setopt($ch, CURLOPT_URL, CLOUD_URL.'/mobile_add/');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		//pr($result);
		$result = (json_decode($result,true));
		curl_close ($ch);
		if ($result['r'] == true){
			die(json_encode(array('result'=>true,'message'=>'Odesláno do aplikace')));
			
		} else {
			die(json_encode(array('result'=>false,'message'=>'Chyba odeslání')));
	
		}
	}
}