<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Exception\Exception;
use Cake\I18n\I18n;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\Database\Type;
use Cake\Core\Configure;
Time::$defaultLocale = 'cs-CZ';
Type::build('date')->setLocaleFormat('yyyy-MM-dd');
Type::build('datetime')->setLocaleFormat('yyyy-MM-dd HH:mm:ss'); 

class AppController extends Controller
{
    public $paginate = [
        'limit' => 25,
        'order' => [
        ]
    ];

    public $return = [
        'data' => [],
        'lists' => [],
        'result' => false,
        'count' => null,
        'message' => null,
       // 'validations' => [],
       // 'validation_errors' => false // if validation error is thrown... then reutrn array with errors
    ];
   
    public $languages = ['cz'];
     
    public function initialize()
    {
        parent::initialize();
        
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Paginator', [
            'limit' => 25
        ]);
        
        if(isset($_GET['per-page']) && $_GET['per-page'] > 0 && $_GET['per-page'] <= 60){
            $this->paginate['limit'] = $_GET['per-page'];
        }
    }

   public function checkServerType(){
        if (strncasecmp(PHP_OS, 'WIN', 3) == 0) {
            $this->serverType = 'win';
        } else {
            $this->serverType = 'linux';
        }
   }

   private function checkLogged(){
    if ($this->Auth->user()){
        $this->set('loggedUser',$this->Auth->user());
        $this->loggedUser = $this->Auth->user();
    }
   }
    
    public function beforeFilter( \Cake\Event\Event $event){
        $this->load_select_config();
        $this->checkServerType(); 
        I18n::locale('cz');
         $loginAction = [ 'controller' => 'users', 'action' => 'login', 'prefix'=>false];
         $this->loadComponent('Auth', [
             //'unauthorizedRedirect' => ['controller' => 'pages', 'action' => 'login', 'prefix'=>false],
             'loginRedirect' => ['controller' => 'users', 'action' => 'ng_layout'],
             'loginAction' => $loginAction,
             'authorize' => [
                 'Controller'
             ],
         
             'authenticate' => [
             'Form' => [
                 'userModel' => 'Users',
                 'fields' => ['username' => 'username', 'password'=>'password'],
             ]
         ],
         'checkAuthIn'=> 'Controller.initialize'
         
         ]);
         $this->checkLogged();
        // session_start();
    }
    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
    
    /**
     * get settings
     */
    
    public function getSettings(){
        $this->loadModel('Settings');
        $this->settings = $this->Settings->getSetting();
    }

    

    /**
     * clear cache
     */
    public function clearCache($name){
        $files = scandir('../tmp/cache');
        foreach($files AS $f){
            if (strpos($f,'cake_'.$name) === 0){
                unlink('../tmp/cache/'.$f);
                //pr($f);
            }
        }
        return true;
    }

    

    
    protected function setJsonResponse($results){
        if (!isset($this->request->query['firstLoad'])){
            //unset($results['select_list']);
            unset($results['top_actions']);
            unset($results['filtration']);
            unset($results['table_th']);
        }
        //pr($this->request->query['firstLoad']);  
        
        $this->loadComponent('RequestHandler');
        $this->RequestHandler->renderAs($this, 'json');
        $this->response->type('application/json');
        $this->set([
			'results' => $results,
			'_serialize' => 'results'
        ]);
    }
        
    
    private function load_select_config(){
		$sc = Configure::read('select_config');
		foreach($sc AS $k=>$item){
			$this->$k = $item;
			$this->set($k,$item);
		}
    }
    
}
