<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

class ProductsController extends AppController{
    
    /**
     * vytvoreni select listu
     */
    public function getSelectList($check=true){
        if ($check == false || isset($this->request->query['firstLoad'])){
            $this->loadModel('ProductGroups');
            $this->product_groups_list = $this->ProductGroups->groupList();
            //pr($this->product_groups_list);die();
            return $select_list = [
                'no_yes'=>$this->no_yes,
                'product_groups_list'=>$this->product_groups_list,
            ];
        } else {
            return false;
        }  
        
        
    }

    /**
     * INDEX orders
     */
    public function index()
    {
        $this->loadComponent('vI');
        $select_list = $this->getSelectList();
        //pr($select_list);die();
        $conditions = [];
        $fields_defined = [
            0=>['col'=>'id','title'=>'ID','type'=>'text'],
            1=>['col'=>'name','title'=>'Název','type'=>'text'],
            2=>['col'=>'product_group_id','title'=>'Skupina','type'=>'list','list_data'=>$select_list['product_groups_list']],
            3=>['col'=>'price','title'=>'Cena','type'=>'text'],
            4=>['col'=>'amount','title'=>'Gramáž','type'=>'text'],
            5=>['col'=>'code','title'=>'Kod','type'=>'text'],
            6=>['col'=>'num','title'=>'Číslo','type'=>'text'],
            7=>['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
            //3=>['col'=>'zdroj_id','title'=>'Zdroj','type'=>'list','list_data'=>'zdroj_list'],
        ];

        $this->filtration_defined = [
            'id'=>['col'=>'id','title'=>'ID','type'=>'text'],
            'name'=>['col'=>'name','title'=>'Jméno','type'=>'text_like'],
            'num'=>['col'=>'num','title'=>'Číslo','type'=>'text_like'],
            'code'=>['col'=>'code','title'=>'Kod','type'=>'text_like'],
            'product_group_id'=>['col'=>'product_group_id','title'=>'Skupina','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['product_groups_list'])],
            
            //'created'=>['col'=>'created','title'=>'Vytvořeno','type'=>'date_range'],
            //'modified'=>['col'=>'modified','title'=>'Upraveno','type'=>'date'],
        ];
        
        if (isset($this->request->data['conditions'])){
            //pr($this->request->data['conditions']);
            $conditions = $this->vI->convertConditions($conditions,$this->request->data['conditions'],$this->filtration_defined);
            //pr($conditions);die();
        }
        
       
        $posibility = [
            0=>['link'=>'/api/status/Products/','title'=>__('Status'),'class'=>'fa-check-circle-o','type'=>'ajax','params'=>'status'],
            1=>['link'=>'./edit/','title'=>__('Editovat'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
            2=>['link'=>'/api/trash/Products/','title'=>__('Smazat'),'class'=>'fa-trash','type'=>'ajax','params'=>'trash','confirm'=>__('Opravdu smazat?')],
        ];
        
        $top_actions = [
            0=>['link'=>'./edit/','title'=>__('Nová položka'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
        ];

        
        $fields = $this->vI->fieldsConvert($fields_defined);
        //pr($fields);die();
        
        $mapper = function ($data, $key, $mapReduce) {
            //$data->operating = (($data->operating == '')?0:1);
            //$data->problem = (($data->problem == '')?0:1);

            
            $mapReduce->emit($data);  
        };

        $query = $this->Products->find()
            ->select($fields)
            ->where($conditions)
            ->cache(function ($query) {
				return 'product_data-' . md5(serialize($query->clause('where')));
            })
            ->mapReduce($mapper)
        ;

        
        //$data = $query;
        //pr($query->toArray());die();
        
        
        $this->loadComponent('Paginator');
        $data_list = $this->paginate($query);
        
        
        
        $pagination = $this->vI->convertPagination();
        //pr($pagination['page']);
        $results = [
            'result'=>true,
            'data'=>$data_list->toArray(),
            'data_count'=>count($data_list->toArray()),
            'pagination'=>$pagination,
            'table_th'=>$fields_defined,
            'filtration'=>array_values($this->filtration_defined),
            'select_list'=>$select_list,
            'posibility'=>$posibility,
            'top_actions'=>$top_actions,
            'conditions'=>(object) $conditions,
        ];
        
        $this->setJsonResponse($results);
    }


    /**
     * kontrola existence kodu
     */
    private function checkCode(){
        //pr($this->request);
        $con = [
            'code'=>$this->request->data['saveData']['code'],
        ];
        if (!empty($this->request->data['saveData']['id'])){
            $con['id !='] = $this->request->data['saveData']['id'];
        }

        $find = $this->Products->find()
        ->select([])
        ->where($con)
        ->first();
        if ($find){
            $results = [
                'result'=>false,
                'message'=>__('Kód produktu je již použit'),
            ];
            die(json_encode($results));  
    
        }
    }

    /**
     * editace
     */
    public function edit($id=null){
        $select_list = $this->getSelectList(false);
        $this->loadComponent('vI');
        //pr($select_list);die();
        
        // save data
        if (isset($this->request->data['saveData'])){
            $saveData = $this->request->data['saveData'];
            $saveData = $this->vI->convertTime($saveData);

            $this->checkCode();

            $save_entity = $this->Products->newEntity($saveData);
            //pr($save_entity); die();
            
            $this->vI->checkErrors($save_entity);

            if (!$resultDb = $this->Products->save($save_entity)){
                $results = [
                    'result'=>false,
                    'message'=>__('Chyba uložení')
                ];    
            } else {
                $results = [
                    'result'=>true,
                    'message'=>__('Uloženo'),
                    'data'=>$resultDb
                ];  
            }
            
            

            $this->setJsonResponse($results);
        
        // load data
        } else {
            $defaultValues = [
                'nodelete'=>0,
            ];
            if ($id != null){
                // pokud je edit
                if ($id != 'new'){
                    $conditions = ['id'=>$id];
                    $data = $this->Products->find()
                    ->select()
                    ->where($conditions)
                    ->first()
                ;
                // pokud je nova polozka
                } else {
                   
                    $data = $this->vI->emptyEntity('Products'); 
                    
                }
                $data = $this->vI->convertLoadData($data,$defaultValues);        
                //pr($data);die();
                $validations = $this->vI->getValidations('Products');
            } else {

            }

            $results = [
                'result'=>true,
                'validations'=>(isset($validations)?$validations:null),
                'data'=>(isset($data)?$data:''),
                'select_list'=>$this->vI->SelectList($select_list),
            ];  
            //pr($results);die();  
            $this->setJsonResponse($results);
        }
        $this->clearCache('product_data');
    }
}