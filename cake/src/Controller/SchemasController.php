<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Datasource\TableSchemaInterface;
use Cake\Database\Schema\Table;
use Cake\Datasource\ConnectionManager;

class SchemasController extends AppController{
    
    /**
     * create table
     */
    private function createTable($table){
        $query = 'CREATE TABLE `'.$table.'` ( `id` INT NOT NULL AUTO_INCREMENT , PRIMARY KEY (`id`)) ENGINE = MyISAM;';
        $this->db->execute($query);
        //pr($query);
    }

    public function setAutoincrement(){
        $this->db = ConnectionManager::get('default');
        $query = 'ALTER TABLE orders AUTO_INCREMENT=40000';
        $this->db->execute($query);

        die(json_encode(['result'=>true,'message'=>'Nastaveno na 40 000']));
    }


    /**
     * add column to table
     */
    private function addColumn($table,$col_name,$col_params){
        
        if ($col_params['type'] != 'varchar' && $col_params['type'] != 'text'){

            $query = 'ALTER TABLE `'.$table.'` ADD IF NOT EXISTS `'.$col_name.'` '.$col_params['type'].(isset($col_params['length'])?'('.$col_params['length'].') '.(isset($col_params['disable_UNSIGNED'])?'':'UNSIGNED').' ':'').' '.(isset($col_params['null'])?'NULL':'').(isset($col_params['default'])?' DEFAULT '.$col_params['default']:'').';';
        } else {
        
            $query = 'ALTER TABLE `'.$table.'` ADD IF NOT EXISTS `'.$col_name.'` '.$col_params['type'].(isset($col_params['length'])?'('.$col_params['length'].')':'').' CHARACTER SET utf8 COLLATE utf8_czech_ci '.(isset($col_params['null'])?'NULL':'').(isset($col_params['default'])?' DEFAULT '.$col_params['default']:'').';';
        }
        $this->db->execute($query);
        //pr($query);
    }

    /**
     * remove column from table
     */
    private function removeColumn($table,$col_name){
        $query = 'ALTER TABLE `'.$table.'` DROP COLUMN `'.$col_name.'`;';
        $this->db->execute($query);
        //pr($col_name);
    }
    

    /**
     * add index to table
     */
    private function addIndex($table,$indexName,$indexData){
        //$query .= 'ALTER TABLE `'.$table.'` ADD INDEX(`'.implode($indexData['col'],',').'`);';
        $query = 'SHOW INDEX FROM '.$table.';';
        $result = $this->db->execute($query)->fetchAll('assoc');
        $index_list = [];
        if ($result){
            foreach($result AS $r){
                $index_list[] = $r['Column_name'];
            }
        }
        //pr($index_list);
        if ($index_list){
            foreach($index_list AS $ind){
                if ($ind != 'id' && $ind == $indexName){
                    $query = 'DROP INDEX '.$ind.' ON '.$table.';';
                    $this->db->execute($query);
                }
                
            }
        }
        $query = 'CREATE INDEX '.$indexName.' ON '.$table.' ('.implode($indexData['col'],',').'); ';
        $this->db->execute($query);
       // pr($query);
    }

    /**
     * column list
     */
    private function colList($table){
        $query = 'SHOW COLUMNS FROM '.$table.';';
        $result = $this->db->execute($query)->fetchAll('assoc');
        $list = [];
        foreach($result AS $r){
            $list[] = $r['Field'];
        }
        //pr($list);
        return $list;
    }

    /**
     * create table cols
     */
    private function createCols($table,$sql){
        $cols = $this->colList($table);
        //$tableSchema = $this->collection->describe($table);
        //$cols = $tableSchema->columns();
        //pr($cols);
        //die();

        if (isset($sql['cols'])){
            $currentCol = [];
            $importCol = [];
            foreach($sql['cols'] AS $colName=>$colData){
                $currentCol[] = $colName;
                
                if (!in_array($colName,$cols)){
                    $this->addColumn($table,$colName,$colData);
                } else {
                    
                }
            }
            
            // vymazani rozdilu
            $diff = array_diff($cols,$currentCol);
            if (count($diff)>0){
                foreach($diff AS $d){
                    $this->removeColumn($table,$d);
                }
            }
            //pr($diff);
            
        }

        // pridani indexu
        if (isset($sql['indexs'])){
            foreach($sql['indexs'] AS $indexName=>$indexData){
                //pr($indexName);
                $this->addIndex($table,$indexName,$indexData);
            }
        }

        // pridani indexu
        if (isset($sql['defaultValues']) && isset($this->defaulValues)){
            foreach($sql['defaultValues'] AS $key=>$data){
                $this->addValue($table,$data);
            }
        }
    }

    /**
     * insert default value
     */
    private function addValue($table,$data){
        $keys = array_keys($data);
        $values = array_values($data);
        
        
        $query = "SELECT EXISTS(SELECT 1 FROM ".$table." WHERE id=".$values[0].") AS exist";
        $res = $this->db->execute($query)->fetchAll('assoc');
        
        if ($res[0]['exist'] == 0){
            $query = "INSERT INTO ".$table." (".implode($keys,',').") VALUES (\"".implode($values,'","')."\");";
            $this->db->execute($query);
        }
        
        //pr($data);
        //die();
    }

    /**
     * load list from folder !sql
     * 
     */
    private function loadFolder(){
        $sql_list = [];
        $files = scandir('../src/!sql');
        //pr(getcwd());
        unset($files[0]);
        unset($files[1]);
        //pr($files);
        foreach($files AS $file){
            require_once('../src/!sql/'.$file);
            $sql_list = array_merge($sql_list,$sql);
        }
        // pr($sql_list);
        return ($sql_list);
        //die('a');
    }

    /**
     * spusteni scriptu
     * pokud parametr true tak importuje vychozi data
     * 
     */
    public function createSql($defaulValues=false){
        
        if ($defaulValues == true){
            $this->defaulValues = true;
        }

        $this->db = ConnectionManager::get('default');
        $this->collection = $this->db->schemaCollection();
        
        // Get the table names
        $tables = $this->collection->listTables();
        
        $sql_list = $this->loadFolder();

        //pr($tables);
        //pr($sql_list);
        if (isset($sql_list) && count($sql_list)>0)
        foreach($sql_list AS $table=>$sql){
            if (in_array($table,$tables)){
                $this->createCols($table,$sql);
                
        
            } else {
                $this->createTable($table);
                
                $this->createCols($table,$sql);
            }
            
        }
        $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
        if (!isset($this->outResult)){
            $this->outResult = [];
        }
        $this->logs(true,'SQL: Vytvoreno tabulek: '.count($sql_list));
        $this->logs(true,"SQL: Vlozeny výchozí data: ".(isset($this->defaulValues)?'ANO':'NE'));
        $this->outResult['time'] = round($time,2);
        
        //$result = "Process Time: ".round($time,2)."s , Vytvoreno tabulek: ".count($sql_list).", Vlozeny výchozí data: ".(isset($this->defaulValues)?'ANO':'NE');
        
        //pr($result);
        $this->autoRender = false;
        
    }

    function logs($result,$value){
        if (!isset($this->outResult)){
            $this->outResult = [];
        }
        
        $this->outResult['log'][] = date('Y-m-d H:i:s').': '.$value;
        $this->outResult['result'] = $result;
        
    }
      /**
     *  globalni funkce na vytvoreni dump
     * */
    function createDump(){
        
        
        $this->outResult = [];

        if ($this->serverType == 'win'){
            $file = "D:\\xampp\\mysql\\bin\\mysqldump.exe";
            //$outPath = "D:\\xampp\\htdocs\\pokladna\\src\\cake\\webroot\\uploaded\\dump\\";
            $outPath = ".\\uploaded\\dump\\";
       
        } else {
            $file = "mysqldump";
            $outPath = "./uploaded/dump/";
            $outPath = "./uploaded/";
            
        }
        // pr($outPath);
        $outFile = 'dump_'.date('Y-m-d_H-i-s').'.sql';
        $outFile = 'dump.sql';
        
        $script = $file.'  -u '.configSqlUser.' -p'.configSqlPass.' -h '.configSqlServer.' '.configSqlDb.' > '.$outPath.$outFile;
        exec($script." 2>&1",$out);
        
        $this->logs(true,(isset($out[0])?$out[0]:'Dump vytvořen v lokální kopii '.$outPath.$outFile));
        //$this->outResult['result'] = true;
        
        $zipFile = $this->createZip($outPath,$outFile);
        if ($zipFile){
            $this->uploadFtp($outPath,$zipFile);
            
        }
        //$out = system($file);
        //pr($script);
        $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
        $this->outResult['time'] = $time;
        //pr($this->outResult);
        $this->autoRender = false;
        if ($this->outResult['result'] == true){
            $this->outResult['message'] = 'Záloha vytvořena';
            
        } else {
            $this->outResult['message'] = 'Chyba vytvoření zálohy';
            
        }
        die(json_encode($this->outResult));
    }

      /**
     *  vytvoreni ZIP dump
     * */
    public function createZip($file_path=null,$file_name=null){
        if ($file_name == null){
            $file_path = ".".DIRECTORY_SEPARATOR."uploaded".DIRECTORY_SEPARATOR."dump".DIRECTORY_SEPARATOR;
            $file_name = "dump.sql";
        }
        
        $this->autoRender = false;
        
        $file_zip = strtr($file_name,['sql'=>'zip']);
        //print_r($file_path.$file_zip);
        unlink($file_path.$file_zip);
        if (!file_exists($file_path.$file_zip)){
            $zip_file = fopen($file_path.$file_zip, "w");
        } 
        //$f = file_get_contents($file_path.$file_name);
        // print_r($file_path.$file_zip);
        $zip = new \ZipArchive;
        if ($zip->open($file_path.$file_zip) === TRUE) {
            $zip->addFile(realpath($file_path.$file_name), 'dump.sql');
            //$zip->close();
            //die('a');
            $this->logs(true,'ZIP vytvořen '.$file_path.$file_zip);
            //$this->outResult['result'] = true;
            return $file_zip;
        } else {
            
            $this->logs(false,'Chyba vytvoření ZIP '.$file_path.$file_zip);
            //$this->outResult['result'] = false;
        }
        
    }

      /**
     *  UNZIP dump from ftp
     * */
    private function unZip($file){
        $zip = new \ZipArchive;
        $res = $zip->open($file);
        if ($res === TRUE) {
          $zip->extractTo('./uploaded/dump/');
          $zip->close();
          $this->logs(true,'UNZIP v poradku '.$file);
          //$this->outResult['result'] = true;
          $this->importDump();
        } else {
            $this->logs(false,'Chyba unzip! '.$file);
            //$this->outResult['result'] = false;
        }
    }

    /**
     *  import dump
     * */
    public function importDump($outFile=null){
        if ($outFile == null){
            $outFile = 'dump.sql';
        }
        
        // :\xampp\mysql\bin>mysql -u {DB_USER} -p {DB_NAME} < path/to/file/ab.sql
        if ($this->serverType == 'win'){
            $file = "D:\\xampp\\mysql\\bin\\mysql.exe";
            //$outPath = "D:\\xampp\\htdocs\\pokladna\\src\\cake\\webroot\\uploaded\\dump\\";
            $outPath = ".\\uploaded\\dump\\";
       
        } else {
            $file = "mysql";
            $outPath = "./uploaded/dump/";
            
        }
        $this->autoRender = false;

        $script = $file.'  -u '.configSqlUser.' -p'.configSqlPass.' -h '.configSqlServer.' '.configSqlDb.' < '.realPath($outPath).DIRECTORY_SEPARATOR.$outFile;
        exec($script." 2>&1",$out);
        $this->logs(true,(isset($out[0])?$out[0]:'Dump obnoven z lokální kopie '.$outPath.$outFile));
        //$this->outResult['result'] = true;

        //print_r($script);
    }

    /**
     * get dump list
     */
    public function dumpList(){
        $this->autoRender = false;
        // set up basic connection
        $conn_id = ftp_connect(configFtpServer);
        
        // login with username and password
        $login_result = ftp_login($conn_id, configFtpUser, configFtpPass);
        
        // get contents of the current directory
        $files = ftp_nlist($conn_id, "./".configFtpFolder);
        unset($files[0]);
        unset($files[1]);
        rsort($files);
        $path = 'http://chacharint.fastest.cz/uploaded/dump/';
        $files_path = [];
        $files_name = [];
        foreach($files AS $k=>$f){
            $name_data = strtr($f,['dump.zip'=>'','_'=>' ']);
            $name_data = explode(' ',$name_data);
            $name = date('d.m.Y',strtotime($name_data[0])). ' '.date('H:i:s',strtotime(strtr($name_data[1],['-'=>':'])));
            $files_path[$k] = [
                'file'=>$path.$f,
                'name'=>$name,
                'file_name'=>$f,
            ];
        }
        // pr($files);
        if (count($files) == 0){
            die(json_encode(['result'=>false,'message'=>'Nenalezeny žádné zálohy']));
            
        }
        //pr($files_path);
        die(json_encode(['result'=>true,'files'=>$files,'files_path'=>$files_path,'path'=>$path.configFtpFolder]));
    }

    // restore dump
    public function restoreDump($file){
        $this->autoRender = false;
        $this->outResult = [];             
        // define some variables
        $local_file = '.'.DIRECTORY_SEPARATOR.'uploaded'.DIRECTORY_SEPARATOR.'dump'.DIRECTORY_SEPARATOR.'restoreDump.zip';
        $server_file = configFtpFolder.'/'.$file;

        // set up basic connection
        $conn_id = ftp_connect(configFtpServer);

        // login with username and password
        $login_result = ftp_login($conn_id, configFtpUser, configFtpPass);

        // try to download $server_file and save to $local_file
        if ($res = @ftp_get($conn_id, $local_file, $server_file, FTP_BINARY)) {
            
            $this->logs(true,"Stazeno z FTP a ulozeno do $local_file");
            //$this->outResult['result'] = true;
            $this->unZip($local_file);
        
        } else {
            $this->logs(false,"Problem stazeni z FTP");
            //$this->outResult['result'] = false;
        }

        // close the connection
        ftp_close($conn_id);
        $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
        $this->outResult['time'] = $time;
        //pr($this->outResult);
        if ($this->outResult['result'] == true){
            $this->outResult['message'] = 'Záloha obnovena';
            
        } else {
            $this->outResult['message'] = 'Chyba obnovení zálohy';
            
        }
        die(json_encode($this->outResult));
    }

    // nahrani DUMP.zip na FTP
    private function uploadFtp($path,$file_name){
        $file = $path.$file_name;
        
        
        $remote_file = configFtpFolder.'/'.date('Y-m-d_H-i-s').$file_name;
        //$remote_file = configFtpFolder.'/'.$file_name;
        //pr($file);die();
        
        // set up basic connection
        $conn_id = ftp_connect(configFtpServer);
        
        
        // login with username and password
        $login_result = ftp_login($conn_id, configFtpUser, configFtpPass);
        
        if (ftp_nlist($conn_id, configFtpFolder) == false) {
            ftp_mkdir($conn_id, configFtpFolder); 
        }

        //pr($file);
        // upload a file
        if (ftp_put($conn_id, $remote_file, $file, FTP_BINARY)) {
            $this->logs(true,"Nahráno na FTP $file");
            //$this->outResult['result'] = true;
        } else {
            $this->logs(false,"Problém s nahráním na FTP $remote_file $file");
            //$this->outResult['result'] = false;
        }

        // promazani starych zaloh
        $files = ftp_nlist($conn_id, "./".configFtpFolder);
        unset($files[0]);
        unset($files[1]);
        rsort($files);
        unset($files[0]);
        unset($files[1]);
        unset($files[3]);
        foreach($files AS $file){
            if (ftp_delete($conn_id, "./".configFtpFolder.'/'.$file)) {
                $this->logs(true,"Smazano z FTP: $file");
                
            } else {
                $this->logs(true,"Chyba smazani z FTP: $file");
                
            }
        }        
        // close the connection
        ftp_close($conn_id);
    }

    /**
     * kontrola aktualnosti verze + nasledne spusteni update
     */
    public function checkUpdate(){
        header('Content-Type: text/html; charset=utf-8');
        $this->autoRender = false;
        $this->outResult = [];
        
        
        // spusteni kontroly z GIT
        $this->checkGitPull();

        // pokud je aktualizovano spustit aktualizaci SQL
        if($this->outResult['result'] == true){
            $this->createSql();
            $this->updateChmod();
            
        }
        //$this->outResult['result'] = true;
        //pr($this->outResult);
        
        die(json_encode($this->outResult));
    }

    public function updateChmod($direct=false){
        $script = "sudo chmod -R 0777 /var/www/cake/tmp";
        exec($script." 2>&1",$out);
        if ($direct) pr($out);
        
        $script = "sudo chmod -R 0777 /var/www/cake/webroot/uploaded";
        exec($script." 2>&1",$out);
        if ($direct) pr($out);
        
        $script = "sudo chmod -R 0777 /var/www/cake/webroot/configPokladna.php";
        exec($script." 2>&1",$out);
        if ($direct) pr($out);
        

        $script = "sudo chmod -R 0777 /var/www/cake/logs";
        exec($script." 2>&1",$out);
        
        if ($direct) pr($out);
        
        
        if ($direct){
            die('chmod change');
        }
       
    }

    public function testFce(){
        //exec('ping seznam.cz',$out);
        //exec('ping google.com',$out);
        //pr($out);
        die('a');
    }

    /**
     * overeni aktualnosti z git
     */
    public function checkGitPull(){
        $this->autoRender = false;
        $script = "git pull ".configGitRepository;
        $script2 = "git reset --hard HEAD";
        
        if (!isset($this->outResult)){
            $this->outResult = [];
        }

        exec($script2." 2>&1",$out2);
        exec($script." 2>&1",$out);
        $msg = (isset($out[0])?$out[0]:'Chyba ověření nové verze systému');
        
        if (isset($out)){
			foreach($out AS $o){
				if ($o == 'Already up-to-date.'){
					$msg = $o;
				}
			}
		}
        //pr($script);
        if (substr($msg,0,6) == 'fatal:'){
            $this->logs(false,$msg);
            $this->outResult['result'] = false;
        } else {
            if ($msg == 'Already up-to-date.'){
                $msg = 'Verze systému je aktuální';
              //  $this->outResult['needRefresh'] = true;
                $this->logs(false,$msg);
                
            } else {
                $this->logs(true,$msg);
                $this->outResult['needRefresh'] = true;
                $this->outResult['result'] = true;
                
            }
            
        }
        
        $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
        $this->outResult['time'] = round($time,2);
        
    }
}
