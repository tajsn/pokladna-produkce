<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

class PagesController extends AppController
{
    
    // zmena stavu polozky
    public function changeTrash($model,$id){
        $this->loadModel($model);
        $find = $this->$model->find()->where(['id'=>$id])->select()->first();
        //pr($find);die();
        if ($find){
            if (isset($find->nodelete) && $find->nodelete == 1){
                $results = [
                    'result'=>false,
                    'message'=>__('Chyba smazání, položku je zakázáno smazat'),
                    'trash'=>$id,
                ];
                
            } else {
                $this->$model->trash($find);
                $results = [
                    'result'=>true,
                    'message'=>(($find)?__('Položka byla smazána'):__('Chyba smazání, položka nenalezena')),
                    'trash'=>$id,
                ];
                        
            }
        } else {
            $results = [
                'result'=>false,
                'message'=>__('Chyba smazání, položka nenalezena'),
                'trash'=>$id,
            ];
        }
        $this->setJsonResponse($results);
        //$this->model->tra
    
    }

    // zmena stavu polozky
    public function changeStatus($model,$id,$status){
        $this->loadModel($model);
        //die('a');
        $this->$model->updateAll(
            ['status' => $status], // fields
            ['id' => $id]
        );
        $results = [
            'result'=>true,
            'message'=>(($status == 1)?__('Položka aktivována'):__('Položka deaktivována')),
            'status'=>$status,
        ];
        $this->setJsonResponse($results);
        
    }
    

    public function oldImport($type,$offset=0){
        $time_start = microtime(true); 
        $limit = 2000;
        $limit2 = 3000;
        //pr($type);
        if ($type == 'users'){
            $this->loadModel('OldDeliverys');
            $mapper = function ($data, $key, $mapReduce) {
                //$data->products = unserialize($data->products);
                //$data->zakaznik = unserialize($data->zakaznik);
                $mapReduce->emit($data);
            };
            $load = $this->OldDeliverys->find()
            ->where(['kos'=>0])
            ->limit($limit2)
            ->offset($offset)
            ->order('id DESC')
            ->mapReduce($mapper)
            ->toArray();
            //pr($load);
            $save_user = [];
            if ($load){
                $this->getSettings();
                foreach($load AS $l){
                    if ($l->id != 1)
                    $save_user[] = [ 
                        'id'=>$l->id,
                        'name'=>$l->name,
                        'code'=>$l->code,
                        'created'=>$l->created,
                        'modified'=>$l->updated,
                    ];
                 }
                 $this->loadModel('Deliverys');
                 $saveEntities = $this->Deliverys->newEntities($save_user);
                 //pr($saveEntities);die();
                 foreach($saveEntities AS $saveEn){
                     $this->Deliverys->save($saveEn);
                 }
            }
           $count = count($save_user);
            
        }
        
        if ($type == 'orders'){
            $this->loadModel('OldOrders');
            $mapper = function ($data, $key, $mapReduce) {
                $data->products = unserialize($data->products);
                $data->zakaznik = unserialize($data->zakaznik);
                $mapReduce->emit($data);
            };
            $load = $this->OldOrders->find()
            ->where()
            ->limit($limit2)
            ->offset($offset)
            ->order('id DESC')
            ->mapReduce($mapper)
            ->toArray();
           // pr($load);
            $save_order = [];
            if ($load){
                $this->getSettings();
                foreach($load AS $l){
                    $order_items = [];
                    foreach($l->products AS $oi){
                        $order_items[] = [
                            'order_id'=>$l->id,
                            'name'=>$oi['name'],
                            'amount'=>$oi['ks'],
                            'count'=>$oi['ks'],
                            'code'=>$oi['code'],
                            'num'=>$oi['num'],
                            'free'=>$oi['zdarma'],
                            'price'=>$oi['price_row'],

                        ];
                    }
                    $save_order[] = [
                        'id'=>$l->id,
                        'name'=>$l->name,
                        'client_id'=>$l->pokladna_id,
                        'web_id'=>$l->web_id,
                        'source_id'=>($l->zdroj_id == 0)?1:$l->zdroj_id,
                        'created'=>$l->created,
                        'modified'=>$l->updated,
                        'street'=>$l->zakaznik['ulice'],
                        'city'=>$l->zakaznik['mesto'],
                        'area_id'=>$l->oblast_id,
                        'area_name'=>$l->oblast_name,
                        'company'=>$l->zakaznik['firma'],
                        'note'=>$l->note,
                        'pay_points'=>$l->platba_body,
                        'close_order'=>($l->kos == 1)?1:0,
                        'createdTime'=>$l->created_time,
                        'distributor_id'=>$l->rozvozce_id,
                        'storno'=>$l->storno,
                        'storno_type'=>$l->storno_type,
                        'xml_type_id'=>($l->xml_type>0)?$l->xml_type:null,
                        'dj_id'=>$l->dj_id,
                        'operating'=>$l->provozni,
                        'system_id'=>$this->settings->data->system_id,
                        'order_items'=>$order_items,
                        
                    ];
                }
                $this->loadModel('Orders');
                $saveEntities = $this->Orders->newEntities($save_order);
                //pr($saveEntities);die();
                foreach($saveEntities AS $saveEn){
                    $this->Orders->save($saveEn);
                }
                //pr($save_order);
                $count = count($save_order);
            }
        }
        
        if ($type == 'clients'){
            $this->loadModel('OldClients');
            $load = $this->OldClients->find()
            ->where()
            ->contain(['OldClientAddressas'])
            ->limit($limit)
            ->offset($offset)
            ->order('OldClients.id DESC')
            ->toArray();
            //pr($load);die();
            $save_client = [];
            if ($load){
                foreach($load AS $l){
                    $phone = substr($l->telefon,3,20);
                    $phone_pref = substr($l->telefon,0,3);

                    $address = [];
                    
                    if (isset($l->old_client_addressas)){
                        foreach($l->old_client_addressas AS $adr){
                            $address[] = [
                                'type_id'=>1,
                                'client_id'=>$adr->pokladna_id,
                                'street'=>substr($adr->ulice,0,40),
                                'city'=>$adr->mesto,
                                'zip'=>'',
                                'company'=>$adr->company,

                            ];
                        }
                    }

                    $save_client[] = [
                        'id'=>$l->id,
                        'name'=>$l->name,
                        'created'=>$l->created,
                        'modified'=>$l->updated,
                        'first_name'=>$l->jmeno,
                        'last_name'=>$l->prijmeni,
                        'email'=>$l->email,
                        'phone'=>substr($phone,0,9),
                        'phone_pref'=>$phone_pref,
                        'operating'=>$l->provozni,
                        'problem'=>$l->problem,
                        'problem_message'=>substr($l->problem_text,0,20),
                        'client_addresses'=>$address,
                    ];
                }
                $this->loadModel('Clients');
                $saveEntities = $this->Clients->newEntities($save_client);
                foreach($saveEntities AS $saveEn){
                    $this->Clients->save($saveEn);
                }
            }
            // pr($saveEntities);
            // pr($save_client);
            // pr($load);
            $count = count($save_client);
        }
        $time_end = microtime(true);
        die(json_encode(['result'=>true,'count'=>$count,'time'=>round(($time_end - $time_start),2).'s']));
    
    }

    public function test(){
		$this->viewBuilder()->layout("angular");
		$this->set('hp','test hp');
		$this->loadModel('Users');
		$data = $this->Users->find()->toArray();
		$this->set('data',$data);
		//pr($data);
		$this->set('_serialize', ['data']);
	}
    
    
}
