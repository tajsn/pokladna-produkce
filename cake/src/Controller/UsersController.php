<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;

class UsersController extends AppController{
    
    // check logged user function
    public function checkLogged($user=null){
        
        if (!isset($this->request->data['username'])){
            $results = [
                'result'=>false,
                'message'=>__('Není definován uživatel'),
                
            ];    
        } else {
            $this->loadComponent('Auth');
            //pr($this->Auth);
            if ($user = $this->Auth->user()){
                //pr($user);die();
                if ($user['hash'] != sha1($_SERVER['REMOTE_ADDR'].$user['id'])){
                    $results = [
                        'result'=>false,
                        'message'=>__('Chyba ověření hash'),
                        
                    ];    
                } else {
                    $results = [
                        'result'=>true,
                        'message'=>__('Ověření v pořádku'),
                        
                    ];
                }
            } else {
                $results = [
                    'result'=>false,
                    'message'=>__('Není přihlášen uživatel'),
                    
                ];  
            }

        }
        $this->setJsonResponse($results);
       
    }
    // login function
    public function login(){
        //pr($this->request);
        //$this->request->data['username'] = 'test';
        //$this->request->data['password'] = 'testt';
        if (!$this->request->is('send')) {
       
            
            if ($this->request->data['username'] == 'superadmin' && $this->request->data['password'] == 'pokladna159'){
                $user = [
                    'id'=>-1,
                    'name'=>'Superadmin',
                    'username'=>'Superadmin',
                    'user_group_id'=>1,
                ];

            } else {

                $user = $this->Auth->identify();
                //pr($user);die();
            }
            
            if($user){
                $user['hash'] = sha1($_SERVER['REMOTE_ADDR'].$user['id']);
                $this->Auth->setUser($user);
                $userLogged = $this->Auth->user();
                //  pr($userLogged);
                $results = [
                    'result'=>true,
                    'message'=>__('Přihlášení v pořádku'),
                    'data'=>$this->Auth->user()
                ];
            } else {
  
                $results = [
                    'result'=>false,
                    'message'=>__('Chybné přihlašovací údaje'),
                    
                ];              
            }
        
        } else {

            $results = [
                'result'=>false,
                'message'=>__('Nepřihlášen'),
                
            ];
        }
        
        
        $this->setJsonResponse($results);
    }

    public function saveUser()
    {
		$save = $this->Users->newEntity([
            'name'=>'test',
            'username'=>'test',
            'password'=>'testt',
        ]);
        pr($save);
        $result = $this->Users->save($save);
        pr($result);
        die('save user');
    }


/**
     * vytvoreni select listu
     */
    public function getSelectList($check=true){
        if ($check == false || isset($this->request->query['firstLoad'])){
            $this->loadModel('ProductGroups');
            $this->product_groups_list = $this->ProductGroups->groupList();
            //pr($this->product_groups_list);die();
            return $select_list = [
                'no_yes'=>$this->no_yes,
                'user_group_list'=>$this->user_group_list,
            ];
        } else {
            return false;
        }  
        
        
    }

    /**
     * INDEX orders
     */
    public function index()
    {
        $this->loadComponent('vI');

        $select_list = $this->getSelectList();
        //pr($select_list);die();
        $conditions = [];
        $fields_defined = [
            0=>['col'=>'id','title'=>'ID','type'=>'text'],
            1=>['col'=>'name','title'=>'Název','type'=>'text'],
            2=>['col'=>'user_group_id','title'=>'Skupina','type'=>'list','list_data'=>$select_list['user_group_list']],
            3=>['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
            //3=>['col'=>'zdroj_id','title'=>'Zdroj','type'=>'list','list_data'=>'zdroj_list'],
        ];

        $this->filtration_defined = [
            'id'=>['col'=>'id','title'=>'ID','type'=>'text'],
            'name'=>['col'=>'name','title'=>'Jméno','type'=>'text_like'],
            'user_group_id'=>['col'=>'user_group_id','title'=>'Skupina','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['user_group_list'])],
            
            //'created'=>['col'=>'created','title'=>'Vytvořeno','type'=>'date_range'],
            //'modified'=>['col'=>'modified','title'=>'Upraveno','type'=>'date'],
        ];
        
        if (isset($this->request->data['conditions'])){
            //pr($this->request->data['conditions']);
            $conditions = $this->vI->convertConditions($conditions,$this->request->data['conditions'],$this->filtration_defined);
            //pr($conditions);die();
        }
        
       
        $posibility = [
            0=>['link'=>'/api/status/Users/','title'=>__('Status'),'class'=>'fa-check-circle-o','type'=>'ajax','params'=>'status'],
            1=>['link'=>'./edit/','title'=>__('Editovat'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
            2=>['link'=>'/api/trash/Users/','title'=>__('Smazat'),'class'=>'fa-trash','type'=>'ajax','params'=>'trash','confirm'=>__('Opravdu smazat?')],
        ];
        
        $top_actions = [
            0=>['link'=>'./edit/','title'=>__('Nová položka'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
        ];

        
        $fields = $this->vI->fieldsConvert($fields_defined);
        //pr($fields);die();
        
        $mapper = function ($data, $key, $mapReduce) {
            //$data->operating = (($data->operating == '')?0:1);
            //$data->problem = (($data->problem == '')?0:1);

            
            $mapReduce->emit($data);  
        };

        $query = $this->Users->find()
            ->select($fields)
            ->where($conditions)
            ->cache(function ($query) {
				return 'users_data-' . md5(serialize($query->clause('where')));
            })
            ->mapReduce($mapper)
        ;

        
        //$data = $query;
        //pr($query->toArray());die();
        
        
        $this->loadComponent('Paginator');
        $data_list = $this->paginate($query);
        
        
        
        $pagination = $this->vI->convertPagination();
        //pr($pagination['page']);
        $results = [
            'result'=>true,
            'data'=>$data_list->toArray(),
            'data_count'=>count($data_list->toArray()),
            'pagination'=>$pagination,
            'table_th'=>$fields_defined,
            'filtration'=>array_values($this->filtration_defined),
            'select_list'=>$select_list,
            'posibility'=>$posibility,
            'top_actions'=>$top_actions,
            'conditions'=>(object) $conditions,
        ];
        
        $this->setJsonResponse($results);
    }


    /**
     * editace
     */
    public function edit($id=null){
        $select_list = $this->getSelectList(false);
        $this->loadComponent('vI');
        //pr($select_list);die();
        
        // save data
        if (isset($this->request->data['saveData'])){
            $saveData = $this->request->data['saveData'];
            $saveData = $this->vI->convertTime($saveData);
            //pr($saveData);die();
            
            if ($saveData['password_tmp'] == $saveData['password']){
                unset($saveData['password_tmp']);
                unset($saveData['password']);
                unset($saveData['password2']);
            } else {
                if ($saveData['password'] != $saveData['password2']){
                    $results = [
                        'result'=>false,
                        'message'=>__('Hesla se neshodují')
                    ];    
                    die(json_encode($results));
                }
            }

         
            $save_entity = $this->Users->newEntity($saveData);
            
            //pr($save_entity); die();
            
            $this->vI->checkErrors($save_entity);

            if (!$resultDb = $this->Users->save($save_entity)){
                $results = [
                    'result'=>false,
                    'message'=>__('Chyba uložení')
                ];    
            } else {
                $results = [
                    'result'=>true,
                    'message'=>__('Uloženo'),
                    'data'=>$resultDb
                ];  
            }
            
            

            $this->setJsonResponse($results);
        
        // load data
        } else {
            $defaultValues = [
                'nodelete'=>0,
            ];
            if ($id != null){
                // pokud je edit
                if ($id != 'new'){
                    $conditions = ['id'=>$id];
                    $data = $this->Users->find()
                    ->select()
                    ->where($conditions)
                    ->first()
                ;
                // pokud je nova polozka
                } else {
                   
                    $data = $this->vI->emptyEntity('Users'); 
                    
                }
                $data = $this->vI->convertLoadData($data,$defaultValues);        
                $data->password2 = $data->password;
                $data->password_tmp = $data->password;
                //pr($data);die();
                $validations = $this->vI->getValidations('Users');
            } else {

            }

            $results = [
                'result'=>true,
                'validations'=>(isset($validations)?$validations:null),
                'data'=>(isset($data)?$data:''),
                'select_list'=>$this->vI->SelectList($select_list),
            ];  
            //pr($results);die();  
            $this->setJsonResponse($results);
        }
        $this->clearCache('users_data');
    }

}
