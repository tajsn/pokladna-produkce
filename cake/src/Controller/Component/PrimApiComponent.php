<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Exception\Exception;
use Cake\ORM\TableRegistry;

class PrimApiComponent extends Component {
    private $debug = false;
    private $showErrors = false;
    public $error = null;
   // private $controller = null;
    private $url_domain = 'http://api.prim-hodinky.cz';
    private $url_actions = [
        'search_product_card'=>'/rqs/getCards.php',   //Vyhledání ID skladové karty podle názvu produktu?
        'product_prices'=>'/rqs/getStockPrice.php',   //Vyhledani cen a ks produktu/ů podle jeho ID a státu pro konkrétní měnu
        'search_client'=>'/rqs/checkClient.php',     //Vyhledani klienta podle ICO nebo Emailu
        'get_client'=>'/rqs/getClient.php',          //Ziskani clienta podle ID
        'get_product_card_ean'=>'/rqs/getCardIdsByEan.php',          //Ziskani karty produktu pomoci EAN
        'insert_order_b2c'=>'/rqs/insertOrderB2C.php',          //Insert Order B2C
        'insert_order_b2b'=>'/rqs/insertOrderB2B.php',          //Insert Order B2B
        'insert_address_b2b'=>'/rqs/insertAddress.php'          //Insert Address B2B
    ]; 
    
                    
    public function initialize(array $config) {
        //$this->controller = $this->_registry->getController();
    }
    
    private function send_post($url_action, $data){
        if(!isset($this->url_actions[$url_action])){
            throw new \Exception('Nenalezena url adresa pro akci '.$url_action );
        }
        $start = microtime(true);
        $postdata = http_build_query($data);
        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded'. "\r\n" . sprintf('Content-Length: %d', strlen($postdata)),
                'content' => $postdata
            )
        );
        $context  = stream_context_create($opts);

        $result = file_get_contents( $this->url_domain . $this->url_actions[$url_action], false, $context);
        $ret_data = false;
        if($result){
            $result = json_decode($result, true);
          
            if($result['result'] == true){
                $ret_data = $result['data'];
            }else{
                throw new \Exception($result['message'], $result['code']);
            }
        }
        $this->run_time = round((microtime(true) - $start), 2);
        
        if($this->debug){
             echo 'Run time: ' . $this->run_time.'s <br/>';
        }
        return $ret_data;
    }

    /**
     *  VYHLEDÁNÍ SKLADOVÉ KARTY (NAŠEPTÁVAČ)
        http://c3.localhost/prim/api/search-card/123
        vstupní parametry
            search - textový řetězec pro vyhledávání, zatím není omezený minimální počet znaků, ale asi bude vhodné to omezit, aby to nevracelo příliš mnoho záznamů

        výstupní parametry
            card_id - ID skladové karty (použije se pro načítání stavu a ceny)
            card_number - číslo skladové karty
            card_name - název skladové karty (kód produktu)
            stock - stav skladem

        chybové kódy
            404 - nebyly nalezeny žádné záznamy
            509 - prázdný vyhledávací řetězec
     */
    public function searchCard( $search = ''){
        try{
            $data = $this->send_post('search_product_card', ['search'=>$search]);
        }catch(\Exception $e){
            $this->error = $e->getMessage();
            if($this->showErrors){
                pr($e->getMessage());
            }
            return false;
        }
        return $data;
    }
    
    
    /**
     * NAČTENÍ STAVU SKLADU A CENY
     * 
        http://c3.localhost/prim/api/get-product-prices/220601,229548,229516/hu
        vstupní parametry
            card_ids - pole ID skladových karet
            lang - jazyková zkratka pro určení měny ceny (hodnoty: cz,sk,pl,hu)
            client_id - ID zákazníka (ID adresáře), nepovinná položka, slouží pro načítání ceny pro B2B zákazníka po přihlášení, na základě ID se podle typu ceny pošle zpět maloobchodní nebo velkoobchodní cena pro daného zákazníka (poznámka: PŘEJMENOVÁNO z user_id, aby se používal jednotný parametr jako u adresáře client_id)
            dealer_id - ID dealera, je-li zadáno, budou ve výstupu také velkoobchodní ceny, např. 208 nebo 002

        výstupní parametry
            card_id - ID skladové karty (použije se pro načítání stavu a ceny)
            card_number - číslo skladové karty
            card_name - název skladové karty (kód produktu)
            ean - čárový kód
            stock - stav skladem
            retail_price_vat - maloobchodní cena s dph 
            retail_price - maloobchodní cena bez dph
            retail_price_vat_sk
            retail_price_sk
            retail_price_vat_pl
            retail_price_pl
            retail_price_vat_hu
            retail_price_hu
            vat - sazba DPH
            lang - jazyková zkratka pro určení měny ceny pro ověření
            price_pc1 - velkoobchodní cena 1 
            price_pc1_vat - velkoobchodní cena 1 s dph
            price_pc1_sk
            price_pc1_vat_sk
            price_pc1_pl
            price_pc1_vat_pl
            price_pc1_hu
            price_pc1_vat_hu
            price_pc4 - velkoobchodní cena 4
            price_pc4_vat - velkoobchodní cena 4 s dph
            price_pc4_sk
            price_pc4_vat_sk
            price_pc4_pl
            price_pc4_vat_pl
            price_pc4_hu
            price_pc4_vat_hu
            stockall - stav součtový sklad, stav zboží mimo hlavní sklad, např. u dealerů - prosím zobrazovat pouze v B2B systému pro dealery, ve stávajícím systému to bylo vedle stavu hlavního sladu v závorce
            sortiment - označení sortimentu do, kterého patří daná skladová karta, podle tohoto se bude nastavovat zboží jednotlivým dealerům, aby každý viděl jen svůj definovaný sortiment.
            client_id - ID clienta pro kontrolu, pokud bylo na vstupu nastaveno a daný klient je nalezen v adresáři
            client_price - typ ceny klienta pro kontrolu, pokud bylo na vstupu zadáno client_id a je daný klient nalezen v adresáři
     
        chybové kódy
            404 - nebyly nalezeny žádné záznamy
            510 - nebylo zadáno žádné ID karty
            511 - nebyl zadán jazykový kód
            512 - zadán nesprávný jazykový kód
    */
    public function getProductPrices($ids = array(), $lang = 'cz', $b2b_id = null, $dealer_id = null){
        try{
            $params = [
                'card_ids' => $ids, 
                'lang' => ($lang ? $lang : 'all')
            ];
            if($b2b_id){
                $params['client_id'] = $b2b_id;
            }
            if($dealer_id){
                $params['dealer_id'] = $dealer_id;
            }
            $data = $this->send_post('product_prices', $params);
            
            //Transform pricec if client has allowed PC4 prices - velkoobchodni
            if($b2b_id){
                if($data){
                    foreach($data as &$item){
                        if($item['client_price'] == 'V'){
                            $item['retail_price'] = $item['price_pc4'];
                            $item['retail_price_vat'] = $item['price_pc4_vat'];
                        } 
                    }
                }
            }
        }catch(\Exception $e){
            $this->error = $e->getMessage();
            if($this->showErrors){
                pr($e->getMessage());
            }
            return false;
        }
        return $data;
    }
    
    /**
        OVĚŘENÍ EXISTENCE ZÁKAZNÍKA V ADRESÁŘI
            - pro firmu podle IČ, fyzické osoby podle e-mailu
            http://c3.localhost/prim/api/search-client/1/13448323
            http://c3.localhost/prim/api/search-client/2/zessick@seznam.cz
                - pro firmu podle IČ, fyzické osoby podle e-mailu
        
        - UPŘESNĚNÍ: vyhledává pouze ve fakturačních adresách

        vstupní parametry
            company_id - textový řetězec IČ
            client_email - textový řetězec e-mail
            client_type - hodnoty 1 - firma (vyhledávání podle IČ), 2 - fyzická osoba (vyhledávání podle e-mailu)
            country - zkratka státu (hodnoty: CZ, SK, PL, HU)

        výstupní parametry
            client_id - ID zákazníka (ID záznamu v adresáři, použije se pro výpis informací z adresáře a pro spárování s adresářem skladového systému, nejspíš také pro import objednávek)
            client_name - název firmy nebo jméno a příjmení fyzické osoby
            company_id - IČ
            street - ulice
            town -  město
            zip - PSČ
            email - jeden nebo více e-mailů (textpový řetězec)
            phone - jedno nebo více tel. čísel (textový řetězec)
            mobile - jedno  nebo více čísel (textový řetězec)
            price_type - M/V (M - maloobchodní cena - retail_price, V - velkoobchodní cena - price_pc4)
     
        chybové kódy
            404 - nebyly nalezeny žádné záznamy
            513 - prázdný vyhledávací řetězec (nezadano ani IČ ani e-mail)
            514 - nezadán typ zákazníka
            515 - nesprávný typ zákazníka
            516 - fyzickou osobu lze hledat jen podle e-mailu
            517 - může být zadán pouze jeden parametr vyhledávání (IČ nebo e-mail)
            518 - nebyl zadán kód státu
    */
    public function searchClient($type = 1, $search = '', $lang = 'cz'){
         try{
            $searchParams = [
                'client_type' => intval($type),
                'country'=>$lang
            ];
            if($type == 1){
                $searchParams['company_id'] = $search;
            }else if($type == 2){
                $searchParams['client_email'] = $search;
            }else{
                throw new \Exception('nepovolený typ klienta');
            }
            $data = $this->send_post('search_client', $searchParams);
        }catch(\Exception $e){
            $this->error = $e->getMessage();
            if($this->showErrors){
                pr($e->getMessage());
            }
            return false;
        }
        return $data;
    }
    /**
     NAČTENÍ INFORMACÍ Z ADRESÁŘE PODLE ID
        http://c3.localhost/prim/api/get-client/19981   
     
    vstupní parametry
        client_id - ID záznamu v adresáři (zadává se jedno ID, nikoliv pole, předpokládám, že se bude používat pro detailní výpis údajů z napojeného záznamu konkrétního zákazníka na kartě zákazníka v e-shopu nebo když dealer vybere zákazníka do objednávky.
    
     výstupní parametry
        client_id - ID zákazníka (ID záznamu v adresáři, použije se pro výpis informací z adresáře a pro spárování s adresářem skladového systému, nejspíš také pro import objednávek)
        client_name - jméno a příjmení fyzické osoby nebo jméno a příjmení zástupce firmy
        company_name - název firmy
            rozdělil jsem název firmy a jméno do dvou parametrů, tak jak máme v adresáři, domníval jsem se, že to půjde sloučit, ale byl by to problém při zpracování objednávek a nových adres, původně jsem posílal pouze parametr client_name s názvem firmy, to je přesunuto do company_name
        company_id - IČ
        street - ulice
        town -  město
        zip - PSČ
        email - jeden nebo více e-mailů (textový řetězec)
        phone - jedno nebo více tel. čísel (textový řetězec)
        mobile - jedno  nebo více čísel (textový řetězec)
        company_vat_id - DIČ
        company_vat_id2 - IČ DPH (používá se pro Slovensko)
        country_code - zkratka státu
        ma_different - korespondenční adresa se liší od fakturační, nabývá hodnot A nebo N
        ma_company_name - korespondenční adresa název firmy                        
        ma_client_name - korespondenční adresa jméno zástupce firmy
        ma_street - korespondenční adresa ulice
        ma_town - korespondenční adresa město
        ma_zip - korespondenční adresa PSČ
        bank - jméno banky
        account - číslo účtu
        iban - iban účtu
        price_type - M/V (M - maloobchodní cena - retail_price, V - velkoobchodní cena - price_pc4)
        bonus - částka pro výpočet bonusu - pro kreditní systém, upřesní IVO
        bonus_percentage - procento slevy pro bonus - pro kreditní systém, upřesní IVO
        children - pole koncových adres

    Funkce je momentálně nastavena tak, že vyhledává a vrací pouze záznamy, které jsou označeny jako fakturační adresa, která nemá nadřazený záznam. Výstupní parametry doplněny o pole záznamů (koncových adres, které jsou na fakturační adresu navázané). Struktura pole je stejná jako struktura hlavního záznamu. Bude potřeba aby si zákazník mohl vybrat, že chce zboží poslat na vybranou pobočku (koncovou adresu) nebo aby v mohl dealer vybrat pobočku, kam se objednávka odešle.

    chybové kódy
        404 - nebyly nalezeny žádné záznamy
        519 - nebylo zadáno ID zákazníka
     */
    public function getClient($client_id ){
         try{
            $data = $this->send_post('get_client', [
                'client_id'=> $client_id
            ]);
        }catch(\Exception $e){
            $this->error = $e->getMessage();
            if($this->showErrors){
                pr($e->getMessage());
            }
            return false;
        }
        return $data;
    }
    
    /**
        NAČTENÍ ID SKLADOVÉ KARTY PODLE EANU - nová funkce
            http://c3.localhost/prim/api/get-product-card-by-ean/8591212816722 

        vstupní parametry
            eans - pole čárových kódů

        výstupní parametry
            card_id - ID skladové karty (použije se pro načítání stavu a ceny)
            card_number - číslo skladové karty
            card_name - název skladové karty (kód produktu)
            ean - čárový kód

        chybové kódy
            404 - nebyly nalezeny žádné záznamy
            508 - nebyl zadán žádný čárový kód
    */ 
    public function getProductCardByEan($eans = []){
        try{ 
            $data = $this->send_post('get_product_card_ean', [
                'eans'=> $eans
            ]);
        }catch(\Exception $e){
            $this->error = $e->getMessage();
            if($this->showErrors){
                pr($e->getMessage());
            }
            return false;
        }
        return $data;
    }
    
    /**
        vstupní parametry
           data objednavky 

        výstupní parametry
            id_order_eshop - ID objednávky na e-shopu
            id_order_maxim - ID, které přiřadí datový můstek a pod, kterým bude zavedena rezervace ve skladovém systému

        chybové kódy
        510 - nezadány žádné vstupní parametry
        520 - ID objednávky nebylo zadáno nebo je 0
        521 - Zadán neplatný status objednávky (Nová, Oprava, Storno)
        522 - Povinný parametr e-mail není zadán
        523 - Nelze zpracovat nulovou objednávku. Celková cena chybí nebo je 0
        524 - Objednávka neobsahuje žádné produkty
        525 - Není zadán způsob dopravy
        526 - Není zadán způsob platby
        527 - Zadán neplatný status platby (Zaplaceno, Neplaceno)
        528 - Objednávka označená jako zaplaceno neobsahuje údaje EET
        529 - Povinný parametr IČ není zadán (jen B2B)
    */
    public function insertOrder($orderItem){
        try{ 
           
            $data = $this->transformOrder($orderItem);
          // pr($data);
            $urlName = $orderItem->shop_client->is_approved == 1 && $orderItem->shop_client->shop_client_type_id == 2 ? 'insert_order_b2b' : 'insert_order_b2c';
            //TODO switch url by b2b or b2c
            //insert_order_b2c
            if(($result = $this->send_post($urlName ,  $data ))){
                if(isset($result['order_ids']['id_order_maxim']) && !$orderItem->stock_order_id){
                    $orderItem->stock_order_id = $result['order_ids']['id_order_maxim'];
                    $ShopOrders = TableRegistry::get('ShopOrders');
                    $ShopOrders->save($orderItem, ['associtation'=>false]);
                }
                $res = true;
            }else{
                pr($this->error);
                $res = false;
            }
        }catch(\Exception $e){
            $this->error = $e->getMessage();
            if($this->showErrors){
                pr($e->getMessage());
            }
            $res = false;
        }
        return $res;
    }
    
    /**
     * Pomocna funkce ktera prevadi DB objekt ShopOrder na pole vhodne pro odeslani na API mustek
     * @param type $orderItem
     * 
                [id_order_eshop]  - id objednávky na e-shopu (integer)
                [order_status] - určení objednávky pro typ zpracování - hodnoty: Nová/Oprava/Storno
                [dealer] - trojciferné číslo dealera, bude vyplněno pokud objednávku odesílá přímo dealer ze svého rozhraní

                [price_type] - předávat nastavení TYP CENY - M/V, aby bylo jasné, která cena se posílá v položce unit_price u jednotlivých položek objednávky, povinná položka
                [payment_status] - stav platby, pokud proběhne platba přes GoPay bude objednávka označena jako “Zaplaceno” a vyplní se pole EET (viz níže)  hodnoty: Zaplaceno/Neplaceno
                [total_price]  - celková cena objednávky včetně dopravy bez DPH - POZOR ZMĚNA!,  formát s desetinnou tečkou, 2 desetinná místa

                [total_price_vat]  - celková cena objednávky včetně dopravy s DPH,  formát s desetinnou tečkou, 2 desetinná místa
                [currency] - měna hodnoty: CZK/PLN/HUF/EUR
                [note] - poznámka
                [date_time] - čas objednávky ve formátu UNIX timestamp
                [delivery_title] - popis dopravy, např. Česká pošta, DPD, Osobní odběr
                [delivery_price] - cena dopravy bez DPH, POZOR ZMĚNA!

                [delivery_price_vat] - cena dopravy s DPH
                [payment_title] - popis platby, např. Dobírka, GoPay, Hotově
                [discount] - uplatněn slevový kupón hodnoty A/

                [discount_price] - hodnota uplatněného slevového kupónu, např. -1000.00, jen dárkové poukazy v měně, nezapočítávat procentní slevy, ty se uplatní u jednotlivých produktů, kromě VIP hodinek
                [discount_code] - název slevového poukazu nebo akce, slevový kód, číslo slevového kupónu nebo uplatnění slevový kód, např BLACK FRIDAY 
                [email] - e-mail (povinné pole pro všechny objednávky)
                [phone] - telefon 
                [mobile] - mobil
                [invoice_address] => Array   - pole fakturační adresa
                (
                    [id_add_eshop] - ID adresy u vás na e-shopu (pro kontrolu) - JEN B2B
                    [id_add_maxim] - ID adresy z Maximu (z můstku) - JEN B2B
                    [company] - název firmy - JEN B2B
                    [name] - jméno a příjmení
                    [street] - ulice a č. p.
                    [city] - město
                    [zip] - PSČ
                    [country_code] - kód státu, hodnoty: CZ/SK/PL/HU/EN
                    [company_id] - IČ
                    [company_vat_id] - DIČ
                    [company_vat_id2] - DIČ DPH - jen Slovensko
                )

            [delivery_address] => Array   - pole doručovací adresy
                (
                    [dl_id_add_eshop] - ID adresy u vás na e-shopu (pro kontrolu) - JEN B2B
                    [dl_id_add_maxim] - ID adresy z Maximu (z můstku) - JEN B2B
                    [dl_id_add_maxim_parent] => - ID fakturační adresy z Maximu (z můstku) - JEN B2B
                    [dl_company] - firma, POZOR zde se bude používat i u B2C, pokud bude někdo chtít doručení na firmu
                    [dl_name] - jméno a příjmení
                    [dl_street] - ulice a č. p. 
                    [dl_city] - město
                    [dl_zip]  - PSČ
                    [dl_country_code] - kód státu, hodnoty: CZ/SK/PL/HU/EN
                )

            [items] => Array  - pole položek objednávky
                (
                    [0] => Array
                        (
                            [card_id_maxim] - id skladové karty (id podle kterého se načítají cena a stav)
                            [card_number_maxim] - číslo skladové karty (např.10120-12345)
                            [name] => název a kód produktu, pokud má produkt pouze kód, tak neduplikovat, např. PRIM Skeleton A - W01P.10151.A
                            [quantity] - počet kusů, celé číslo
                            [unit_price] - jednotková cena za kus s DPH, formát s desetinnou tečkou, 2 desetinná místa, cena bude s nebo bez DPH podle toho, za jaké ceny daný zákazník objednává - podle nastavení TYP CENY -  (maloobchodní s DPH (retail_price) pro koncové zákazníky (jednotlivce i firmy) a velkoobchodní bez DPH (pc4_price) - firemní B2B zákazníci.
                            [discount_per]  sleva, číselné vyjádření v procentech, např. 10.0

                            [discount_price] - částka odečtená jako sleva, formát s desetinnou tečkou, 2 desetinná místa
                            [total_price] - celková cena za položku bez DPH - POZOR ZMĚNA!, včetně započítané procentní slevy, pokud je poskytnuta, formát s desetinnou tečkou, 2 desetinná místa

                            [total_price_vat] - celková cena za položku s DPH včetně započítané procentní slevy, pokud je poskytnuta, formát s desetinnou tečkou, 2 desetinná místa
                            [vat] - DPH v procentech, celé číslo
                            [vat_price] - celková částka DPH za položku, formát s desetinnou tečkou, 2 desetinná místa

                            [left_sample] - zanecháno ze vzorků -  A/N (pokud dealer nechá na obchodě ze svých vzorků)
                            [left_quamtity] - počet zanechaných kusů, vyplní se pokud je položka left_sample označena jako A (ano)
                        )

                )

            [eet] => Array   - pole položek parametrů přebíraných z EET
                (
                    [pkp]
                    [fik]
                    [receipt_number]
                    [date]
                    [id_store]
                    [device]
                    [mode]
                )
    výstupní parametry
        id_order_eshop - ID objednávky na e-shopu
        id_order_maxim - ID, které přiřadí datový můstek a pod, kterým bude zavedena rezervace ve skladovém systému

    chybové kódy
            510 - nezadány žádné vstupní parametry
            520 - ID objednávky nebylo zadáno nebo je 0
            521 - Zadán neplatný status objednávky (Nová, Oprava, Storno - bude se realizovat později, zatím lze používat pouze status Nová)
            522 - Povinný parametr e-mail není zadán
            523 - Nelze zpracovat nulovou objednávku. Celková cena chybí nebo je 0
            524 - Objednávka neobsahuje žádné produkty
            525 - Není zadán způsob dopravy
            526 - Není zadán způsob platby
            527 - Zadán neplatný status platby (Zaplaceno, Neplaceno)
            528 - Objednávka označená jako zaplaceno neobsahuje údaje EET
            529 - Povinný parametr IČ není zadán (jen B2B)
            530 - Povinný parametr měna není zadán
            531 - Objednávka se zadaným ID již byla zpracována
            532 - Povinný parametr price_type není zadán nebo obsahuje nepovolenou hodnotu.
     */
    private function transformOrder($orderItem){
            $data = [];
            $isGopay = isset($orderItem->gopay_id) && $orderItem->gopay_id ? true : false;
            function currencyShortcut($id){
                $currency = 'CZK';
                switch($id){
                    case 1:
                        $currency = 'CZK';
                        break;
                    case 2:
                        $currency = 'EUR';
                        break;
                    case 3:
                        $currency = 'PLN';
                        break;
                    case 4:
                        $currency = 'HUF';
                        break;
                     case 5:
                        $currency = 'EUR';
                        break;
                }
                return $currency;
            }
            function stateShortcut($id){
                $state = 'CZ';
                switch($id){
                    case 1:
                        $state = 'CZ';
                        break;
                    case 2:
                        $state = 'SK';
                        break;
                    case 3:
                        $state = 'PL';
                        break;
                    case 4:
                        $state = 'HU';
                        break;
                     case 5:
                        $state = 'EN';
                        break;
                }
                return $state;
            }
            function orderStatus($id){
                $state = 'CZ';
                switch($id){
                    case 1:
                        $state = 'Nová';
                        break;
                    case 2:
                        $state = 'Oprava';
                        break;
                    case 3:
                        $state = 'Storno';
                        break;
                }
                return $state;
            }
            $data['id_order_eshop'] = $orderItem->id;
            $data['order_status'] = 'Nová';//orderStatus(1 /*isset($orderItem->stock_order_id) && $orderItem->stock_order_id ? 2 : 1*/); // Nová/Oprava/Storno
        
            if($orderItem->dealer_id){
                //TODO get code of dealer
                $data['dealer'] = $orderItem->dealer_id; // trojciferné číslo dealera, bude vyplněno pokud objednávku odesílá přímo dealer ze svého rozhraní
            }
            
            $data['price_type'] = (isset($orderItem->shop_client->show_price_pc4) ? ($orderItem->shop_client->show_price_pc4 == 1 ? 'V': 'M') : 'M'); // stav platby, pokud proběhne platba přes GoPay bude objednávka označena jako “Zaplaceno” a vyplní se pole EET (viz níže)  hodnoty: Zaplaceno/Neplaceno
            $data['payment_status'] = $isGopay ? 'Zaplaceno' : 'Neplaceno'; // stav platby, pokud proběhne platba přes GoPay bude objednávka označena jako “Zaplaceno” a vyplní se pole EET (viz níže)  hodnoty: Zaplaceno/Neplaceno
            $data['total_price'] = number_format($orderItem->price, 2, ',',''); // celková cena objednávky včetně dopravy s DPH,  formát s desetinnou tečkou, 2 desetinná místa
            $data['total_price_vat'] = number_format($orderItem->price_vat, 2, ',',''); // celková cena objednávky včetně dopravy s DPH,  formát s desetinnou tečkou, 2 desetinná místa
            $data['currency'] = isset($orderItem->currency_id) ? currencyShortcut($orderItem->currency_id) : 'CZK'; // měna hodnoty: CZK/PLN/HUF/EUR
            $data['note'] = $orderItem->note; // poznámka
            $data['date_time'] = $orderItem->created ? $orderItem->created->toUnixString() : 0; // čas objednávky ve formátu UNIX timestamp
    
            if(!isset($orderItem->shop_transport)){
                throw new \Exception('Není zvolen typ dopravy');
            }
            $data['delivery_title'] = $orderItem->shop_transport->name; // popis dopravy, např. Česká pošta, DPD, Osobní odběr
            $data['delivery_price'] = number_format($orderItem->shop_transport->price, 2, ',',''); // cena dopravy s DPH
            $data['delivery_price_vat'] = number_format($orderItem->shop_transport->price_vat, 2, ',',''); // cena dopravy s DPH
          
            if(!isset($orderItem->shop_payment)){
                throw new \Exception('Není zvolen typ platby');
            }
            $data['payment_title'] = $orderItem->shop_payment->name; // popis platby, např. Dobírka, GoPay, Hotově
            $data['discount'] = null; // uplatněn slevový kupón hodnoty A/N
            $data['discount_price'] = null; // uplatněn slevový kupón absolutni hodnoty - ne procentualni slevy
            $data['discount_code'] = null; // - slevový kód, číslo slevového kupónu nebo uplatnění slevový kód, např BLACK FRIDAY 
            
            if(!isset($orderItem->shop_client) || !$orderItem->shop_client->id){
                throw new \Exception('K objednavce neni prirazen klient');
            }
            $data['email'] = $orderItem->shop_client->email; // - e-mail (povinné pole pro všechny objednávky)
            $data['phone'] = $orderItem->shop_client->phone; // - telefon 
            $data['mobile'] = $orderItem->shop_client->mobile; // - mobil
           
            if(!isset($orderItem->invoice_addres) || !$orderItem->invoice_addres->id){
                throw new \Exception('K objednavce neni nactena fakturacni adresa');
            }
            //Pokud adresa neni odeslana na do maximu tak musime poslat nejprve na mustek...
            if(!isset($orderItem->invoice_addres->stock_address_id) || !$orderItem->invoice_addres->stock_address_id){
                
            }
            $data['invoice_address'] = [
                    'id_add_eshop' => $orderItem->invoice_addres->id, // - ID adresy u vás na e-shopu (pro kontrolu) - JEN B2B
                    'id_add_maxim' => null, //$orderItem->invoice_addres->stock_address_id, // - ID adresy z Maximu (z můstku) - JEN B2B
                    'company' => $orderItem->shop_client->company_name, // - název firmy - JEN B2B
                    'name' => $orderItem->shop_client->first_name .' '. $orderItem->shop_client->last_name, // - jméno a příjmení
                    'street' => $orderItem->invoice_addres->street . ' ' . $orderItem->invoice_addres->cp, // - ulice a č. p.
                    'city' => $orderItem->invoice_addres->city, // - město
                    'zip' => $orderItem->invoice_addres->zip, // - PSČ
                    'country_code' => isset($orderItem->invoice_addres->state_id) ? stateShortcut($orderItem->invoice_addres->state_id) : 'CZ', // - kód státu, hodnoty: CZ/SK/PL/HU/EN
                    'company_id' => $orderItem->shop_client->ico, // - IČ
                    'company_vat_id' => $orderItem->shop_client->dic, // DIČ
                    'company_vat_id2' => $orderItem->shop_client->dic2 // - DIČ DPH - jen Slovensko
           ];
            
           if(!isset($orderItem->delivery_addres) || !$orderItem->delivery_addres->id){
                throw new \Exception('K objednavce neni nactena dorucovaci adresa');
           }
           //Pokud adresa neni odeslana na do maximu tak musime poslat nejprve na mustek...
           if(!isset($orderItem->delivery_addres->stock_address_id) || !$orderItem->delivery_addres->stock_address_id){
                
           }
           $data['delivery_address'] = [
                    'dl_id_add_eshop' => $orderItem->delivery_addres->id, // ID adresy u vás na e-shopu (pro kontrolu) - JEN B2B
                    'dl_id_add_maxim' => null, //$orderItem->delivery_addres->stock_address_id, // - ID adresy z Maximu (z můstku) - JEN B2B
                    'dl_id_add_maxim_parent' => null, // - ID fakturační adresy z Maximu (z můstku) - JEN B2B
                    'dl_company' => $orderItem->delivery_addres->company_name, // firma, POZOR zde se bude používat i u B2C, pokud bude někdo chtít doručení na firmu
                    'dl_name' => $orderItem->delivery_addres->first_name . ' ' . $orderItem->delivery_addres->last_name, // jméno a příjmení
                    'dl_street' => $orderItem->delivery_addres->street .' ' . $orderItem->delivery_addres->cp, // ulice a č. p. 
                    'dl_city' => $orderItem->delivery_addres->city, // město
                    'dl_zip' => $orderItem->delivery_addres->zip, // PSČ
                    'dl_country_code' => isset($orderItem->delivery_addres->state_id) ? stateShortcut($orderItem->delivery_addres->state_id) : 'CZ', // kód státu, hodnoty: CZ/SK/PL/HU/EN
            ];
           
            $data['items'] = [];
            $discount = $discount_price = $discount_price_vat = null;
            foreach($orderItem->shop_order_items as $oItem){ 
                if($oItem->shop_discount_voucher_id && !$oItem->shop_product_id){
                     $ShopDiscountVouchers = TableRegistry::get('ShopDiscountVouchers');
                     $discountVoucher = $ShopDiscountVouchers->get($oItem->shop_discount_voucher_id);
                     if($discountVoucher){ 
                        $discount = $discountVoucher->value_percent ? $discountVoucher->value_percent : null;
                        $data['discount'] = 'A'; //$oItem->discount_percent ? $oItem->discount_percent.'%' : $oItem->discount_value;
                        $data['discount_code'] = $discountVoucher->name . ' (ID:'. $discountVoucher->id.')' . ($discountVoucher->pernament_code ? ',KOD:'. $discountVoucher->pernament_code : '');
                     }
                }
            }
           
            foreach($orderItem->shop_order_items as $oItem){ 
                if($oItem->shop_discount_voucher_id && !$oItem->shop_product_id){
                    continue;
                }
                if($discount){
                    $discount_price = round($oItem->price * ($discount / 100)); 
                    $discount_price_vat = round($oItem->price_vat * ($discount / 100)); 
                    $oItem->price -= $discount_price;
                    $oItem->price_vat -= $discount_price_vat;
                }
                $data['items'][] = [
                    'card_id_maxim' => $oItem->shop_product->stock_card_id, // - id skladové karty (id podle kterého se načítají cena a stav)
                    'card_number_maxim' => $oItem->shop_product->stock_card_number, // číslo skladové karty (např.10120-12345)
                    'name' => $oItem->shop_product->name_internal != $oItem->shop_product->name ? $oItem->shop_product->name .' - '. $oItem->shop_product->name_internal : $oItem->shop_product->name , // název a kód produktu, pokud má produkt pouze kód, tak neduplikovat, např. PRIM Skeleton A - W01P.10151.A
                    'quantity' => $oItem->count, // počet kusů, celé číslo 
                    'unit_price' =>  number_format($oItem->price_vat_per_item ? $oItem->price_vat_per_item : 0 , 2, ',','') , // jednotková cena za kus s DPH, formát s desetinnou tečkou, 2 desetinná místa
                    'discount_per' => $discount, //sleva, číselné vyjádření v procentech, např. 10.0     
                    'discount_price' => $discount_price_vat ? number_format($discount_price_vat, 2, ',','') : null, // částka odečtená jako sleva, formát s desetinnou tečkou, 2 desetinná místa
                    'total_price' =>   number_format( $oItem->price_vat ? $oItem->price_vat : 0 , 2, ',',''), //celková cena za položku s DPH včetně započítané slevy, pokud je poskytnuta, formát s desetinnou tečkou, 2 desetinná místa
                    'vat' => (!isset($oItem->tax->id) ? 21 : ($oItem->tax->value * 100)), //DPH v procentech, celé číslo
                    'vat_price' => number_format(($vatPrice = $oItem->price_vat - $oItem->price) ? ($vatPrice) : 0, 2, ',','') , //celková částka DPH za položku, formát s desetinnou tečkou, 2 desetinná místa
                ];
            }
           
            if($isGopay && isset($orderItem->eet_data)){
                $eet = json_decode($orderItem->eet_data, true);
                if(isset($eet['fik'])){
                    $data['eet'] = [
                        'pkp'=> $eet['pkp'],
                        'fik'=> $eet['fik'],
                        'receipt_number'=> $eet['bkp'], 
                        'date'=>  $orderItem->created ? $orderItem->created->toUnixString() : 0,
                        'id_store'=>null,
                        'device'=>null,
                        'mode'=>null,
                    ];
                }
            }
            return $data;
    }
    /*
    IMPORT ADRES B2B zákazníků
            - povinné parametry:  e-mail, IČ, kód státu a id adresy z e-shopu
            Funkce pro import nových zákazníků (adres) do IS MAxim. Posílají se data v obdobné struktuře jako z můstku na e-shop posílá funkce getClient pro načítání informací z adresáře o jednotlivých zákaznících. Nová adresa vznikne buď registrací zadáním dealera přes B2B modul nebo přes registrační formulář a buď se posílá hned přímo na můstek (koncový zákazník s IČ, který nezaškrtne při registraci, že má zájem stát se prodejcem) nebo po kontrole odpovědným pracovníkem přes administraci e-shopu, kde má být tlačítko pro odeslání na můstek (B2B zákazník, který má zájem stát se prodejcem a nakupovat za velkoobchodní ceny). Podrobně popsáno v tabulce nedodělků u jednotlivých typů zákazníků. První zmíněný typ zákazníka pak může i bez registrace udělat objednávku, automaticky bude mít nastaveny maloobchodní ceny a pokud se adresa automaticky nespáruje přes IČ s existujícím zázname, pak se spolu s objednávkou musí poslat také informace o zákazníkovi přes tento import.

        url 
            http://api.prim-hodinky.cz/rqs/insertAddress.php

        vstupní parametry
                status - určení pro typ zpracování - hodnoty: Nová (Oprava/Storno - zatím neaktivní)
                id_add_eshop - ID zákazníka (fakturační adresy) na e-shopu 
                client_name - jméno a příjmení fyzické osoby nebo jméno a příjmení zástupce firmy
                company_name - název firmy
                company_id - IČ
                street - ulice
                city -  město
                zip - PSČ
                email - jeden nebo více e-mailů (textový řetězec)
                phone - jedno nebo více tel. čísel (textový řetězec)
                mobile - jedno  nebo více čísel (textový řetězec)
                company_vat_id - DIČ
                company_vat_id2 - IČ DPH (používá se pro Slovensko)
                country_code - zkratka státu
                bank - jméno banky
                account - číslo účtu
                iban - iban účtu
                ma_status - status korespondenční adresy určení pro typ zpracování - hodnoty: Nová (Oprava/Storno - zatím neaktivní), pokud není korespondenční adresa zadána, tak se parametry začínající na ma_ nevyplňují
                ma_company_name - korespondenční adresa název firmy                        
                ma_client_name - korespondenční adresa jméno zástupce firmy
                ma_street - korespondenční adresa ulice
                ma_city - korespondenční adresa město
                ma_zip - korespondenční adresa PSČ
                dealer - ID dealere, pokud adresu zadává dealer přes B2B modul
                date_time - čas objednávky ve formátu UNIX timestamp
                children - pole koncových adres (doručovací adresy, pobočky)
                (
                 [0] => Array
                    (
                        status - určení pro typ zpracování - hodnoty: Nová (Oprava/Storno - zatím neaktivní)
                        id_add_eshop - ID zákazníka (koncové adresy) na e-shopu
                        id_add_eshop_parent - ID zákazníka (fakturační adresy) na e-shopu pod kterou koncová adresa spadá
                        client_name - jméno a příjmení fyzické osoby nebo jméno a příjmení zástupce firmy
                        company_name - název firmy
                        street - ulice
                        city -  město
                        zip - PSČ
                        phone - jedno nebo více tel. čísel (textový řetězec)
                        mobile - jedno  nebo více čísel (textový řetězec)
                        country_code - zkratka státu
                    )        
                )

        výstupní parametry
            id_add_eshop - ID zákazníka, fakturační adresy na e-shopu
            id_add_bridge - ID, které přiřadí datový můstek

        chybové kódy
            521 - Zadán neplatný status objednávky - povolená hodnota Nová (Oprava, Storno - bude se realizovat později, zatím lze používat pouze status Nová)
            522 - Povinný parametr e-mail není zadán
            529 - Povinný parametr IČ není zadán
            540 - Povinný parametr ID zákazníka, fakturační adresy na e-shopu není zadán
            541 - Povinný parametr kód státu není zadán
            542 - Zákazník (fakturační adresa) se zadaným ID z e-shopu již byl zpracován
        */
    
     public function insertAddress($clientEntity){
        try{ 
           
            $data = $this->transformAddressData($clientEntity);
           
            //insert_address_b2b
            if(($result = $this->send_post('insert_address_b2b' ,  $data ))){
                
               /* $addrItem->stock_address_id = $result['id_add_eshop'];
                $addrItem->stock_address_id = $result['id_add_bridge'];
                
                $ShopAddress = TableRegistry::get('ShopClientAddress');
                $ShopAddress->save($clientEntity, ['associtation'=>false]);
                */
                $res = true;
            }else{
                pr($this->error);
                $res = false;
            }
        }catch(\Exception $e){
            $this->error = $e->getMessage();
            if($this->showErrors){
                pr($e->getMessage());
            }
            $res = false;
        }
        return $res;
    }
    
    private function transformAddressData($entity, $dealer_id = null, $order_created = null){
        $invoiceAddr = null;
        $deliveryAddr = null;
        $mailAddr = null;
        $addresses = [];
        if(isset($entity->shop_client_addresses)){
            $addresses = $entity->shop_client_addresses;
        }else if(isset($entity->user_addresses)){ 
            $addresses = $entity->user_addresses;
        }
        
        foreach($addresses as $item){
            if($item['shop_address_type_id'] == 2){
                $invoiceAddr = is_array($item) ? $item : $item->toArray();
            }else if($item['shop_address_type_id'] == 1){
                $deliveryAddr = is_array($item) ? $item : $item->toArray();
            }else if($item['shop_address_type_id'] == 3){
                $mailAddr =  is_array($item) ? $item : $item->toArray();
            }
        }
        if(!$invoiceAddr){
            throw new Exception('Nenalezena fakturacni adresa');
        }
        $data = [];
        $data['status'] = 'Nová'; // - určení pro typ zpracování - hodnoty: Nová (Oprava/Storno - zatím neaktivní)
        $data['id_add_eshop'] = null; // - ID zákazníka (fakturační adresy) na e-shopu 
        $data['client_name'] = $entity->first_name .' '. $entity->last_name; //  - jméno a příjmení fyzické osoby nebo jméno a příjmení zástupce firmy
        $data['company_name'] = $entity->company_name; //  - název firmy
        $data['company_id'] = $entity->ico; //  - IČ
        $data['street'] = $invoiceAddr['street'] . (isset($invoiceAddr['co']) && $invoiceAddr['cp'] ? ' '.$invoiceAddr['cp'] : '') . (isset($invoiceAddr['cp']) && $invoiceAddr['cp'] ? ' '.$invoiceAddr['cp'] : ''); //  - ulice
        $data['city'] = $invoiceAddr['city']; //  -  město
        $data['zip'] = $invoiceAddr['zip']; //  - PSČ
        $data['email'] = $entity->email; //  - jeden nebo více e-mailů (textový řetězec)
        $data['phone'] = $entity->phone; //  - jedno nebo více tel. čísel (textový řetězec)
        $data['mobile'] = $entity->mobile; //  - jedno  nebo více čísel (textový řetězec)
        $data['company_vat_id'] = $entity->dic; //  - DIČ
        $data['company_vat_id2'] = $entity->dic2; //  - IČ DPH (používá se pro Slovensko)
        $data['country_code'] = (isset($invoiceAddr['state_id']) && $invoiceAddr['state_id'] > 1 ?  $this->stateShortcut($invoiceAddr['state_id']) : 'CZ' ); //  - zkratka státu
        $data['bank'] = $entity->bank; //  - jméno banky
        $data['account'] = $entity->account; //  - číslo účtu
        $data['iban'] = null; //  - iban účtu
        if($mailAddr){
            $data['ma_status'] = 'Nová'; //  - status korespondenční adresy určení pro typ zpracování - hodnoty: Nová (Oprava/Storno - zatím neaktivní), pokud není korespondenční adresa zadána, tak se parametry začínající na ma_ nevyplňují
            $data['ma_company_name'] = $entity->company_name; //  - korespondenční adresa název firmy                        
            $data['ma_client_name'] = ($mailAddr['name'] != '' ? $mailAddr['name'] : $entity->first_name .' '. $entity->last_name); //  - korespondenční adresa jméno zástupce firmy
            $data['ma_street'] = $mailAddr['street'] . (isset($mailAddr['cp']) && $mailAddr['cp'] ? ' '.$mailAddr['cp'] : ''); //  - korespondenční adresa ulice
            $data['ma_city'] = $mailAddr['city']; //  - korespondenční adresa město
            $data['ma_zip'] = $mailAddr['zip']; //  - korespondenční adresa PSČ
        }else{
            $data['ma_status'] = null;
            $data['ma_company_name'] = null;   
            $data['ma_client_name'] = null; //  - korespondenční adresa jméno zástupce firmy
            $data['ma_street'] = null;
            $data['ma_city'] = null;
            $data['ma_zip'] = null;
        }
        $data['dealer'] = $dealer_id; //  - ID dealere, pokud adresu zadává dealer přes B2B modul
        $data['date_time'] = $order_created; //  - čas objednávky ve formátu UNIX timestamp
        $data['children'] = [];
        if($deliveryAddr){
            $data['children'][]  = [
                'status'=> 'Nová', // určení pro typ zpracování - hodnoty: Nová (Oprava/Storno - zatím neaktivní)
                'id_add_eshop'=> null, //- ID zákazníka (koncové adresy) na e-shopu
                'id_add_eshop_parent'=> null, //- ID zákazníka (fakturační adresy) na e-shopu pod kterou koncová adresa spadá
                'client_name'=> ($deliveryAddr['name'] != '' ? $deliveryAddr['name'] : $entity->first_name .' '. $entity->last_name), //- jméno a příjmení fyzické osoby nebo jméno a příjmení zástupce firmy
                'company_name'=> $entity->company_name, //- název firmy
                'street'=> $deliveryAddr['street'], //- ulice
                'city'=> $deliveryAddr['city'], //-  město
                'zip'=> $deliveryAddr['zip'], //- PSČ
                'phone'=> ($deliveryAddr['phone'] ? $deliveryAddr['phone'] : $invoiceAddr['phone']),// - jedno nebo více tel. čísel (textový řetězec)
                'mobile'=> ($deliveryAddr['email'] ? $deliveryAddr['email'] : $invoiceAddr['email']), //- jedno  nebo více čísel (textový řetězec)
                'country_code'=> (isset($deliveryAddr['state_id']) && $deliveryAddr['state_id'] > 1 ?  $this->stateShortcut($deliveryAddr['state_id']) : 'CZ' ) // - zkratka státu
            ]; 
        }
        return $data;
    }
}
