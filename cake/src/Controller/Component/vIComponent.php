<?php
namespace App\Controller\Component;
use Cake\Controller\Component;

use Cake\I18n\Time;
use Cake\I18n\Date;
use Cake\Utility\Inflector;

class vIComponent extends Component {

    public function initialize(array $config) 
    {
        $this->controler = $this->_registry->getController();
        //pr($this->controler);die('a');
    }

    /**
     * convert fields from controller to angular
     */
    public function fieldsConvert($fields_defined){
        
        if (isset($this->request->data['pagination'])){
            foreach($this->request->data['pagination'] AS $pag_key=>$pag_value){
                if (in_array($pag_key,['page','sort','direction'])){
                    $this->request->query[$pag_key] = $pag_value;
                }
                
            }
        }    

        if (!isset($this->request->query['sort']) || empty($this->request->query['sort'])){
            $this->request->query['sort'] = 'id';
            $this->request->query['direction'] = 'DESC';
        }

        $fields = [];
        foreach($fields_defined AS $f){
            $fields[] = $f['col'];
            
        }
        $fields[] = 'status';
        return $fields;
    }

    
    /**
     * prevod conditions z angular na PHP
     */
    public function convertConditions($conditions,$data,$filtration_defined){
        $this->filtration_defined = $filtration_defined;
        //pr($this->filtration_defined);
        //pr($data);die();
        $this->convert_conditions = [];
        foreach($data AS $con_key=>$con_value){
            //pr($con_key);
            //pr($con_value);
            if($con_value != '' && isset( $this->filtration_defined[$con_key])){
                // pokud je text
                if ($this->filtration_defined[$con_key]['type'] == 'text'){
                    $this->convert_conditions[$con_key] = $con_value;
                    $this->filtration_defined[$con_key]['value'] = $con_value;
                }
                // pokud je text LIKE
                if ($this->filtration_defined[$con_key]['type'] == 'text_like'){
                    $this->convert_conditions[$con_key.' LIKE'] = '%'.$con_value.'%';
                    $this->filtration_defined[$con_key]['value'] = $con_value;
                }
                
                // pokud je date_range
                if ($this->filtration_defined[$con_key]['type'] == 'date_range'){
                    $con_value_split = explode(' - ',$con_value);
                    $this->convert_conditions['date('.$con_key.') >='] = $con_value_split[0];
                    $this->convert_conditions['date('.$con_key.') <='] = $con_value_split[1];
                    $this->filtration_defined[$con_key]['value'] = $con_value;
                }
                
                // pokud je select
                if ($this->filtration_defined[$con_key]['type'] == 'select'){
                    // if (is_int($con_value)){
                        //pr(intval$con_value);
                    if ($con_value != ''){
                        $this->convert_conditions[$con_key] = $con_value;
                        $this->filtration_defined[$con_key]['value'] = $con_value;
                        //pr($this->convert_conditions);
                        
                    }
                }
                $this->request->query['page'] = 1;
            }
        }
        //pr($this->convert_conditions);die();
        return $this->convert_conditions;
    }

    /**
     * vytvoreni select listu pro filtraci
     */
    public function filtrSelectList($data){
        $list = [];
        if ($data)
        foreach($data AS $k=>$value){
            $list[] = [
                'id'=>$k,
                'value'=>$value,
            ];
        }
        return $list;
        //pr($list);die();
    }

    /**
     * ziskani dat validace pro formular
     */
    public function getValidations($model,$contain=null){
        $this->controller = $this->_registry->getController();
        
        $validation = $this->controller->$model->validationDefault(new \Cake\Validation\Validator());
        $result = $this->transformValidationRules( $validation->__debugInfo() );
        if ($contain){
            foreach($contain AS $ctype=>$c){
                if ($ctype == 'belongsTo'){
                    $validation = $this->controller->$c->validationDefault(new \Cake\Validation\Validator());
                    $result[Inflector::tableize($c)] = $this->transformValidationRules( $validation->__debugInfo() );
                    
                } else {
                    $validation = $this->controller->$c->validationDefault(new \Cake\Validation\Validator());
                    $result[Inflector::tableize($c)] = $this->transformValidationRules( $validation->__debugInfo() );
                    
                }
                //pr($result2);die();
            }
        }
        //pr($result);die();
        return $result;
        
    }
    
    private function transformValidationRules($validation){
        
        $result = [];
        if($validation){
            foreach($validation['_fields'] as $key => $field){
                if($field['isPresenceRequired'] === true){
                    $result[$key] = [
                        'required'=> (isset($validation['_presenceMessages'][$key]) ? $validation['_presenceMessages'][$key] : null)];
                }
                if($field['isEmptyAllowed'] === true){
                    $result[$key] = [
                        'empty'=> (isset($validation['_allowEmptyMessages'][$key]) ? $validation['_allowEmptyMessages'][$key] : null)
                    ];
                }
            }
        }
        return $result;
    }


    /**
     * vytvoreni select listu pro angular
     */
    public function SelectList($data){
        $list = [];
        if (isset($data) && !empty($data))
        foreach($data AS $klist=>$kdata){
            if (!empty($kdata)){
                foreach($kdata AS $k=>$value){
                    $list[$klist][] = [
                        'id'=>$k,
                        'value'=>$value,
                    ];
                }
            }
        }
        //pr($list);die();
        return $list;
    }



    /**
     * prevod casu z angular do PHP
     */
    public function convertTime($data){
        foreach($data AS $k=>$d){
            if (isset($d['year']) && isset($d['month']) && isset($d['day'])){
                $date = $d['year'].'-'.$d['month'].'-'.$d['day'];
                $date = new Date($date);
                $data[$k] = $date;
            }
            if (isset($d['year']) && isset($d['month']) && isset($d['day']) && isset($d['hours'])){
                $date = $d['year'].'-'.$d['month'].'-'.$d['day'].' '.$d['hours'].':'.$d['min'].':'.$d['sec'];
                $date = new Time($date);
                $data[$k] = $date;
            }

        }
        /*
        if (strpos($data,':') === false){
            $data.= ' 00:00:00';
        }
        $data = new Time($data);
        */
        return $data;
    }

    /**
     * create empty entity
     */
    public function emptyEntity($model,$contain=null){
        $this->controller = $this->_registry->getController();
        //pr($this->controler);die();
        $this->controller->loadModel($model);
        $columns = array_fill_keys($this->controller->$model->schema()->columns(), '');
        unset($columns['trash']);
        unset($columns['created']);
        unset($columns['modified']);
        
        if (isset($columns['status'])){
            $columns['status'] = 1;
        }


        if(!empty($contain)){
            $col_contain = [];
            foreach($contain as $type => $con){
                
                //pr($con);die();
                $this->controller->loadModel($con);
                //pr($type);
                //pr($this->controller->$con->schema());
                if ($type == 'hasMany'){
                    $c = array_fill_keys($this->controller->$con->schema()->columns(), '');
                    unset($c['trash']);
                    unset($c['created']);
                    unset($c['modified']);
                    
                    $col_contain[Inflector::tableize($con)][] = $c;
                } else {
                    $c = array_fill_keys($this->controller->$con->schema()->columns(), '');
                    unset($c['trash']);
                    unset($c['created']);
                    unset($c['modified']);
                    
                    $col_contain[Inflector::tableize($con)] = $c;
                    
            
                }
               
                //pr($col_contain);die();
            }
        }
        //pr($col_contain);die();

        $params = ['validate'=>false];

        if (isset($col_contain)){
            $columns = array_merge($columns,$col_contain);
            
            $params['associated'] = [];
            foreach($col_contain AS $colName=>$col){
                $params['associated'][$colName] = ['validate' => false];
            }
         }

        $data = $this->controller->$model->newEntity($columns,$params);
        //pr($data);die();
       
        
        return $data;
    }

    public function convertLoadData($data,$defaultValues=null){
        foreach($data->toArray() AS $k=>&$d){
            if (isset($d->timezone)){
                $data[$k] = $d->format('Y-m-d H:i:s');
            }
            //if ($data[$k] == '')
            //$data[$k] = 'a';
            if (isset($defaultValues)){
                foreach($defaultValues AS $keyVal=>$val){
                    if (isset($data[$keyVal]) && ($data[$keyVal] == '')){
                        $data[$keyVal] = $val;
                    }
                    if ($data[$keyVal] == null){
                        $data[$keyVal] = $val;
                    }
                    if (isset($data[$keyVal]) && $data[$keyVal] == true){
                        $data[$keyVal] = 1;
                    }
                }
            }
        }
        return $data;
    }

    public function checkErrors($entity){
        //pr($entity->errors());
        if ($entity->errors()){
            $errorsList = [];
            foreach($entity->errors() AS $key=>$error){
               // pr($error);die();
                if (isset($error['phone']['length']))
                    $errorsList[] = ['message'=>$error['phone']['length'],'key'=>$key];
                if (isset($error['length']))
                    $errorsList[] = ['message'=>$error['length'],'key'=>$key];
                if (isset($error['_empty']))    
                    $errorsList[] = ['message'=>$error['_empty'],'key'=>$key];
                if (isset($error['_required']))    
                    $errorsList[] = ['message'=>$error['_required'],'key'=>$key];
                if (isset($error['validFormat']))    
                    $errorsList[] = ['message'=>$error['validFormat'],'key'=>$key];
            }
            $results = [
                'result'=>false,
                'errorsList'=>$errorsList,

            ];    
            die(json_encode($results));
            //$this->setJsonResponse($results);
            
            //return false;
        } else {
            return;
        }
        
        //pr($errorsList);
        //die('a');

    }

    
    public function convertPagination(){
        $this->range = 5;
        $pag = $this->request->paging[$this->request->params['controller']]; 
        if($pag['pageCount'] > $this->range){
            $start = ($pag['page'] <= $this->range)?1:($pag['page'] - $this->range);
            $end   = ($pag['pageCount'] - $pag['page'] >= $this->range)?($pag['page']+$this->range): $pag['pageCount'];
        }else{
            $start = 1;
            $end   = $pag['pageCount'];
        } 
        //pr($start);   
        //pr($pag['page'] - $this->range);   
        //pr($pag);   
        //pr($end);   
        $pagination = $this->request->paging[$this->request->params['controller']];
        $range = [];
        for($i = $start;$i<=$end; $i++){
            $range[] = $i;
        }
        //pr($range);

        $pagination['range'] = $range;
        $pagination['start'] = $start;
        $pagination['end'] = $end;
        //pr($pagination);die();
        return $pagination;

        
    }

   

    
}