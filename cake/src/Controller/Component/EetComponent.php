<?php
namespace App\Controller\Component;
use Cake\Controller\Component;

use FilipSedivy\EET\Certificate;
use FilipSedivy\EET\Dispatcher;
use FilipSedivy\EET\Receipt;
use FilipSedivy\EET\Utils\UUID;

use Cake\I18n\Time;
use Cake\I18n\Date;
use Cake\Utility\Inflector;

class EetComponent extends Component {
    // testovaci prostredi
    var $testingEET = true;

    var $options = [
        'uuid_zpravy'=>null, // uu id zpravy
        'order_id'=>null, // id objednavky
        'id_provoz'=>null, // id provozovny
        'id_pokl'=>null, // id pokladny
        'dic_popl'=>null, // dic poplatnika
        'dic_poverujiciho'=>null, // dic poverujiciho
        'pobocka_id'=>null, // cislo pobocky
        'porad_cis'=>null, // poradove cislo, nebo cislo uctu ,možná nastavit UNIQUE ID
        'dat_trzby'=>null, // datum trzby
        'celk_trzba'=>null, // celkova trzba
        'storno'=>false, // pokud je storno
    ];

    public function initialize(array $options) 
    {
        $this->controler = $this->_registry->getController();
        $this->setOptions($options);
    }

    /**
     * set options from controller
     */
	function setOptions($options){
        foreach($options AS $k=>$v){
            $this->options[$k] = $v;
        }
        //$this->options['order_id'] 	= $options['order_id'];
        
        $uuid = UUID::v4();
        $this->options['uuid_zpravy'] = $uuid;
	
	}

    /**
     * prepare data from eet
     */
    public function prepareData(){
        //pr($this->options);
        
        $r = new Receipt;
		$r->uuid_zpravy = $this->options['uuid_zpravy'];
		$r->id_provoz = $this->options['id_provoz'];//$setting["id_provoz"];
		$r->id_pokl = $this->options['id_pokl'];//$setting["id_pokl"];
		$r->dic_popl = $this->options['dic_popl'];//$setting["dic_popl"];
		$r->dic_poverujiciho = $this->options['dic_popl'];
		$r->pobocka_id = $this->options['pobocka_id'];//$_SESSION['Auth']['User']['pobocka'];
		$r->porad_cis = $this->options['porad_cis']; //možná nastavit UNIQUE ID
		$r->dat_trzby = $this->options['dat_trzby'];
		$r->celk_trzba = $this->options['celk_trzba'];
        $r->storno = $this->options['storno'];
       
        $this->sendToEet($r);
        return $this->result;
    }

    /**
     * odeslani na server EET
     */
    private function sendToEet($receipt){
        $certificate = new Certificate('cert/eet.p12', 'eet');
      
        if($this->testingEET == true) {
			$certificate = new Certificate('cert/'.$this->options['cert'], 'eet');
			$dispatcher = new Dispatcher($certificate);
			$dispatcher->setPlaygroundService();
		} else {
			$certificate = new Certificate('cert/2861669664.p12', '12091988');
			$dispatcher = new Dispatcher($certificate);
			$dispatcher->setProductionService();
        }
        try {
            // Odeslání tržby
            $dispatcher->send($receipt);
            $this->result = array(
                "result"=>true,
                "order_id" => $this->options['order_id'],
                "fik" => $dispatcher->getFik(),
                "bkp" => $dispatcher->getBkp(),
                "uid" => $receipt->uuid_zpravy,
                "date_send" => $receipt->dat_trzby,
                "storno" => $receipt->storno,
                "price" => $receipt->celk_trzba,
                "eet_time" => new \DateTime(),
                "message" =>'Tržba odeslána v normálním režimu',
            );    
            // Tržba byla úspěšně odeslána
            //echo sprintf('FIK: %s <br>', $dispatcher->getFik());
            //echo sprintf('BKP: %s <br>', $dispatcher->getBkp());
        } catch(\FilipSedivy\EET\Exceptions\EetException $ex){
            // Tržba nebyla odeslána
            $this->result = array(
                "result"=>true,
                "order_id" => $this->options['order_id'],
                "eet_offline"=>1,
                "fik" => $dispatcher->getFik(),
                "bkp" => $dispatcher->getBkp(),
                "uid" => $receipt->uuid_zpravy,
                "date_send" => $receipt->dat_trzby,
                "storno" => $receipt->storno,
                "price" => $receipt->celk_trzba,
                "eet_time" => new \DateTime(),
                "message" =>'Není připojení k internetu',
            );
            //echo sprintf('BKP: %s <br>', $dispatcher->getBkp());
            //echo sprintf('PKP: %s <br>', $dispatcher->getPkp());
        } catch(\FilipSedivy\EET\Exceptions\ServerException $ex){
            // Obecná chyba
            $this->result = array(
                "result"=>false,
                "order_id" => $this->options['order_id'],
                "uid" => $receipt->uuid_zpravy,
                "date_send" => $receipt->dat_trzby,
                "storno" => $receipt->storno,
                "price" => $receipt->celk_trzba,
                "eet_time" => new \DateTime(),
                "message" =>$ex->getMessage(),
            );
            //die('a');
            //pr($ex->getMessage());
        }

        //return $result;
        //pr($dispatcher->processError());

		
    }

	public function prepare($billId, $storno = false){
		$st = TableRegistry::get("Settings");
		$setting = $st->get(1);

		if($billId <= 0){
			return false;
		}

		$bill = $this->getData($billId);
		$priceAll = $this->calculate($bill);

		$date_send = Time::now();
		if($storno === true) {
			$priceAll = -1 * $priceAll;
		}

		$uuid = UUID::v4();
		//pr($setting["id_pokl"]); die();
		$r = new Receipt;
		$r->uuid_zpravy = $uuid;
		$r->id_provoz = $setting["id_pokl"];//$setting["id_provoz"];
		$r->id_pokl = $setting["id_pokl"];//$setting["id_pokl"];
		$r->dic_popl = 'CZ6361040719';//$setting["dic_popl"];
		$r->dic_poverujiciho = 'CZ6361040719';
		$r->pobocka_id = $setting["id_pokl"];//$_SESSION['Auth']['User']['pobocka'];
		$r->porad_cis = $billId; //možná nastavit UNIQUE ID
		$r->dat_trzby = new \DateTime();
		$r->celk_trzba = $priceAll;
		$r->storno = $storno;
		//pr($r); die();
		return $r;
	}

	public function send($receipt){
		
		$Settings = TableRegistry::get("Settings");
		$testingEET = true;
		//$data = $Settings->find()->where(['id_provoz'=>$_SESSION['Auth']['User']['pobocka']])->first();
		$data = $Settings->get(1);
		
		if($testingEET == true) {
			$certificate = new Certificate('cert/eet.p12', 'eet');
			$dispatcher = new Dispatcher($certificate);
			$dispatcher->setPlaygroundService();
		} else {
			$certificate = new Certificate('cert/2861669664.p12', '12091988');
			$dispatcher = new Dispatcher($certificate);
			$dispatcher->setProductionService();
		}
		//$certificate = new Certificate('cert/'.$data->name, $data->cerf_pass);
		
		//$certificate = new Certificate('cert/2861669664.p12', '12091988');

		$dispatcher->send($receipt);

		$result = array(
			"fik" => $dispatcher->getFik(),
			"bkp" => $dispatcher->getBkp(),
			"uid" => $receipt->uuid_zpravy,
			"date_send" => $receipt->dat_trzby,
			"storno" => $receipt->storno,
			"price" => $receipt->celk_trzba
		);
		
		//pr($dispatcher); die();
		
		return $result;
	}
}