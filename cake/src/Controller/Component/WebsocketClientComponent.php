<?php 
namespace App\Controller\Component;
use Cake\Controller\Component;

use Cake\I18n\Time;
use Cake\I18n\Date;
use Cake\Utility\Inflector;

class WebsocketClientComponent extends Component {

	var $host = 'localhost';  //where is the websocket server
	var $port = 8080;
	var $local = "http://localhost/api/hang/";  //url where this script run
	
	
	
	
	function applySetting($setting){
		$this->host 	= $setting['host'];
		$this->port 	= $setting['port'];
		$this->local 	= $setting['local'];
	
	
	}
	
	function sendCall($setting,$message,$end=false){
		$this->applySetting($setting);
		
		$data = array(
			//'type'=>'call',
			'data'=>$message,
		);  //data to be send
		
		
		if ($end == false){
			$data['type'] = 'call';
		} else {
			if ($end == 'hang')
				$data['type'] = 'hang';
			else
				$data['type'] = 'end_call';
		}
		//pr($data);
		$data = json_encode($data);
		$head = "GET / HTTP/1.1"."\r\n".
				"Upgrade: WebSocket"."\r\n".
				"Connection: Upgrade"."\r\n".
				"Origin: $this->local"."\r\n".
				"Host: $this->host"."\r\n".
				"Sec-WebSocket-Protocol: json\r\n".
				"Sec-WebSocket-Version: 13\r\n".
				"Sec-WebSocket-Key: asdasdaas76da7sd6asd6as7d"."\r\n".
				"Content-Length: ".strlen($data)."\r\n"."\r\n";
				
		//WebSocket handshake
			//pr($this->host);
			//pr($this->port);
			$sock = @fsockopen($this->host, $this->port, $errno, $errstr, 1);
            stream_set_timeout($sock, 3);
            //$sock = @fsockopen($this->host, $this->port, $errno, $errstr, 2);
			
            if (!$sock)
            
			die(json_encode(['result'=>false,'message'=>'Neni websocket otevreny']));
			
			fwrite($sock, $head ) or die('error:'.$errno.':'.$errstr);
			$headers = fread($sock, 2000);
			//echo $headers;

			fwrite($sock, $this->hybi10Encode($data)) or die('error:'.$errno.':'.$errstr);
			$wsdata = fread($sock, 2000);
			//var_dump($this->hybi10Decode($wsdata));
			$result = $this->hybi10Decode($wsdata);
            fclose($sock);
            die(json_encode(['result'=>true,'message'=>'Telefon odeslan','result'=>$result]));
			//return $result;
		
	}

	function send_online($setting,$message,$end=false){
		$this->applySetting($setting);
		
		$data = array(
			//'type'=>'call',
			'data'=>$message,
		);  //data to be send
		
		$data['type'] = 'online';
		
		//pr($data);
		$data = json_encode($data);
		$head = "GET / HTTP/1.1"."\r\n".
				"Upgrade: WebSocket"."\r\n".
				"Connection: Upgrade"."\r\n".
				"Origin: $this->local"."\r\n".
				"Host: $this->host"."\r\n".
				"Sec-WebSocket-Protocol: json\r\n".
				"Sec-WebSocket-Version: 13\r\n".
				"Sec-WebSocket-Key: asdasdaas76da7sd6asd6as7d"."\r\n".
				"Content-Length: ".strlen($data)."\r\n"."\r\n";
				
		//WebSocket handshake
		
			$sock = @fsockopen($this->host, $this->port, $errno, $errstr, 2);
			
			if (!$sock) die('Není socket');
			
			fwrite($sock, $head ) or die('error:'.$errno.':'.$errstr);
			$headers = fread($sock, 2000);
			//echo $headers;

			fwrite($sock, $this->hybi10Encode($data)) or die('error:'.$errno.':'.$errstr);
			$wsdata = fread($sock, 2000);
			//var_dump($this->hybi10Decode($wsdata));
			$result = $this->hybi10Decode($wsdata);
			//pr($result);
			return $result;
		
	}

	function hybi10Decode($data){
		$bytes = $data;
		$dataLength = '';
		$mask = '';
		$coded_data = '';
		$decodedData = '';
		
		$secondByte = (isset($bytes[1])?sprintf('%08b', ord($bytes[1])):'');
		$masked = (isset($secondByte[0]) && $secondByte[0] == '1') ? true : false;
        if (isset($bytes[1]))
        $dataLength = ($masked === true) ? ord($bytes[1]) & 127 : ord($bytes[1]);
        else
        $dataLength = 0;
		if($masked === true)
		{
			if($dataLength === 126)
			{
			   $mask = substr($bytes, 4, 4);
			   $coded_data = substr($bytes, 8);
			}
			elseif($dataLength === 127)
			{
				$mask = substr($bytes, 10, 4);
				$coded_data = substr($bytes, 14);
			}
			else
			{
				$mask = substr($bytes, 2, 4);       
				$coded_data = substr($bytes, 6);        
			}   
			for($i = 0; $i < strlen($coded_data); $i++)
			{       
				$decodedData .= $coded_data[$i] ^ $mask[$i % 4];
			}
		}
		else
		{
			if($dataLength === 126)
			{          
			   $decodedData = substr($bytes, 4);
			}
			elseif($dataLength === 127)
			{           
				$decodedData = substr($bytes, 10);
			}
			else
			{               
				$decodedData = substr($bytes, 2);       
			}       
		}   

		return $decodedData;
	}


	function hybi10Encode($payload, $type = 'text', $masked = true) {
		$frameHead = array();
		$frame = '';
		$payloadLength = strlen($payload);

		switch ($type) {
			case 'text':
				// first byte indicates FIN, Text-Frame (10000001):
				$frameHead[0] = 129;
				break;

			case 'close':
				// first byte indicates FIN, Close Frame(10001000):
				$frameHead[0] = 136;
				break;

			case 'ping':
				// first byte indicates FIN, Ping frame (10001001):
				$frameHead[0] = 137;
				break;

			case 'pong':
				// first byte indicates FIN, Pong frame (10001010):
				$frameHead[0] = 138;
				break;
		}

		// set mask and payload length (using 1, 3 or 9 bytes)
		if ($payloadLength > 65535) {
			$payloadLengthBin = str_split(sprintf('%064b', $payloadLength), 8);
			$frameHead[1] = ($masked === true) ? 255 : 127;
			for ($i = 0; $i < 8; $i++) {
				$frameHead[$i + 2] = bindec($payloadLengthBin[$i]);
			}

			// most significant bit MUST be 0 (close connection if frame too big)
			if ($frameHead[2] > 127) {
				$this->close(1004);
				return false;
			}
		} elseif ($payloadLength > 125) {
			$payloadLengthBin = str_split(sprintf('%016b', $payloadLength), 8);
			$frameHead[1] = ($masked === true) ? 254 : 126;
			$frameHead[2] = bindec($payloadLengthBin[0]);
			$frameHead[3] = bindec($payloadLengthBin[1]);
		} else {
			$frameHead[1] = ($masked === true) ? $payloadLength + 128 : $payloadLength;
		}

		// convert frame-head to string:
		foreach (array_keys($frameHead) as $i) {
			$frameHead[$i] = chr($frameHead[$i]);
		}

		if ($masked === true) {
			// generate a random mask:
			$mask = array();
			for ($i = 0; $i < 4; $i++) {
				$mask[$i] = chr(rand(0, 255));
			}

			$frameHead = array_merge($frameHead, $mask);
		}
		$frame = implode('', $frameHead);
		// append payload to frame:
		for ($i = 0; $i < $payloadLength; $i++) {
			$frame .= ($masked === true) ? $payload[$i] ^ $mask[$i % 4] : $payload[$i];
		}

		return $frame;
	}

}
?>