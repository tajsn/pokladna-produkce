<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class UploaderComponent extends Component {
    private $ftp_setting = [];
    private $allow_resizing = true;
    private $max_image_height_res = 1000;
    private $max_image_width_res = null;
    private $max_image_size = 10; //MB
    private $move_to_ftp = true; 
    public $keep_original_filename = false;
    private $generated_filename_type = 'numeric';  //unique, numeric
    
    public function initialize(array $config) {
       $setting = $this->request->session()->read('domainSetting');
       if(!isset($setting->ftp) || empty($setting->ftp)){
           throw new \Exception('Nejsou nastaveny FTP udaje');
       }
       $this->ftp_setting = $setting->ftp;
    }
    /**
     * Upload file to target directory
     * @param type $path
     * @return type
     * @throws \Exception
     */
    public function upload($path = '') {
        if (isset($_FILES['file']['name'])) {
            //Temporary file that must be moved to target location
            $tmp_file = $_FILES['file']['tmp_name'];
            $original_filename = $_FILES['file']['name'];
           
            //Directory in uploaded where new file should be stored in...
                //We remove . because of injection! Can't allow to access lower dir like ../../something
                //We translate | to / because multilevel directory
            $target_dir = trim(str_replace('|', '/', str_replace('.', '', $path)), '/');

            //Check maximum size of uploaded image ... must be limited because of memory overflow...
            if ($_FILES['file']['size'] > ($this->max_image_size * 1000000)) {
                throw new \Exception('Maximální velikost nahrávaného obrázku je ' . $this->max_image_size . 'MB, zmenšete obrázek před jeho nahrátím');
            }
           
            //Check if directory exist and is writable
            $full_target_dir_path = $this->checkDirectory($target_dir);
           
            if($this->keep_original_filename){
                //We keep original file name, but without extension
                $new_filename = friendlyUri($this->getFileBasename($original_filename));
            }else{
                //Generated file name without extension
                $new_filename = $this->createFilename($target_dir);
            }
          
            if ($this->allow_resizing == true) {
                try{
                    $ext = $this->resize($tmp_file);
                }catch(\Exception $e){
                    throw new \Exception('Nepodařilo se zmenšit obrázek: ' . $e->getMessage());
                }
            }else{
                $ext = $this->getFileExtension($original_filename);
            }
            
            //new filename together with its extension
            $full_new_filename = $new_filename . '.' . $ext;

            if (!$this->move_to_ftp) {
                //Move tmp file to local directory
                if (@!move_uploaded_file($tmp_file, $full_target_dir_path . $full_new_filename)) {
                    throw new \Exception('Dočasný soubor ' . $tmp_file . ' nemůže být přesunut do lokalniho adresare '.$target_dir.'. Ověřte zda má oprávnění pro zápis.');
                }
            } else {
                if (($conn_id = ftp_connect($this->ftp_setting['host'])) !== false) {
                    if (ftp_login($conn_id, $this->ftp_setting['user'], $this->ftp_setting['password'])) {
                        if (@!ftp_put($conn_id, $full_target_dir_path . $full_new_filename , $tmp_file, FTP_BINARY)) {
                            throw new \Exception('Dočasný soubor ' . $tmp_file . ' nemůže být přesunut do vzdáleného adresáře '.$target_dir.'. Ověřte zda má oprávnění pro zápis.');
                        }
                    } else {
                        throw new \Exception('Nepodařilo se přihlásit k FTP');
                    }
                    ftp_close($conn_id);
                }else{
                     throw new \Exception('Host ' . $this->ftp_setting['host'] . ' není dostupny');
                }
                
            }

            return array(
                'file_name' => $full_new_filename,
                'title' => $original_filename
            );
        }else{
            throw new \Exception('Nenalezen soubor k nahrání');
        }
    }
    /**
     * Check if directory exist and is writable, return full target dir path
     * @param type $target_dir
     * @return string
     * @throws \Exception
     */
    private function checkDirectory($target_dir){
         if (!$this->move_to_ftp) {
            //Absolute path to target directory
            $full_target_dir_path = UPLOADED_PATH . $target_dir . DS;
            if (!is_dir($full_target_dir_path)) {
                if(!$this->createDir($target_dir)){
                    throw new \Exception('Adresař ' . $full_target_dir_path . ' neexistuje');
                }
            }
            if (!is_writable($full_target_dir_path)) {
                throw new \Exception('Adresař ' . $full_target_dir_path . ' nemá přidělena práva pro zápis');
            }
        }else{
            $full_target_dir_path = $this->ftp_setting['path'] . $target_dir . '/';
            if(!$this->ftpDirExist($full_target_dir_path)){
                if(!$this->createDir($target_dir)){
                    throw new \Exception('Vzdálený adresař ' . $full_target_dir_path . ' neexistuje');
                }
            }
            //TODO check ftp path
        }
        
        return $full_target_dir_path;
    }
    
    /**
     * Resize image to target resolution
     * If $targetFile is empty, original file will be rewrited in same location it is stored
     * @param type $originalFile
     * @param type $targetFile
     * @return string
     * @throws Exception
     */
    private function resize($originalFile, $targetDirectory = null, $targetFilename = null) {
        $newHeight = $this->max_image_height_res;
        $newWidth = $this->max_image_width_res;
        $info = getimagesize($originalFile);
     
       
        if (!$targetDirectory || !$targetFilename) {
            $targetFile = $originalFile;
            $withoutExt = true;
        }else{
            $targetFile = $targetDirectory . $targetFilename;
            $withoutExt = false;
        }

        switch ($info['mime']) {
            case 'image/jpeg':
                $image_create_func = 'imagecreatefromjpeg';
                $image_save_func = 'imagejpeg';
                $new_image_ext = 'jpg';
                break;

            case 'image/png':
                $image_create_func = 'imagecreatefrompng';
                $image_save_func = 'imagepng';
                $new_image_ext = 'png';
                break;

            case 'image/gif':
                $image_create_func = 'imagecreatefromgif';
                $image_save_func = 'imagegif';
                $new_image_ext = 'gif';
                break;

            default:
                throw new Exception('Nepovolený typ obrázku ('.$info['mime'].')');
        }

        if( ($this->max_image_height_res && $this->max_image_height_res > $info[1]) || 
            ($this->max_image_width_res && $this->max_image_width_res > $info[0])){
            return $new_image_ext;
        }
        
        $img = $image_create_func($originalFile);
        list($width, $height) = getimagesize($originalFile);
    
        if (!$newWidth) {
            $newWidth = ($width / $height) * $newHeight;
        } else if (!$newHeight) {
            $newHeight = ($height / $width) * $newWidth;
        }else{
            throw new Exception('Neni zvolen cílový rozměr výsledného obrázku (výška ani šířka).');
        }
        $tmp = imagecreatetruecolor($newWidth, $newHeight);
        if($info['mime'] == 'image/png'){
            imagecolortransparent($img);
            imagealphablending($tmp, false);   
            $color = imagecolorallocatealpha($tmp, 0, 0, 0, 127);   
            imagefill($tmp, 0, 0, $color);   
            imagesavealpha($tmp, true);
        }
        imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        $full_target_filename = $targetFile . ( $withoutExt ? '' : '.' . $new_image_ext);
        if (file_exists($full_target_filename)) {
            unlink($full_target_filename);
        }
        $image_save_func($tmp, $full_target_filename);
        return $new_image_ext;
    }
    /**
     * Remove file from ftp direcoty or local directory
     * @param type $filename
     * @param type $path
     * @return boolean
     */
    public function unlinkFile($filename, $path = ''){
        if($this->move_to_ftp){
            if (($conn_id = ftp_connect($this->ftp_setting['host'])) !== false) {
                if (ftp_login($conn_id, $this->ftp_setting['user'], $this->ftp_setting['password'])) {
                    if(@ftp_delete($conn_id, $this->ftp_setting['path'] . trim($path, DS) . '/'. $filename)){
                        ftp_close($conn_id);
                        return true;
                    }
                } 
                ftp_close($conn_id);
            }
            return false;
        }else{
            if(unlink(UPLOADED_PATH . $path . DS . $filename )){
                return true;
            }else{
                return false;
            }
        }
    }
    /**
     * Get filename without extension
     * @param type $filename
     * @return type
     */
    public function getFileBasename($filename){
        $p = new \SplFileInfo($filename);
        $filename = $p->getBasename();
        $ext = $p->getExtension();
        return rtrim($filename, '.'.$ext);
    }
    /**
     * Get extension of filename
     * @param type $filename
     * @return type
     */
    public function getFileExtension($filename){
        $p = new \SplFileInfo($filename);
        return strtolower($p->getExtension());
    }
    
    /**
     * Generate filename of ne file by setting
     * @param type $path
     * @return boolean
     */
    private function createFilename($path) {
        if($this->generated_filename_type == 'numeric'){
            if ($this->move_to_ftp) {
                if (($lastFilename = $this->getHighestFilenameFromFtp($path)) !== false) {
                    //Remove extension
                    $filename = $this->getFileBasename($lastFilename); 
                    //Increment it
                    $filename = intval($filename) + 1;
                    return $filename;
                }
            } else {
                //GET last local filename
            }
            return time();
        }else if($this->generated_filename_type == 'unique'){
            return uniqid();
        }
        return false;
    }

        /**
     * Check if directory exist on ftp connection
     * @param type $dir
     * @return type
     */
    private function ftpDirExist($dir){
        return is_dir('ftp://'.$this->ftp_setting['user'].':'.$this->ftp_setting['password'].'@'.$this->ftp_setting['host'].'/' . $dir);
    }
    
    /**
     * Recursively create directory on ftp or locale store
     * @param type $dir
     * @param type $recursive
     * @return boolean
     */
    private function createDir($dir, $recursive = true){
        
        if($this->move_to_ftp){
            if($recursive){
                $path_parts = explode('/', trim($dir,'/'));
                if(Count($path_parts) > 1){
                    $path = '';
                    foreach($path_parts as $subdir){
                        $path .= $subdir .'/';
                        if(!$this->ftpDirExist($path)){
                            if(!$this->createDir($path, false)){
                                break;
                                return false;
                            }
                        }
                    }
                    return true;
                }
            }
            
            if (($conn_id = ftp_connect($this->ftp_setting['host'])) !== true) {
                if (ftp_login($conn_id, $this->ftp_setting['user'], $this->ftp_setting['password'])) {
                    if(@ftp_mkdir($conn_id, $this->ftp_setting['path'] . $dir . '/')){
                        ftp_close($conn_id);
                        return true;
                    }
                }
                ftp_close($conn_id);
            }
        }else{
            if(mkdir(UPLOADED_PATH . $dir . DS)){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Connect to ftp and get highest filename (if there are files named numericaly) from directory
     * @param type $path
     * @return boolean|string
     */
    private function getHighestFilenameFromFtp($path) { 
        $list = null;
        if (($conn_id = ftp_connect($this->ftp_setting['host'])) !== true) {
            if (ftp_login($conn_id, $this->ftp_setting['user'], $this->ftp_setting['password'])) {
                $list = ftp_nlist($conn_id, $this->ftp_setting['path'] . $path . '/');
            }else{
                return false;
            }
            ftp_close($conn_id);
        }else{
            return false;
        } 
        if (!empty($list)) {
            natsort($list);
            return end($list);
        } else { 
            return '0.png';
        }
    }

}
