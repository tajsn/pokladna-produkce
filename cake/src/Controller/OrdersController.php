<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;
use App\Component\WebsocketClient;

class OrdersController extends AppController{
    
    /**
     * vytvoreni select listu
     */
    public function getSelectList($check=true){
        if ($check == false || isset($this->request->query['firstLoad'])){
            $this->loadModel('Deliverys');
            $this->loadModel('Settings');
            return $select_list = [
                'close_order_list'=>$this->close_order_list,
                'no_yes'=>$this->no_yes,
                'area_list'=>$this->area_list = [],
                'source_list'=>$this->source_list,
                'payment_list'=>$this->payment_list,
                'storno_type_list'=>$this->storno_type_list,
                'distributor_list'=>$this->Deliverys->deliveryList(),
                'operations_list'=>$this->Settings->operationsList(),
            ];
        } else {
            return false;
        }  
        
        
    }

    /**
     * INDEX orders
     */
    public function index()
    {
        $this->loadComponent('vI');

        $select_list = $this->getSelectList();
        $conditions = [];

        $fields_defined = [
            0=>['col'=>'id','title'=>'ID','type'=>'text'],
            1=>['col'=>'name','title'=>'Jméno','type'=>'text'],
            2=>['col'=>'web_id','title'=>'WEB ID','type'=>'text'],
            3=>['col'=>'total_price','title'=>'Cena','type'=>'text'],
            4=>['col'=>'phone_call','title'=>'Telefon','type'=>'text'],
            5=>['col'=>'street','title'=>'Ulice','type'=>'text'],
            6=>['col'=>'city','title'=>'Město','type'=>'text'],
            7=>['col'=>'area_id','title'=>'Oblast','type'=>'list','list_data'=>$select_list['area_list']],
            8=>['col'=>'source_id','title'=>'Zdroj','type'=>'list','list_data'=>$select_list['source_list']],
            9=>['col'=>'payment_id','title'=>'Platba','type'=>'list','list_data'=>$select_list['payment_list']],
            10=>['col'=>'distributor_id','title'=>'Rozvozce','type'=>'list','list_data'=>$select_list['distributor_list']],
            11=>['col'=>'paymentSessionId','title'=>'Gopay ID','type'=>'text'],
            12=>['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
            13=>['col'=>'close_order','title'=>'Dokončeno','type'=>'list','list_data'=>$select_list['close_order_list']],
            14=>['col'=>'storno','title'=>'Storno','type'=>'list','list_data'=>$select_list['no_yes']],
            15=>['col'=>'storno_type','title'=>'Storno typ','type'=>'list','list_data'=>$select_list['storno_type_list']],
            16=>['col'=>'operating','title'=>'Provozní','type'=>'list','list_data'=>$select_list['no_yes']],
        ];

        $this->filtration_defined = [
            'id'=>['col'=>'id','title'=>'ID','type'=>'text'],
            'web_id'=>['col'=>'web_id','title'=>'WEB ID','type'=>'text'],
            'name'=>['col'=>'name','title'=>'Jméno','type'=>'text_like'],
            'phone_call'=>['col'=>'phone_call','title'=>'Telefon','type'=>'text_like'],
            'created'=>['col'=>'created','title'=>'Vytvořeno','type'=>'date_range'],
            //'modified'=>['col'=>'modified','title'=>'Upraveno','type'=>'date'],
            'source_id'=>['col'=>'source_id','title'=>'Zdroj','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['source_list'])],
            'payment_id'=>['col'=>'payment_id','title'=>'Platba','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['payment_list'])],
            'distributor_id'=>['col'=>'distributor_id','title'=>'Platba','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['distributor_list'])],
            'area_id'=>['col'=>'area_id','title'=>'Oblast','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['area_list'])],
            'close_order'=>['col'=>'close_order','title'=>'Dokončeno / nedokončeno','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['close_order_list'])],
            'storno'=>['col'=>'storno','title'=>'Storno','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['no_yes'])],
            'storno_type'=>['col'=>'storno_type','title'=>'Storno typ','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['storno_type_list'])],
            'operating'=>['col'=>'operating','title'=>'Provozní','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['no_yes'])],
        ];
        
        
        if (isset($this->request->data['conditions'])){
            $conditions = $this->vI->convertConditions($conditions,$this->request->data['conditions'],$this->filtration_defined);
            //pr($conditions);die();
        }
        
       
        $posibility = [
            0=>['link'=>'/api/orders/show/','title'=>__('Zobrazit'),'class'=>'fa-search','type'=>'ajax','params'=>'showPrint'],
            1=>['link'=>'/api/orders/reprint/','title'=>__('Tisk'),'class'=>'fa-print','type'=>'ajax','params'=>'print'],
            2=>['link'=>'/api/orders/storno/','title'=>__('Storno upečeno'),'class'=>'fa-battery-full','type'=>'ajax','params'=>'storno'],
            3=>['link'=>'/api/orders/storno_nobake/','title'=>__('Storno neupečeno'),'class'=>'fa-battery-quarter','type'=>'ajax','params'=>'storno'],
            4=>['link'=>'/api/orders/backBon/','title'=>__('Vrátit zpět na výběr rozvozců'),'class'=>'fa-undo','type'=>'ajax','params'=>'backBon','confirm'=>'Opravdu vrátit zpět?'],
            
            //0=>['link'=>'/api/status/Orders/','title'=>__('Status'),'class'=>'fa-check-circle-o','type'=>'ajax','params'=>'status'],
            //1=>['link'=>'./edit/','title'=>__('Editovat'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
            //2=>['link'=>'/api/trash/Orders/','title'=>__('Smazat'),'class'=>'fa-trash','type'=>'ajax','params'=>'trash','confirm'=>__('Opravdu smazat?')],
        ];
        
        $top_actions = [
            //0=>['link'=>'./edit/','title'=>__('Nová položka'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
        ];

        if (!$this->loggedUser){
            unset($posibility[2]);
            unset($posibility[3]);
            sort($posibility);
        }

        
        $fields = $this->vI->fieldsConvert($fields_defined);
        //pr($fields);die();
        $mapper = function ($data, $key, $mapReduce) {
            $data->close_order = (($data->close_order)?1:0);
            $data->storno = (($data->storno)?1:0);
            $data->operating = (($data->operating)?1:0);
            $data->total_price = $this->price($data->total_price);
            $mapReduce->emit($data);
		};
        $query = $this->Orders->find()
            ->select($fields)
            ->where($conditions)
            ->mapReduce($mapper)
            ->cache(function ($query) {
				return 'orders_data-' . md5(serialize($query->clause('where')));
			})
        ;
        //$data = $query;
        
        
        $this->loadComponent('Paginator');
        $data_list = $this->paginate($query);
        
        ;
        
        $pagination = $this->vI->convertPagination();
        //pr($pagination['page']);
        //pr($this->paginate);
        //pr($data_list->toArray());die();
        
        $results = [
            'result'=>true,
            'data'=>$data_list->toArray(),
            'data_count'=>count($data_list->toArray()),
            'pagination'=>$pagination,
            'table_th'=>$fields_defined,
            'filtration'=>array_values($this->filtration_defined),
            'select_list'=>$select_list,
            'posibility'=>$posibility,
            'top_actions'=>$top_actions,
            'conditions'=>(object) $conditions,
        ];
        
        $this->setJsonResponse($results);
    }


    /**
     * back bon 
     */
    public function backBon($id){
        
        $resultDb = $this->Orders->updateAll(
			['distributor_id'=>null,], // fields
			['id' => $id]
        );
        die(json_encode(['result'=>true,'message'=>'Vráceno zpět na hlavní stránku']));
    }

    /**
     * show nahled objednavky
     */
    public function show($id){
        $data = $this->Orders->find()
        ->contain(['OrderItems'])
        ->where(['id'=>$id])
        ->first();
        //pr($data);
        $print_data = $this->jsonToPrinter($data);
        $results = [
            'result'=>true,
            'print_type'=>'show',
            'print_data'=>$print_data,
        ];
        die(json_encode($results));
    }

    /**
     * reprint objednavky
     */
    public function reprint($id){
        $data = $this->Orders->find()
        ->contain(['OrderItems'])
        ->where(['id'=>$id])
        ->first();
        //pr($data);
        $print_data = $this->jsonToPrinter($data);
        $results = [
            'result'=>true,
            //'print_type'=>'show',
            'print_data'=>$print_data,
        ];
        die(json_encode($results));
    }


    /**
     * kontrola zda neni bon pro stornovan v nejake uzaverce
     */
    private function checkStornoDeadlines($id){
        $this->loadModel('Deadlines');
        $deadline = $this->Deadlines->find()->where(['type_id'=>1])->select(['id','order_id_to'])->order('id DESC')->first();
        if ($deadline && $id < $deadline->order_id_to){
            die(json_encode(['result'=>false,'message'=>'Stornovat lze jen BON, který není ještě v uzávrce']));
        }
    }

    /**
     * storno objednavky
     */
    public function storno($id){
        $this->autoRender = false;
        
        $this->checkStornoDeadlines($id);

        $resultDb = $this->Orders->updateAll(
			['storno'=>1,'storno_type'=>1], // fields
			['id' => $id]
        );
        $order = $this->Orders->find()->where(['id'=>$id])->first();
        $results = [
            'result'=>true,
            'message'=>'Objednávka stornována',
            'data'=>$order,
        ];
        die(json_encode($results));
    }
    
    /**
     * storno neupeceno objednavky
     */
    public function stornoNobake($id){
        $this->autoRender = false;


        $this->checkStornoDeadlines($id);

        $resultDb = $this->Orders->updateAll(
			['storno'=>1,'storno_type'=>2], // fields
			['id' => $id]
        );
        $order = $this->Orders->find()->where(['id'=>$id])->first();
        $results = [
            'result'=>true,
            'message'=>'Objednávka stornována jako neupečeno',
            'data'=>$order,
        ];
        die(json_encode($results));
    }

    /**
     * editace
     */
    public function edit($id=null){
        $select_list = $this->getSelectList(false);
        $this->loadComponent('vI');
        //pr($select_list);die();
        
        // save data
        if (isset($this->request->data['saveData'])){
            $saveData = $this->request->data['saveData'];
            //pr($this->request->data['saveData']);
            $saveData = $this->vI->convertTime($saveData);

            //pr($saveData['close_order']);
            // kontrola produkty
            if (empty($saveData['billItems'])) {
                if ($saveData['close_order'] == 1){
                    die(json_encode(['result'=>true]));
                    
                } else {
                    die(json_encode(['result'=>false,'message'=>'Nejsou vybrány produkty']));
                }
            }

            // kontrola ulice
            if (empty($saveData['street'])) {
                die(json_encode(['result'=>false,'message'=>'Není zadána adresa']));
            }
            
            // zdroj
            if (empty($saveData['source_id'])) {
                $saveData['source_id'] = 1;
            }
            
            if (empty($saveData['lat'])){
                $address = $saveData['street'].' '.$saveData['city']; // Google HQ
                $prepAddr = str_replace(' ','+',$address);
                $ctx = stream_context_create(array(
                    'http'=>
                        array(
                            'timeout' => 5,  //1200 Seconds is 20 Minutes
                        ),
                    'https'=>
                        array(
                            'timeout' => 5,  //1200 Seconds is 20 Minutes
                        )
                    )
                );
                //pr($_SERVER);
                //$key = 'AIzaSyAmRIyQUbK5x_c_cm78ZV5ODf-sqVO1-2s';
                $key = 'AIzaSyDQ_FWjy2YdGCTCSHazaGs3ifwyzJ8TyyQ';
                //$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key='.$key, false, $ctx);
                $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr, false, $ctx);
                
                $output= json_decode($geocode);
                //pr($output);
                if (isset($output->results[0]->geometry->location->lat)){
                    $parts = array(
                        'street'=>array('street_number','route'),
                        'city'=>array('locality','administrative_area_level_2'),
                        'state'=>array('administrative_area_level_1'),
                        'zip'=>array('postal_code'),
                        'cp'=>array('premise'),
                        'co'=>array('street_number'),
                    );
                //    pr($output->results[0]->address_components);
                    if (!empty($output->results[0]->address_components)) {
                        $ac = $output->results[0]->address_components;
                        foreach($parts as $need=>$types) {
                            foreach($ac as $a) {
                            if (in_array($a->types[0],$types)) 
                                $address_out[$need] = $a->short_name;
                            elseif (empty($address_out[$need])) 
                                $address_out[$need] = '';
                          }
                        }
                        //pr($address_out);
                        $street = ' '.$address_out['cp'].(!empty($address_out['co'])?'/'.$address_out['co']:'');
                        $street = $address_out['street'].$street;
                        //pr($street);
                        $this->googleCity = $address_out['city'];
                        $this->googleStreet = $street;
                        
                    }
                    
                    if (isset($this->googleStreet)){
                        
                        $saveData['street'] = $this->googleStreet;
                    }
                    if (isset($this->googleCity)){
                        
                        $saveData['city'] = $this->googleCity;
                    }
                    //pr($output->results[0]);
                    

                    $lat = $output->results[0]->geometry->location->lat;
                    $lng = $output->results[0]->geometry->location->lng;
                    $saveData['lat'] = (!empty($lat)?$lat:'');
                    $saveData['lng'] = (!empty($lng)?$lng:'');
                //pr($lat);
                }
            }

            // adresa
            $newAddr = [
                'id'=>$saveData['client_addresses_id'],
                'street'=>$saveData['street'],
                'city'=>$saveData['city'],
                'zip'=>$saveData['zip'],
                'lat'=>$saveData['lat'],
                'lng'=>$saveData['lng'],
                'company'=>(isset($saveData['company'])?$saveData['company']:''),
            ];
            $saveData['client_addresses'][] = $newAddr;

            unset($saveData['clients']['created']);
            unset($saveData['clients']['modified']);
            //$saveData['client_addresses'] =$saveData['client_addresses'],$newAddr);

            $saveData['clients']['client_addresses'] = $saveData['client_addresses'];
            unset($saveData['client_addresses']);
            //pr($saveData);die();
            $saveData['client'] = $saveData['clients'];
            unset($saveData['clients']);
            
            // load settings
            if (!isset($this->settings)){
                $this->getSettings();
            }
            $saveData['order_items'] = $saveData['billItems'];
            foreach($saveData['order_items'] AS $k=>$i){
                $saveData['order_items'][$k]['system_id'] = $this->settings->data->system_id;
                $saveData['order_items'][$k]['product_id'] = $saveData['order_items'][$k]['id'];
                unset($saveData['order_items'][$k]['id']);
            }
            unset($saveData['billItems']);
            
           
            
            //pr($saveData['street']);die();
            
            $saveData['client']['status'] = 1;
            unset($saveData['client']['trash']);
            unset($saveData['client']['modified']);
            unset($saveData['client']['created']);

            if (empty($saveData['phone_call'])){
                $saveData['phone_call'] = (($saveData['client']['phone_pref']!='420')?$saveData['client']['phone_pref']:'').$saveData['client']['phone'];
                
            }
            
           
            // total price
            $saveData['total_price'] = $this->getTotalPrice($saveData['order_items']);
            $saveData['createdTime'] = strtotime(date('Y-m-d H:i:s'));
            $saveData['system_id'] = $this->settings->data->system_id;
            $saveData['name'] = $saveData['client']['last_name'].' '.$saveData['client']['first_name'];
            

            // doba doruceni
            switch($saveData['send_time_id']){
                case 1: $delivery_to_time = new Time(date("Y-m-d H:i:s", strtotime('+60 minutes')));break;
                case 2: $delivery_to_time = new Time(date("Y-m-d H:i:s", strtotime('+75 minutes')));break;
                case 3: $delivery_to_time = new Time(date("Y-m-d H:i:s", strtotime('+90 minutes')));break;
                case 4: $delivery_to_time = new Time(date("Y-m-d H:i:s", strtotime('+120 minutes')));break;
                
            }
            
            //pr($delivery_to_time);die();
            if (isset($delivery_to_time))
            $saveData['delivery_to_time'] = $delivery_to_time;
            //pr($this->settings->data->system_id);die();
            //pr($saveData);die();
            
            $save_entity = $this->Orders->newEntity($saveData,['associated' => ["Clients"=>['validate' => 'default','accessibleFields'=>['id' => true]], "Clients.ClientAddresses","OrderItems"]]);
            //pr($save_entity); die();
            
            $this->vI->checkErrors($save_entity);
            
            if (!$resultDb = $this->Orders->save($save_entity)){
                $results = [
                    'result'=>false,
                    'message'=>__('Chyba uložení')
                ];    
            } else {
                $this->jsonToPrinter($resultDb);
                $results = [
                    'result'=>true,
                    'message'=>__('Uloženo'),
                    'data'=>$resultDb,
                    //'print_data'=>$this->json_print_data,
                
                ];
                if (!$resultDb['close_order']){
                    $results['print_data'] = $this->json_print_data;
                }
                //pr(json_encode($results));die();
                $adrIds = [];
                if ($resultDb['client']['client_addresses']){
                    foreach($resultDb['client']['client_addresses'] AS $adr){
                        $adrIds[$adr->id] = $adr->id;
                    }
                    $this->loadModel('ClientAddresses');
                    $this->ClientAddresses->deleteAll(['id NOT IN' => $adrIds,'client_id'=>$resultDb['client']->id]);
                    //pr($adrIds);
                }
                die(json_encode($results));
            }
            
            //pr($resultDb); die();
            

            $this->setJsonResponse($results);
        
        // load data
        } else {
            $defaultValues = [
                'source_id'=>1,
                'web_id'=>'',
                'client_addresses_id'=>'',
                'client_id'=>'',
                'note'=>'',
                'area_id'=>'',
                'send_time_id'=>'',
                'pay_points'=>0,
                'close_order'=>0,
                'payment_id'=>1,
                'storno'=>0,
                'operating'=>0,
                'later'=>0,
            ];

            $this->loadModel('Products');
            $resultDb = $this->Products->updateAll(
                ['num'=>null], // fields
                ['product_group_id' => 3]
            );

            if ($id != null){
                // pokud je edit
                if ($id != 'new'){
                    $conditions = ['id'=>$id];
                    $data = $this->Orders->find()
                    ->contain('Clients')
                    ->select()
                    ->where($conditions)
                    ->first()
                ;
                // pokud je nova polozka
                } else {
                    $data = $this->vI->emptyEntity('Orders',['belongsTo'=>'Clients']); 
                    
                }
                //pr($data);die();
                $this->loadModel('ProductGroups');
                $this->productGroups = $this->ProductGroups->loadGroupProducts();
                //pr($this->productGroups);
                $data->client_addresses = '';
                $data = $this->vI->convertLoadData($data,$defaultValues);
                //pr($data);die();
                $validations = $this->vI->getValidations('Orders',['belongsTo'=>'Clients']);
            } else {

            }

            $results = [
                'result'=>true,
                'validations'=>(isset($validations)?$validations:null),
                'data'=>(isset($data)?$data:''),
                'others' => [
                    'product_groups'=>$this->productGroups,
                ],
                'select_list'=>$this->vI->SelectList($select_list),
                'select_list2'=>$select_list,
            ];  
            //pr($results);die();  
            $this->setJsonResponse($results);
        }
        $this->clearCache('orders_data');
    }

    /**
     * dohrani objednavek 
     */
    public function getUnloadOrders(){
        $conditions =  [
            'date(created)'=>date('Y-m-d'),
            'distributor_id IS'=>null
        
        ];
        //pr($conditions);
        $orders = $this->Orders->find()
        ->where(
           $conditions
        )
        ->toArray();
        //pr($orders);
        if ($orders){
            die(json_encode(['result'=>true,'data'=>$orders]));
    
        } else {
            die(json_encode(['result'=>false]));
    
        }
    }

    	
	// format price
	public function price($price,$mena_suf = array()){
		
		//pr($mena_suf);
		$symbol_before = '';
		$kurz = null;
		$count = 1;
		$decimal = 2;
		$symbol_after = '';
		extract($mena_suf);
		
    	$white = array(" " => "");
		if ($kurz!=null)
			$price = ($price/strtr($kurz,',','.'))*$count;
		if ($price != null)
			return $symbol_before.number_format(strtr($price,$white), $decimal, '.', ' ').$symbol_after;
		else
			return $symbol_before.number_format(strtr(0.00,$white), $decimal, '.', ' ').$symbol_after;	
	}


    /**
     * get total price
     */
    private function getTotalPrice($items){
        $total_price = 0;
        foreach($items AS $item){
            $total_price += $item['price']*$item['count'];
        }
        return $total_price;
    }

    /**
     * nacteni klienta dle telefoniho cisla
     */
    public function loadClient($phonePrefix,$phone,$client_id=null){
        $this->loadModel('Clients');
        $data = $this->Clients->getClient($phonePrefix,$phone,$client_id);
        if ($data){
            $results = [
                'result'=>true,
                'data'=>$data,
            ];
        } else {
            $results = [
                'result'=>false,
                'data'=>[
                    'phone_pref'=>$phonePrefix,
                    'phone'=>$phone,
                ],
            ];
        }
        
        $this->setJsonResponse($results);
    }

    /**
     * nacteni klienta dle telefoniho cisla
     */
    public function autocomplete($type,$value){
        $this->loadModel('Clients');
        $data = $this->Clients->getClientAutocomplete($type,$value);
        
        if ($data){
            $results = [
                'result'=>true,
                'type'=>$type,
                'data'=>$data,
            ];
        } else {
            $results = [
                'result'=>false,
                
            ];
        }
        
        $this->setJsonResponse($results);
    }







    public function saveUser()
    {
		$save = $this->Users->newEntity([
            'name'=>'test',
            'username'=>'test',
            'password'=>'testt',
        ]);
        pr($save);
        $result = $this->Users->save($save);
        pr($result);
        die('save user');
    }

    /**
     * get printer from cookue
     */
    private function getPrinters(){
		/*
		// get printer list from cookie
		if (isset($_COOKIE['printers_setting'])){
			$printer_list = json_decode($_COOKIE['printers_setting'],true);
			$this->printer_list = $printer_list = json_decode($printer_list['data'],true);
		}
		
		// get default printer from cookie
		if (isset($_COOKIE['printer_default'])){
			$printer_default = json_decode($_COOKIE['printer_default'],true);
			$this->printer_default = $printer_default = json_decode($printer_default['data'],true);
        }
        */
        //pr($this->settings['printer']);
    }
    
    
    /**
     * send client end to socket
     */
    public function sendClientEnd(){
        die('end');
    }
    
    /**
     * send client call to socket
     */
    public function sendClientCall(){
        die('call');
    }

    /**
     * send client hang to socket
     */
    public function sendClientHang($phoneAdd=null){
        
        if (isset($_GET["phoneNumber"]))
			$phone = $_GET["phoneNumber"];
		else 
			$phone = (isset($_POST["phoneNumber"])?$_POST["phoneNumber"]:'');
		
		$phone = trim($phone);
        if ($phoneAdd){
            $phone = $phoneAdd;
        }
        
        $this->autoRender = false;
        $this->pokladna_url = 'tcp://'.$_SERVER['SERVER_NAME'];		
		//if ($this->pokladna_url == 'p-pokladna.fastest.cz')
        //$local = 'http://'.$this->pokladna_url.'/send_client/';
        //else	
        $local = $this->pokladna_url.'/api/hang/';
        
        $setting = array(
            'host'=>$this->pokladna_url,
            'port'=>8080,
            'local'=>$local,
        );
        /*
        if ($phone != null){
            $message = $phone;
        } else {
            $message = '420731956030';
        
        }
        */
        $message = $phone;
        
        $this->loadComponent('WebsocketClient');
        $result = $this->WebsocketClient->sendCall($setting,$message,'hang');
    //die();

    }
   

    public function saveDistributor(){
        $this->autoRender = false;
        if (empty($this->request->data)){
            die(json_encode(['result'=>false,'message'=>'Nejsou data']));
        } else {
            if ($this->request->data['distributor_id'] == 0){
                $resultDb = $this->Orders->updateAll(
                    ['later'=>1], // fields
                    ['id' => $this->request->data['id']]
                );
                die(json_encode(['result'=>false,'message'=>'Přiřazeno na časovka','later'=>true]));

            } else {
                // pr($this->request->data);
                $this->loadModel('Deliverys');
                $find = $this->Deliverys->getDistributor($this->request->data['distributor_id']);
                if ($find){
                    
                    $this->request->data['distributor_id'] = $find->id;
                    $this->loadModel('Deadlines');
                    $findOrder = $this->Deadlines->getDistributorOrders($this->request->data['distributor_id'],$this->request->data['id']);
                    if (!$findOrder){
                        die(json_encode(['result'=>false,'message'=>'Nelze uložit rozvoce, již máte uloženou uzávěrku pro tento BON']));
                
                    } else {
                                
                        $save = $this->Orders->newEntity($this->request->data,['validate'=>false]);
                        //pr($save);die();
                        if (!$result = $this->Orders->save($save)){
                            die(json_encode(['result'=>false,'message'=>'Chyba uložení rozvozce']));
                        } else {
                            $this->sendCloudOrder($result->id,$this->request->data['distributor_id']);
                            die(json_encode(['result'=>true]));
                        }
                    }
                    
                } else { 
                    die(json_encode(['result'=>false,'message'=>'Nelze uložit rozvoce']));
                    
                }
            }
        }
    }

    /**
     * odeslani objedavky do cloud pro trackovani
     */
    public function sendCloudOrder($id,$distributor_id){
        $this->getSettings();
        $this->autoRender = false;
        //pr($this->settings->data->system_id);die();
		$this->settings->data->domain = strtr($this->settings->data->domain,['http'=>'https']);
        $find_order = $this->Orders->find()
            ->contain(['OrderItems'])
            ->select([])
            ->where(['id'=>$id])
            ->first()
        ;
        //pr($find_order);die();
        
        $products = array();
        
        if (!empty($find_order->order_items))
			foreach($find_order->order_items AS $pro){
				$products[] = array(
					//'shop_product_id',
					'code'=>$pro->code,
					'name'=>$pro->name,
					'price_vat_per_item'=>$pro->price,
					'count'=>$pro->count,
				);
			}
			$order_data = array(
				// stav_id
				// system_id
				// shop_client_id
				// lat
				// lng
				'order_id'=>$find_order->id,
				'doba_id'=>$find_order->send_time_id,
				'order_web_id'=>$find_order->web_id,
				'shop_provoz_id'=>$this->settings->data->system_id,
				'zdroj_id'=>$find_order->source_id,
				'price_vat'=>$find_order->total_price,
				'created_pokladna'=>$find_order->created->format('Y-m-d H:i:s' ),
				'created_time'=>strtotime($find_order->created->format('Y-m-d H:i:s')),
				'shop_payment_id'=>$find_order->payment_id,
				'note'=>$find_order->note,
				'rozvozce_id'=>$distributor_id,
				'client_name'=>$find_order->name,
				'client_telefon'=>((strlen($find_order->phone_call) == 9)?'420':'').$find_order->phone_call,
				'client_ulice'=>$find_order->street,
				'client_mesto'=>$find_order->city,
				'mobile_order_items'=>$products
            );
            //pr($order_data);die();
        $ch = curl_init();
		$post = array(
			'order_data'=>$order_data,
			'order_id'=>$find_order->id,
			'order_web_id'=>$find_order->web_id,
			'rozvozce_id'=>$distributor_id,
			'pokladna_id'=>$this->settings->data->system_id,
		);
        //pr($post);die();
        //pr(CLOUD_URL.'/rozvozce_add/');
        curl_setopt($ch, CURLOPT_URL, $this->settings->data->domain.'/rozvozce_add/');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
	
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec ($ch);
        //pr($result);
        $result = (json_decode($result,true));
        //pr($result);
        if ($result['r'] != true){
            die(json_encode(array('result'=>false,'message'=>'Chyba uložení do aplikace, zkuste uložit znovu')));
        }
        curl_close ($ch);
    }

    /**
     * get OrderList
     */
    
    public function getOrdersList(){
        $OrdersList = $this->Orders->getOrdersList();
        //pr($OrdersList);
        if ($OrdersList){
            foreach($OrdersList AS $k=>$data){
                $this->escPosSetting = ['is_logo'=>false];
                $this->jsonToPrinter($data);
                //pr($this->json_print_data);
                $OrdersList[$k]['print_data'] = $this->json_print_data;
            }
        }
        //pr($OrdersList);
        if (isset($this->request->query['debug'])){
            pr($OrdersList);die();
        }
        
        
       // pr($OrdersList);
        if ($OrdersList){
            //pr($OrdersList);
            die(json_encode(['result'=>true,'data'=>$OrdersList]));
            
        } else {
            die(json_encode(['result'=>false,'message'=>__('Nenalezeny zadne objednavky')]));
            
        }
    }
    
    function price_format($price){
        return number_format($price, 2, '.', ' ');
    }
    
    private function formatPhone($num){
        if (substr($num,0,2) == '42'){
            
            $result = substr($num,0,3)." ".substr($num,3,3)." ".substr($num,6,3)." ".substr($num,9,3);
            
        } else {
            $result = substr($num,0,3)." ".substr($num,3,3)." ".substr($num,6);
            
        }
        return $result;
    }

    /**
     * print json data
     */
    public function jsonToPrinter($data=null,$part_print=false){
            if (!isset($this->settings)){
            $this->getSettings();
            }
            $this->transfer_list = $this->Settings->operationsList(true);
            //pr($this->transfer_list);die();
            //pr(json_decode($settings));
            if (empty($this->settings)){
                die(json_encode(['result'=>false,'message'=>'Neni definovano nastaveni']));
            }
            $this->autoRender = false;
            if ($data == 'null') $data = null;
            if ($data == null){
                $showResult = true;
                $data = $this->Orders->find()
                ->contain(['OrderItems'])
                ->where(['id'=>149359])
                ->first();
            }
            //pr($data);die();
            // get printers from COOKIE
            //$this->getPrinters();
            
            
            
            //pr($_COOKIE['printers_setting']);	
            //pr($printer_default);	
            //pr($printer_list);	
            //pr($data);
            $table_data = [];
            
            $total_price = 0;
            // pr($data['order_items']);die();
            foreach($data['order_items'] AS $itemKey=>$item){
                $total_price += $item['price']*$item['count'];
                
                $pref = '';
                if($item['addon'] == 1) $pref = ' + ';
                if($item['addon'] == 2) $pref = ' - ';
                
                if ($item['addon'] > 0){
                    $item['num'] = '';
                }

                $large = strpos($item['code'],'B');
                if ($large !== false){
                    //$item['num'] = $item['num'].' V';
                    //pr($item['num']);
                }

                $productName = $pref.mb_substr($item['name'],0,18);

                //pr($item);die();
                if ($item['product_group_id'] == 1){
                    $productName = mb_substr($productName,0,15).' M';
                }
                //pr($item);die();
                if ($item['product_group_id'] == 2){
                    $productName = mb_substr($productName,0,15).' V';
                }

                $productName = strtr($productName,['ó'=>'o']);
                
                $table_data[] = [
                    'font'=>'B',
                    'format'=>'normal',
                    'data'=>[
                        'Kod'=> $item['code'], 
                        'Produkt'=> $productName, 
                        'Ks'=> $item['count'], 
                        'Cena'=>$this->price_format($item['price']),
                        //'Celkem'=>$this->price_format($item['price']*$item['count']), 
                    ],
                ];
                if (isset($data['order_items'][$itemKey+1]) && $data['order_items'][$itemKey+1]['addon'] == 0){

                
                $table_data[] = [

                    'format'=>'normal',
                    'data'=>[
                        'Kod'=> '----', 
                        'Produkt'=> '----------------', 
                        'Ks'=> '---', 
                        'Cena'=>'------',
                        //'Celkem'=>$this->price_format($item['price']*$item['count']), 
                    ],
                ];
                }
                
            }
            // $table_data[] = [
            //     'format'=>'bold',  
            //     'data'=>[
            //         'Kod'=>' ', 
            //         'Produkt'=>' ', 
            //         'Ks'=> ' ',
            //         'Cena'=> ' ',
            //     ],
            // ];
            $table_data[] = [

                'format'=>'normal',
                'data'=>[
                    'Kod'=> '----', 
                    'Produkt'=> '----------------', 
                    'Ks'=> '---', 
                    'Cena'=>'------',
                    //'Celkem'=>$this->price_format($item['price']*$item['count']), 
                ],
            ];
            $table_data[] = [
                'font'=>'B',
                'format'=>'bold',  
                'data'=>[
                    'Kod'=>'Celkem', 
                    'Produkt'=>' ', 
                    'Ks'=> ' ',
                    'Cena'=>$this->price_format($total_price),
                ],
            ];
            $path_logo_domain = 'http://localhost';
            if (strpos($_SERVER['SCRIPT_NAME'],'/src/') !== false){
                $path_logo = '/src/cake/webroot/uploaded/';
            } else {
                $path_logo = '/cake/webroot/uploaded/';
            }
            //pr($data);
            //pr($this->settings);
           
            $header = [
                // [
                //     'font'=>'A',
                //     'align'=>'center',
                //     'format'=>'',
                //     'data'=>$this->settings->data->print_top,
                // ],

                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>' '
                ],
                
                [
                    'font'=>'B',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>'Čas: '.$data['created']->format('d.m.y H:i'). '| Dovezeme do: '.(!empty($data['delivery_to_time'])?$data['delivery_to_time']->format('H:i'):''),
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>' '
                ],
                /*
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>'Dovezeme do:||'.(!empty($data['delivery_to_time'])?$data['delivery_to_time']->format('d.m.y H:i'):''),
                ],
                */
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>'Objednávka:||'.$data['id'],
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>'Jméno a příjmení:|'.$data['name'],
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>' '
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>'Adresa:',
                ],
                [
                    'font'=>'B',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>$data['street'],
                ],
                [
                    'font'=>'B',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>$data['city'],
                ],
            ];
            if (!empty($data['company'])){
                array_push($header, 
                    [
                        'font'=>'A',
                        'align'=>'left',
                        'format'=>'',
                        'data'=>(!empty($data['company'])?$data['company']:' '),
                    ]
                );
            }
            if (!empty($data['area_name'])){
                array_push($header, 
                    [
                        'font'=>'A',
                        'align'=>'left',
                        'format'=>'',
                        'data'=>' ',
                    ]
                );
                array_push($header, 
                    [
                        'font'=>'A',
                        'align'=>'left',
                        'format'=>'',
                        'data'=>(!empty($data['area_name'])?'Oblast:|'.$data['area_name']:' '),
                    ]
                );
            }
            if (!empty($data['note'])){
                array_push($header, 
                    [
                        'font'=>'A',
                        'align'=>'left',
                        'format'=>'',
                        'data'=>' ',
                    ]
                );
                array_push($header, 
                    [
                        'font'=>'B',
                        'align'=>'left',
                        'format'=>'',
                        'data'=>(!empty($data['note'])?$data['note']:' '),
                    ]
                );
            }
            
            array_push($header, 
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>' '
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>'Telefon:',
                ],
                [
                    'font'=>'B',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>$this->formatPhone($data['phone_call']),
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>' '
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>'Zdroj:||'.$this->source_list[$data['source_id']],
                ]
                
            );
               
                
            //];
            if (isset($this->transfer_list[$data['transfer_id']])){
                $header[] =  [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>(!empty($data['transfer_id'])?'Přeposláno:||'.(isset($this->transfer_list[$data['transfer_id']])?$this->transfer_list[$data['transfer_id']]:''):' '),
                ];
            }
            if ($data->web_id > 0){
                $header[] = [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>'Web ID:||'.$data->web_id,
                ];
            }

            if ($data->payment_id == 2){
                
                $header[] = [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>' ',
                ];
                $header[] = [
                    'font'=>'B',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>'Neplaťte placeno kartou',
                ];
            }
            
            $header[] = [
                'font'=>'A',
                'align'=>'left',
                'format'=>'',
                'data'=>'-----------------------------------------',
            ];

            //pr($header);die();
            $json_print_data = [
                'printer_name'=>$this->settings['printer'],
                // 'is_logo'=>(isset($this->escPosSetting['is_logo'])?$this->escPosSetting['is_logo']:true),
                'is_logo'=>true,
                'path_logo' => $path_logo_domain.$path_logo.'logo_uctenka.jpg',
                'header'=>$header,
                //'header'=>[],
                'footer'=>[
                    [
                        'align'=>'left',
                        'format'=>'normal',
                        'data'=> ' ',
                    ],
                ],
                'table_data'=>$table_data,
                'table_align'=>[
                    'Produkt'=>'left',
                    'Ks'=>'right',
                    'Cena'=>'right',	
                ],
            ];
            // parsovani hlavicka pokud je na 2 radky
            if (strpos($this->settings->data->print_top,'<br>') == true){
                $top = explode('<br>',$this->settings->data->print_top);
                $top = array_reverse($top);
                foreach($top AS $k=>$t){
                    $topArray = [
                        'font'=>'A',
                        'align'=>'center',
                        'format'=>'',
                        'data'=>$t,
                    ];
                    array_unshift($json_print_data['header'],$topArray);
                }
            } else {
                $topArray = [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>$this->settings->data->print_top,
                ];
                array_unshift($json_print_data['header'],$topArray);
            
            }

            // parsovani hlavicka pokud je na 2 radky
            if (strpos($this->settings->data->print_bottom,'<br>') == true){
                $foot = explode('<br>',$this->settings->data->print_bottom);
                foreach($foot AS $k=>$t){
                    $footArray = [
                        'font'=>'A',
                        'align'=>'center',
                        'format'=>'',
                        'data'=>$t,
                    ];
                    array_push($json_print_data['footer'],$footArray);
                }
            } else {
                $footArray = [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>$this->settings->data->print_bottom,
                ];
                array_push($json_print_data['footer'],$footArray);
            
            }
            if ($data->payment_id == 2){
                $json_print_data['footer'][] = [
                    'font'=>'B',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>' ',
                ];
                $json_print_data['footer'][] = [
                    'font'=>'B',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>'Neplaťte placeno kartou',
                ];
            }
            // eet
            /*
            array_push($json_print_data['footer'],[
                'font'=>'A',
                'align'=>'left',
                'format'=>'',
                'data'=>'FIK: ||'.$data['fik'],
            ]);
            array_push($json_print_data['footer'],[
                'font'=>'A',
                'align'=>'left',
                'format'=>'',
                'data'=>'FIK: ||'.$data['bkp'],
            ]);
            */
            //pr($json_print_data);
            if (isset($this->request->query['debug'])){
                pr($json_print_data);
                die();
            }else if (isset($this->request->query['json']) || isset($showResult)){
                //$convert = iconv("UTF-8", "ISO-8859-1", json_encode($json_print_data));
                //pr($convert);
                // die(json_encode(['result'=>true,'data'=>json_decode($convert)]));
                die(json_encode(['result'=>true,'data'=>$json_print_data]));
                die();
            } else {
                //pr($json_print_data);
                $this->json_print_data = $json_print_data;
                return $json_print_data;
                
            }
        
    }


    public function sendToCloud(){
        $this->loadModel('Deadlines');
        $deadline = $this->Deadlines->find()
            ->select(['id','order_id_from','order_id_to'])
            ->where(['type_id'=>1])
            ->order('id DESC')
            ->first();    
        ;
        if (!$deadline){
            die(json_encode($result = ['result'=>false,'message'=>'Není žádná uzávěrka']));
        }
        //pr($deadline);

        $conditions = [
            'id > '=>$deadline->order_id_from,
            'id <= '=>$deadline->order_id_to,
            'sendCloud'=>0
        ];
        $query = $this->Orders->find()
            ->contain('OrderItems')
            ->select()
            ->where($conditions) 
            ->hydrate(false)   
        ;
        $data = $query->toArray();
        if (!$data){
            
            die(json_encode($result = ['result'=>false,'message'=>'Nejsou žádné objednávky']));
        }
        //pr($conditions);die();
        //pr($data);

        $ids = [];
        foreach($data AS $k=>$d){
            $ids[] = $data[$k]['id'];

            $data[$k]['pokladna_id'] = $data[$k]['id'];
            $data[$k]['created'] = $data[$k]['created']->format('Y-m-d H:i:s');
            $data[$k]['modified'] = $data[$k]['modified']->format('Y-m-d H:i:s');
            unset($data[$k]['id']);
            
            if (isset($d['order_items'])){
                foreach($d['order_items'] AS $ok=>$oi){
                    
                    $data[$k]['order_items'][$ok]['created'] = $data[$k]['order_items'][$ok]['created']->format('Y-m-d H:i:s');
                    $data[$k]['order_items'][$ok]['modified'] = $data[$k]['order_items'][$ok]['modified']->format('Y-m-d H:i:s');
                    $data[$k]['order_items'][$ok]['pokladna_id'] = $data[$k]['order_items'][$ok]['id'];
                    $data[$k]['order_items'][$ok]['pokladna_order_id'] = $data[$k]['order_items'][$ok]['order_id'];
                    unset($data[$k]['order_items'][$ok]['id']);
                    unset($data[$k]['order_items'][$ok]['order_id']);
                }
            }
        }
        /*
        $resultDb = $this->Orders->updateAll(
			['sendCloud'=>1], // fields
			['id IN' => $ids]
        );
        */

        $this->loadModel('ProductGroups');
        $product_groups = $this->ProductGroups->groupList();
        //$data['product_groups'] = $product_groups;
        //pr($data);die();
        
        
        $ch = curl_init();
        foreach($data AS $dat){
            $post = [
                'product_groups'=>$product_groups,
                'data'=>array($dat),
                
            ];
            //pr($post);die();
            $curlUrl = API_URL.'api/orders/save/';
            curl_setopt($ch, CURLOPT_URL, $curlUrl);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec ($ch);
            if (isset($this->request->query['debug'])){
                pr($result);
            }
            //pr('curl result');
        }
        
        if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro uložení objednávek, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
		} else {
            //pr($result);
			$result = json_decode($result,true);
            if (json_encode($result) == null){
                pr($result);
            }
        }
        die(json_encode($result));
    }


    public function changeOperation($order_id,$current,$new=null){
        if ($new == null){
            die(json_encode(['result'=>false,'message'=>'Není zvolej provoz kam chcete přeposlat']));
        }
        $ch = curl_init();
        $post = [];
        //pr($post);
        // load settings
        if (!isset($this->settings)){
            $this->getSettings();
        }
        //pr($this->settings->data->domain);
        $curlUrl = $this->settings->data->domain.'/change_order/'.$order_id.'/'.$current.'/'.$new.'/';
        //pr($curlUrl);die();
        curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		//pr('curl result');
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro změnu provozu, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
		} else {
            //pr($result);
			$result = json_decode($result,true);
            if (json_encode($result) == null){
                pr($result);
            }
        }
        die(json_encode($result));
    }


    /**
     * send doba rozvozu
     */
    public function sendTime($time_id,$web_id,$later=null){
        
        $ch = curl_init();
        $post = [];
        //pr($post);
        // load settings
        if (!isset($this->settings)){
            $this->getSettings();
        }
        //pr($this->settings->data->domain);
        $curlUrl = $this->settings->data->domain.'/confirm_email/'.$time_id.'/'.$web_id.'/'.$later;
        //pr($curlUrl);die();
        curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		//pr('curl result');
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro poslání doby rozvozu, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
		} else {
            //pr($result);
			$result = json_decode($result,true);
            if (json_encode($result) == null){
                pr($result);
            }
        }
        die(json_encode($result));
    }
    

    public function sendEet($order_id=null,$jsonResult=true){
        $this->autoRender = false;
        
        if (!isset($this->settings)){
            $this->getSettings();
        }
        
        
        
        if (!$order_id){
            
            die(json_encode(['result'=>false,'message'=>'Není cislo objednavky'])); 
        }
        if (!isset($this->settings->data->dic)){
            $this->settings->data->dic = 'CZ28591232';
            $this->settings->data->certificate = 'eet.p12';
        }
        
        // unique id
        
        $order_data = $this->Orders->find()
            ->select([
                'id',
                'created',
                'total_price',
            ])
            ->where(['id'=>$order_id])
            ->first()
        ;

        if (!$order_data){
            die(json_encode(['result'=>false,'message'=>'Není nalezena objednávka'])); 
        }
        $uuid = uniqid();

        //pr($this->settings->data->system_id);
        $options = [
            'order_id'=>$order_id, // id objednavky
            'cert'=>$this->settings->data->certificate, // id provozovny
            'id_provoz'=>$this->settings->data->system_id, // id provozovny
            'id_pokl'=>$this->settings->data->system_id, // id pokladny
            'dic_popl'=>$this->settings->data->dic, // dic poplatnika
            'dic_poverujiciho'=>$this->settings->data->dic, // dic poverujiciho
            'pobocka_id'=>$this->settings->data->system_id, // cislo pobocky
            'porad_cis'=>$uuid, // poradove cislo, nebo cislo uctu ,možná nastavit UNIQUE ID
            'dat_trzby'=>$order_data->created, // datum trzby
            'celk_trzba'=>$order_data->total_price, // celkova trzba
            //'storno'=>false, // pokud je storno
        ];
    
        $this->loadComponent('Eet',$options);
        
        $result = $this->Eet->prepareData();
        if (!$result){
            die(json_encode(['result'=>false,'message'=>'Neznama chyba']));
        } else {
            if($result['result'] == true){
                $saveOrder = [
                    'bkp'=>$result['bkp'],
                    'fik'=>$result['fik'],
                    'eet_time'=>$result['eet_time'],
                ];
                if (isset($result['eet_offline'])){
                    $saveOrder['eet_offline'] = $result['eet_offline'];

                }
                //pr($saveOrder);

                $resultDb = $this->Orders->updateAll(
                    $saveOrder, // fields
                    ['id' => $order_data->id]
                );
            } else {
                die(json_encode($result));
    
            }
            if ($jsonResult){
                die(json_encode($result));
            } else {
                return $result;
            }
        }
    }


    
}
