<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;
//use Cake\Controller\Component\CookieComponent;

class SettingsController extends AppController{
    
    /**
     * vytvoreni select listu
     */
    public function getSelectList($check=true){
        if ($check == false || isset($this->request->query['firstLoad'])){
            $this->operations_list = json_decode(file_get_contents(API_URL.'api/getProvoz'));
            //pr($this->operations_list_load);die();
           
            return $select_list = [
                'operations_list'=>$this->operations_list->data,
                'no_yes'=>$this->no_yes,
            ];
        } else {
            return false;
        }  
        
        
    }

    /**
     * INDEX orders
     */
    public function index2()
    {
        $this->loadComponent('vI');

        $select_list = $this->getSelectList();
        //pr($select_list);die();
        $conditions = [];
        $fields_defined = [
            0=>['col'=>'id','title'=>'ID','type'=>'text'],
            1=>['col'=>'name','title'=>'Název','type'=>'text'],
            2=>['col'=>'order_num','title'=>'Pořadí','type'=>'text'],
            3=>['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
            //3=>['col'=>'zdroj_id','title'=>'Zdroj','type'=>'list','list_data'=>'zdroj_list'],
        ];

        $this->filtration_defined = [
            'id'=>['col'=>'id','title'=>'ID','type'=>'text'],
            'name'=>['col'=>'name','title'=>'Jméno','type'=>'text_like'],
            //'created'=>['col'=>'created','title'=>'Vytvořeno','type'=>'date_range'],
            //'modified'=>['col'=>'modified','title'=>'Upraveno','type'=>'date'],
        ];
        
        if (isset($this->request->data['conditions'])){
            //pr($this->request->data['conditions']);
            $conditions = $this->vI->convertConditions($conditions,$this->request->data['conditions'],$this->filtration_defined);
            //pr($conditions);die();
        }
        
       
        $posibility = [
            0=>['link'=>'/api/status/Settings/','title'=>__('Status'),'class'=>'fa-check-circle-o','type'=>'ajax','params'=>'status'],
            1=>['link'=>'./edit/','title'=>__('Editovat'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
            2=>['link'=>'/api/trash/Settings/','title'=>__('Smazat'),'class'=>'fa-trash','type'=>'ajax','params'=>'trash','confirm'=>__('Opravdu smazat?')],
        ];
        
        $top_actions = [
            0=>['link'=>'./edit/','title'=>__('Nová položka'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
        ];

        
        $fields = $this->vI->fieldsConvert($fields_defined);
        //pr($fields);die();
        
        $mapper = function ($data, $key, $mapReduce) {
            //$data->operating = (($data->operating == '')?0:1);
            //$data->problem = (($data->problem == '')?0:1);

            
            $mapReduce->emit($data);  
        };

        $query = $this->Settings->find()
            ->select($fields)
            ->where($conditions)
            ->cache(function ($query) {
				return 'setting_data-' . md5(serialize($query->clause('where')));
            })
            ->mapReduce($mapper)
        ;

        
        if (isset($this->request->query['firstLoad'])){
            $this->request->query['sort'] = 'order_num';
            $this->request->query['direction'] = 'ASC';
        }
        //$data = $query;
        //pr($query->toArray());die();
        
        
        $this->loadComponent('Paginator');
        $data_list = $this->paginate($query);
        
        
        
        $pagination = $this->vI->convertPagination();
        //pr($pagination['page']);
        $results = [
            'result'=>true,
            'data'=>$data_list->toArray(),
            'data_count'=>count($data_list->toArray()),
            'pagination'=>$pagination,
            'table_th'=>$fields_defined,
            'filtration'=>array_values($this->filtration_defined),
            'select_list'=>$select_list,
            'posibility'=>$posibility,
            'top_actions'=>$top_actions,
            'conditions'=>(object) $conditions,
        ];
        
        $this->setJsonResponse($results);
    }

    /**
     * editace
     */
    public function index($id=null){
        if (!$id){
            $find = $this->Settings->find()
            ->select()
            ->where(['id'=>1])
            ->first();
            if (!$find){
                $id = 'new';
            } else {
                $id = 1;
            }
            

        }
        $select_list = $this->getSelectList(false);
        $this->loadComponent('vI');
        //pr($select_list);die();
        
        // save data
        if (isset($this->request->data['saveData'])){
            $saveData = $this->request->data['saveData'];
            $saveData = $this->vI->convertTime($saveData);
            
            $saveData['data'] = json_encode($saveData['data_list']);
            
            
            $save_entity = $this->Settings->newEntity($saveData);
            
            //pr($save_entity); die();
            
            $this->vI->checkErrors($save_entity);

            if (!$resultDb = $this->Settings->save($save_entity)){
                $results = [
                    'result'=>false,
                    'message'=>__('Chyba uložení')
                ];    
            } else {
                $results = [
                    'result'=>true,
                    'message'=>__('Uloženo'),
                    'data'=>$resultDb
                ];  
                if (!empty($resultDb->logo))
                $this->base64ToJpeg($resultDb->logo);
                //setcookie('globalSettings', 'json_encode($resultDb)', time() + (86400 * 30), "/");
            }
            //pr($results);die();  
            
            

            $this->setJsonResponse($results);
        
        // load data
        } else {
            $defaultValues = [
                'nodelete'=>0,
               
            ];
            if ($id != null){
                // pokud je edit
                if ($id != 'new'){
                    $conditions = ['id'=>$id];
                    $data = $this->Settings->find()
                    ->select([
                        'id',
                        'data',
                        'name',
                        'logo',
                        'printer',
                    ])
                    ->where($conditions)
                    ->first()
                ;
                
                // pokud je nova polozka
                } else {
                   
                    $data = $this->vI->emptyEntity('Settings'); 
                    
                }
                $operations_list = json_decode(file_get_contents(API_URL.'api/getProvozAll'),true);
                $selectList2 = $operations_list['data'];
                $data = $this->vI->convertLoadData($data,$defaultValues);        
                //pr($data);die();
                
                $data['data_list'] = json_decode($data['data']);
                if (empty($data['data_list'])){
                    $data['data_list'] = [
                        'print_bottom'=>'',
                        'print_top'=>'',
                        'domain'=>'',
                    ];
                }
                //pr($data);die();
                $validations = $this->vI->getValidations('Settings');
            } else {

            }

            $results = [
                'result'=>true,
                'validations'=>(isset($validations)?$validations:null),
                'data'=>(isset($data)?$data:''),
                'select_list'=>$this->vI->SelectList($select_list),
                'select_list2'=>$selectList2,
            ];  
            $this->setJsonResponse($results);
        }
        $this->clearCache('setting_data');
    }

    function base64ToJpeg($base64_string=null, $output_file=null) {
        $output_file = './uploaded/logo_uctenka.png';
        // open the output file for writing
        $ifp = fopen( $output_file, 'wb' ); 
        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );
    
        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $data[ 1 ] ) );
        $f = fread($ifp,filesize($output_file));
        // clean up the file resource
        fclose( $ifp ); 
        //pr($output_file);
        return $output_file; 
    }
}