<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;

class DeadlinesController extends AppController
{
     /**
     * vytvoreni select listu
     */
    public function getSelectList($check=true){
        if ($check == false || isset($this->request->query['firstLoad'])){
            $this->loadModel('Deliverys');
            $delivery_list = $this->Deliverys->deliveryList();
            return $select_list = [
                'source_list'=>$this->source_list,
                'delivery_list'=>$delivery_list,
            ];
        } else {
            return false;
        }  
        
        
    }

    /**
     * INDEX orders
     */
    public function index()
    {
        $this->loadComponent('vI');
        // pr($this->request->params['type_id']);die();
        $select_list = $this->getSelectList();
        $this->type_id = $this->request->params['type_id'];

        $conditions = [
            'type_id'=>$this->request->params['type_id'],
        ];
        if ($this->type_id == 1){
            $fields_defined = [
                0=>['col'=>'id','title'=>'ID','type'=>'text'],
                1=>['col'=>'order_id_from','title'=>'Objednávky od','type'=>'text'],
                2=>['col'=>'order_id_to','title'=>'Objednávky do','type'=>'text'],
                3=>['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
                //3=>['col'=>'type_id','title'=>'Zdroj','type'=>'list','list_data'=>$select_list['source_list']],
            ];
        }
        if ($this->type_id == 2){
            $fields_defined = [
                0=>['col'=>'id','title'=>'ID','type'=>'text'],
                1=>['col'=>'order_id_from','title'=>'Objednávky od','type'=>'text'],
                2=>['col'=>'order_id_to','title'=>'Objednávky do','type'=>'text'],
                3=>['col'=>'delivery_id','title'=>'Rozvozce','type'=>'list','list_data'=>$select_list['delivery_list']],
                4=>['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
                //3=>['col'=>'type_id','title'=>'Zdroj','type'=>'list','list_data'=>$select_list['source_list']],
            ];
        }

        $this->filtration_defined = [
            'id'=>['col'=>'id','title'=>'ID','type'=>'text'],
            //'order_id_from'=>['col'=>'order_id_from','title'=>'Jméno','type'=>'text_like'],
            'created'=>['col'=>'created','title'=>'Vytvořeno','type'=>'date_range'],
            //'modified'=>['col'=>'modified','title'=>'Upraveno','type'=>'date'],
            'delivery_id'=>['col'=>'delivery_id','title'=>'Rozvozce','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['delivery_list'])],
        ];
        
        
        if (isset($this->request->data['conditions'])){
            $conditions = $this->vI->convertConditions($conditions,$this->request->data['conditions'],$this->filtration_defined);
            //pr($conditions);die();
        }
        
       
        $posibility = [
            0=>['link'=>'/api/deadlines/close/show/x/','title'=>__('Zobrazit'),'class'=>'fa-search','type'=>'ajax','params'=>'showPrint'],
            1=>['link'=>'/api/deadlines/close/print/x/','title'=>__('Tisk'),'class'=>'fa-print','type'=>'ajax','params'=>'print'],
   
            //0=>['link'=>'/api/status/Orders/','title'=>__('Status'),'class'=>'fa-check-circle-o','type'=>'ajax','params'=>'status'],
            //1=>['link'=>'./edit/','title'=>__('Editovat'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
            //2=>['link'=>'/api/trash/Orders/','title'=>__('Smazat'),'class'=>'fa-trash','type'=>'ajax','params'=>'trash','confirm'=>__('Opravdu smazat?')],
        ];
        
        $top_actions = [
            //0=>['link'=>'./edit/','title'=>__('Nová položka'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
        ];

        
        $fields = $this->vI->fieldsConvert($fields_defined);
        //pr($fields);die();
        
        $query = $this->Deadlines->find()
            ->select($fields)
            ->where($conditions)
            ->cache(function ($query) {
				return 'deadlines_data-' . md5(serialize($query->clause('where')));
			})
        ;
        //$data = $query;
        //pr($data->toArray());die();
        
        
        $this->loadComponent('Paginator');
        $data_list = $this->paginate($query);
        
        ;
        
        $pagination = $this->vI->convertPagination();
        //pr($pagination['page']);
        $results = [
            'result'=>true,
            'data'=>$data_list->toArray(),
            'data_count'=>count($data_list->toArray()),
            'pagination'=>$pagination,
            'table_th'=>$fields_defined,
            'filtration'=>array_values($this->filtration_defined),
            'select_list'=>$select_list,
            'posibility'=>$posibility,
            'top_actions'=>$top_actions,
            'conditions'=>(object) $conditions,
        ];
        
        $this->setJsonResponse($results);
    }

    private function checkOpenOrder(){
        $this->loadModel('Orders');
        $count = $this->Orders->checkOpenOrder();
        if ($count > 0){
            die(json_encode(['result'=>false,'message'=>'Nejsou přiřazeny všechny objednávky na rozvozce']));
        }
    }

    /**
     * reprint uzaverky
     */
    public function reprint($id){
        $data = $this->Deadlines->find()
        ->where(['id'=>$id])
        ->first();
        //pr($data);die();
        $print_data = $this->jsonToPrinter($data);
        /*
        $results = [
            'result'=>true,
            //'print_type'=>'show',
            'print_data'=>$print_data,
        ];
        */
        die(json_encode($results));
    }


    
    // uzavreni smeny
    public function close($print_type,$deadline_type,$deadline_id=null,$type_delivery=null,$delivery_id=null){
        
        if ($deadline_id == 'null') $deadline_id = null; 
        //pr($deadline_id);
        $this->loadModel('Deliverys');
        $this->deliveryList = $this->Deliverys->deliveryList();
     
        if ($type_delivery == 'delivery'){
            $this->type_id = 2;
            $this->delivery_id = $delivery_id;
            $conditionsDeadline = [
                'type_id'=>$this->type_id,
                'delivery_id'=>$delivery_id,
            ];

            // pr($this->deliveryList);die();
        } else {
            
            $this->type_id = 1;
            $conditionsDeadline = [
                'type_id'=>$this->type_id,
            ];
        }
        if ($deadline_id){
            unset($conditionsDeadline['type_id']);
            $conditionsDeadline['id'] = $deadline_id;
            
        }
        //pr($conditions);die();

        // kontrola otevrenych objednavek
        if ($deadline_type == 'z' && $type_delivery != 'delivery'){
            $this->checkOpenOrder();
        }

        if (!$deadline_id){
            // nalezeni posledni uzaverky
            $findLast = $this->Deadlines->find()->where($conditionsDeadline)->select(['order_id_from','order_id_to'])->order('id DESC')->first();    
            //pr($findLast);
            if (empty($findLast)){
                $lastIdFrom = 0;
            } else {
                $lastIdFrom = $findLast->order_id_to;
            } 
        } else {
            $findLast = $this->Deadlines->find()->where($conditionsDeadline)->select(['order_id_from','order_id_to','created','type_id','delivery_id','open_date','id'])->order('id DESC')->first();    
            if (!empty($findLast->delivery_id)){
                $this->type_id = $findLast->type_id;
                $this->delivery_id = $delivery_id = $findLast->delivery_id;
                
                $this->loadModel('Deliverys');
                $this->deliveryList = $this->Deliverys->deliveryList();
                
            }
            // pr($findLast);die();
            $lastIdFrom = $findLast->order_id_from;
            $lastIdTo = $findLast->order_id_to;
             
            //pr($findLast);die();
        }
        // vyhledani objednavek dle posledniho ID uzaverky
        $this->loadModel('Orders');
        if (!$deadline_id){
            $conditions = [
                'id >'=>$lastIdFrom,
            ];
        } else {
            $conditions = [
                'id >'=>$lastIdFrom,
                'id <='=>$lastIdTo,
            ];
        
        
        }
        
        

        if ($delivery_id){
            $conditions['distributor_id'] = $delivery_id;
        }
        
        //pr($conditions);die();
        $findOrders = $this->Orders->find()
        ->contain(['OrderItems'=>function($oitems){
            return $oitems->select([
                'id',
                'order_id',
                'name',
                'price',
                'count',
                'product_group_id',
                'code',
                'price_default',
                'free',
            ]);
        }])
        ->where($conditions)
        ->select([
            'Orders.id',
            'Orders.total_price',
            'Orders.source_id',
            'Orders.payment_id',
            'Orders.storno',
            'Orders.storno_type',
            'Orders.close_order',
            'Orders.distributor_id',
            'Orders.operating',
        ])
        ->order('id DESC')
        ->toArray();    
        
        // pr($findOrders);die();

        if (!$findOrders){
            die(json_encode(['result'=>false,'message'=>'Nejsou žádné objednávky pro zobrazení a tisk']));
        }
       
        
        if ($delivery_id){
        $this->open_date = '';    
        $this->close_date = '';    
        $this->loadModel('Deliverys');
        $delivery_code = $this->Deliverys->deliveryListCode($delivery_id);
            if ($delivery_code){
                if ($deadline_type == 'z'){
                    $result = $this->getDeliveryOpen($delivery_code);
                    //pr(new Time());
                    //pr($result);die();
                    if ($result->result == true){
                        //pr($result->data);
                        $this->open_date = new Time($result->data->date_from);
                        //$this->close_date = new Time($result->data->date_to);
                        // pr($this->close_date);
                        if (empty($this->close_date)){
                            $this->close_date = new Time();
                        }
                    }
                    // pr($this->open_date);
                    // pr($this->close_date);
                    // die();
                    $this->connectedDeliverys = $this->saveDeliveryClose($delivery_code);
            
                } else {
                    $this->open_date = (isset($findLast->open_date)?$findLast->open_date:'');
                    $this->close_date = (isset($findLast->created)?$findLast->created:'');
                    //pr($findLast->open_date);
                    //pr($open_date);
                    //pr($close_date);
                    //die();
                   

                }
            }
           //pr($delivery_code);die();
        
        }
        
 //pr($findOrders);die();

        // nacteni skupin
        $this->loadModel('ProductGroups');
        $group_list = $this->ProductGroups->groupList();
        //pr($group_list);die();
        $productsGroup = [];
        foreach($group_list AS $group_id=>$group_name){
            $productsGroup[$group_id] = [
                'name'=>$group_name,
                'total_price'=>0,
                'total_units'=>0,
                'items'=>[],
            ];
        }
        
        if($deadline_type == 'z'){
            $title = 'Uzávěrka Z';
        }
        if($deadline_type == 'x'){
            $title = 'Uzávěrka X';
        }
        if($deadline_type == 'z'){
            $title = 'Uzávěrka Z';
            $this->closeDeadline = true;
        }
        if ($deadline_id){
            $titleDate = $findLast->created->format('d.m.Y H:i:s');
            
        } else {
            $titleDate = date('d.m.Y H:i:s');
            
        }

        if ($deadline_id){
            $title = 'Uzávěrka Z';
            
        }
        
        // pr($group_list);die();

        $resDead = [
            'header'=>[
                'title'=>$title,
                'titleDate'=>$titleDate,
            ],
            'orders_data'=>[
                'order_id_from'=>$lastIdFrom,
                'order_id_to'=>$findOrders[0]->id,
            ],
            'sum_total_complete'=>[
                'orders'=>0,
                'total_price'=>0,
                'items'=>0,
                'units'=>0,
            ],
            'sum_transport'=>[
                'orders'=>0,
                'price'=>0,
              
            ],
            'sum_total_nocomplete'=>[
                'orders'=>0,
                'total_price'=>0,
                'items'=>0,
                'units'=>0,
            ],
            'sum_total_storno'=>[
                'orders'=>0,
                'total_price'=>0,
                'items'=>0,
                'units'=>0,
            ],
            'sum_total_storno2'=>[
                'orders'=>0,
                'total_price'=>0,
                'items'=>0,
                'units'=>0,
            ],
            'sum_total_free'=>[
                'orders'=>0,
                'total_price'=>0,
                'items'=>0,
            ],
            'payed_cards'=>[
                'orders'=>0,
                'total_price'=>0,
                'order_items'=>[],
            ],
            'cupon'=>[
                'orders'=>0,
                'total_price'=>0,
                'order_items'=>[],
            ],
            'spropitne'=>[
                'orders'=>0,
                'total_price'=>0,
                'order_items'=>[],
            ],
            'total_price_products'=>[
                'products_count'=>0,
                'total_price'=>0,
            ],
            'products_list' => $productsGroup,
        ];
        
        $this->resultDeadline = [
            0=>$resDead,
            1=>$resDead,
            
        ];
        $this->resultDeadline[1]['header']['title'] = 'Provozní';
//pr($delivery_id);
        // pokud je rozvozce pridat do tisk
        if (isset($this->deliveryList[$delivery_id])){
            
            $this->resultDeadline[0]['header']['delivery_name'] = $this->deliveryList[$delivery_id];
        }

        $bonList = [
            'total'=>0,
            'items'=>[],
        ];

        $distributor_sum_list = [];

        //pr($resultDeadline);
        if($findOrders){
            foreach($findOrders AS $order){
                //pr($order->id);
                // pokud je nedokoncena
                if ($order->close_order == 1){
                    $res = $this->getOrderData('sum_total_nocomplete',$order);
                
                // pokud je dokoncena
                } else {
                    // pokud je storno upeceno
                    if ($order->storno > 0){
                        
                        if ($order->storno_type == 1){
                            $res = $this->getOrderData('sum_total_storno',$order);
                        
                        // pokud je storno neupecen
                        } else if ($order->storno_type == 2){
                            $res = $this->getOrderData('sum_total_storno2',$order);
                        }
                    // pokud je normalni objednavka    
                    } else {
                        $res = $this->getOrderData('sum_total_complete',$order);
                        $bonList['total'] += (($order->total_price>0)?$order->total_price:0);
                        $bonList['items'][] = [
                            'id'=>$order->id,
                            'price'=>$order->total_price,
                        ];

                        if (!isset($distributor_sum_list[$order->distributor_id])){
                            $distributor_sum_list[$order->distributor_id] = [
                                'name'=>(isset($this->deliveryList[$order->distributor_id])?$this->deliveryList[$order->distributor_id]:'Nezjištěno'),
                                'total'=>0,
                                'items'=>[]

                            ];
                        }
                        if ($order->storno == 0 && $order->operating == 0){
                           
                            $distributor_sum_list[$order->distributor_id]['total'] += (($order->total_price>0)?$order->total_price:0);
                            $distributor_sum_list[$order->distributor_id]['items'][] = $order->id;
                        }  
                        if ($order->distributor_id == 57){
                            // pr($distributor_sum_list[57]);
                            //pr($order);
                        }          
                    }

                
            }
            }
        }

        sort($distributor_sum_list);
       // pr($distributor_sum_list);die();
        $this->resultDeadline[0]['bon_list'] = $bonList;
        $this->resultDeadline[0]['distributor_sum_list'] = $distributor_sum_list;
        
        $print_data = $this->printData($this->resultDeadline[0],$print_type,$delivery_id);
        
        $print_data2 = $this->printData($this->resultDeadline[1],$print_type,$delivery_id);
        //pr($this->resultDeadline[1]);die();
        // ulozeni uzaverky Z
        $this->saveDeadline();

        //pr($print_data);
        //pr($this->resultDeadline);
        //die();

            $results = [
                'result'=>true,
                'message'=>__('Uzávěrka vygenerována'),
                'print_type'=>$print_type,
                'deadline_type'=>$deadline_type,
                'type_delivery'=>$type_delivery,
                'connectedDeliverys'=>(isset($this->connectedDeliverys)?$this->connectedDeliverys:false),
                'print_data'=>$print_data,
                'print_data2'=>(($this->resultDeadline[1]['sum_total_complete']['orders']>0)?$print_data2:false),
            ];
        
        $this->setJsonResponse($results);
        //$this->model->tra
    
    }

    private function getDeliveryOpen($code){
        $ctx = stream_context_create(array(
            'http'=>
                array(
                    'timeout' => 10,  //1200 Seconds is 20 Minutes
                ),
            'https'=>
                array(
                    'timeout' => 10,  //1200 Seconds is 20 Minutes
                )
            )
        );
        //pr(getcwd());
        //$this->Deliverys->saveAttendaces($code);
        //pr($_SERVER);
        if (!isset($this->settings)){
            $this->getSettings();
        }
        $result=file_get_contents(API_URL.'/api/attendances/getAttendance/'.$this->settings->data->system_id.'/'.$code, false, $ctx);
        $result = json_decode($result);
        return $result;
        //pr($result);
    }

    private function saveDeliveryClose($code){
                $ctx = stream_context_create(array(
                    'http'=>
                        array(
                            'timeout' => 10,  //1200 Seconds is 20 Minutes
                        ),
                    'https'=>
                        array(
                            'timeout' => 10,  //1200 Seconds is 20 Minutes
                        )
                    )
                );
                //pr(getcwd());
                //$this->Deliverys->saveAttendaces($code);
                //pr($_SERVER);
                $result=file_get_contents('http://'.$_SERVER['HTTP_HOST'].'/api/attendances/save/'.$code, false, $ctx);
                $result = json_decode($result);
                if (isset($result->connectedDeliverys) && !empty($result->connectedDeliverys)){
                    $return = $result->connectedDeliverys;
                } else {
                    $return = false;
                }
                //pr('a');
                //pr($result);
                //die();
        
                
        return $return;        
                
    }

    private function saveDeadline(){
        // load settings
        if (!isset($this->settings)){
            $this->getSettings();
        }
        if (isset($this->closeDeadline)){
            $save = $this->Deadlines->newEntity([
                'order_id_from'=>$this->resultDeadline[0]['orders_data']['order_id_from'],
                'order_id_to'=>$this->resultDeadline[0]['orders_data']['order_id_to'],
                'createdTime'=>strtotime(date('Y-m-d H:i:s')),
                'type_id'=>$this->type_id,
                'delivery_id'=>$this->delivery_id,
                'system_id' => $this->settings->data->system_id,
                'open_date' => $this->open_date,
                'close_date' => $this->close_date,
            ]);
            // pr($save);die();
            if (!$result = $this->Deadlines->save($save)){
                die(json_encode(['result'=>false,'message'=>'Chyba uložení uzávěrky']));
            }
        }
        //pr($result);die();
        
    }

    private function printData($data,$print_type,$delivery_id){
        $table_data = [];
       // pr($data);die();
        // total price
        $table_data[] = ['format'=>'bold','data'=>['Produkt'=>'Tržba celkem','Ks'=> ' ','Cena'=>' ']];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'------------------------','Ks'=> ' ','Cena'=>' ']];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'Celkem','Ks'=> ' ','Cena'=>$this->price_format($data['sum_total_complete']['total_price'])]];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'Objednávek','Ks'=> ' ','Cena'=>$data['sum_total_complete']['orders']]];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'Položek', 'Ks'=> ' ','Cena'=>$data['sum_total_complete']['items']]];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>' ','Ks'=> ' ','Cena'=>' ']];
        
        // sum_total_nocomplete
        $table_data[] = ['format'=>'bold','data'=>['Produkt'=>'Nedokončené','Ks'=> ' ','Cena'=>' ']];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'------------------------','Ks'=> ' ','Cena'=>' ']];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'Celkem','Ks'=> ' ','Cena'=>$this->price_format($data['sum_total_nocomplete']['total_price'])]];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'Objednávek','Ks'=> ' ','Cena'=>$data['sum_total_nocomplete']['orders']]];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'Položek', 'Ks'=> ' ','Cena'=>$data['sum_total_nocomplete']['items']]];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>' ','Ks'=> ' ','Cena'=>' ']];
        
        // sum_total_storno
        $table_data[] = ['format'=>'bold','data'=>['Produkt'=>'Storna upečeno','Ks'=> ' ','Cena'=>' ']];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'------------------------','Ks'=> ' ','Cena'=>' ']];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'Celkem','Ks'=> ' ','Cena'=>$this->price_format($data['sum_total_storno']['total_price'])]];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'Objednávek','Ks'=> ' ','Cena'=>$data['sum_total_storno']['orders']]];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'Položek', 'Ks'=> ' ','Cena'=>$data['sum_total_storno']['items']]];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>' ','Ks'=> ' ','Cena'=>' ']];
        
        
        // sum_total_storno neupeceno
        $table_data[] = ['format'=>'bold','data'=>['Produkt'=>'Storna neupečeno','Ks'=> ' ','Cena'=>' ']];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'------------------------','Ks'=> ' ','Cena'=>' ']];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'Celkem','Ks'=> ' ','Cena'=>$this->price_format($data['sum_total_storno2']['total_price'])]];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'Objednávek','Ks'=> ' ','Cena'=>$data['sum_total_storno2']['orders']]];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'Položek', 'Ks'=> ' ','Cena'=>$data['sum_total_storno2']['items']]];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>' ','Ks'=> ' ','Cena'=>' ']];
        
        // sum_total_free
        $table_data[] = ['format'=>'bold','data'=>['Produkt'=>'Zdarma','Ks'=> ' ','Cena'=>' ']];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'------------------------','Ks'=> ' ','Cena'=>' ']];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'Celkem','Ks'=> ' ','Cena'=>$this->price_format($data['sum_total_free']['total_price'])]];
        //$table_data[] = ['format'=>'normal','data'=>['Produkt'=>'Objednávek','Ks'=> ' ','Cena'=>$data['sum_total_free']['orders']]];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'Položek', 'Ks'=> ' ','Cena'=>$data['sum_total_free']['items']]];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>' ','Ks'=> ' ','Cena'=>' ']];
        //pr($data);die();


        // prumer
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'------------------------','Ks'=> ' ','Cena'=>' ']];
        $table_data[] = ['format'=>'normal','data'=>['Produkt'=>'Průměr na objednávku', 'Ks'=> ' ','Cena'=>($data['sum_total_complete']['orders']>0)?round($data['sum_total_complete']['total_price'] / $data['sum_total_complete']['orders'],2):0]];
        //pr($data['sum_total_complete']['orders']);
        //pr($table_data);die();
        $table_data[] = [
            'format'=>'normal',  
            'data'=>[
                'Produkt'=>'  ', 
                'Ks'=> ' ',
                'Cena'=>' ',
            ],
        ];

        //bony rozvozce
        //pr($this->open_date);die();
        if ($delivery_id > 0 ){
            if (!empty($this->open_date)){
                // pr($this->open_date);
                // pr($this->close_date);
                // die('a');
                $table_data[] = [
                    'format'=>'normal',  
                    'data'=>[
                        'Produkt'=>'  ', 
                        'Ks'=> ' ',
                        'Cena'=>' ',
                    ],
                ];
                $table_data[] = [
                    'format'=>'bold',  
                    'data'=>[
                        'Produkt'=>'Přihlášení/odhlášení', 
                        'Ks'=> ' ',
                        'Cena'=>' ',
                    ],
                ];
                $table_data[] = [
                    'format'=>'normal',  
                    'data'=>[
                        'Produkt'=>$this->open_date->format('d.m.Y H:i:s'), 
                        'Ks'=> ' ',
                        'Cena'=>' ',
                    ],
                ];
                $table_data[] = [
                    'format'=>'normal',  
                    'data'=>[
                        'Produkt'=>$this->close_date->format('d.m.Y H:i:s'), 
                        'Ks'=> ' ',
                        'Cena'=>' ',
                    ],
                ];
                //die('a');
                

            }


            //pr($data['sum_total_storno']);die();

            if (isset($data['bon_list']['items']) && !empty($data['bon_list']['items'])){
                $table_data[] = [
                    'format'=>'normal',  
                    'data'=>[
                        'Produkt'=>'  ', 
                        'Ks'=> ' ',
                        'Cena'=>' ',
                    ],
                ];
                
                $table_data[] = [
                    'format'=>'bold',  
                    'data'=>[
                        'Produkt'=>'Bony rozvozce', 
                        'Ks'=> 'Bon',
                        'Cena'=>'Cena',
                    ],
                ];
                foreach($data['bon_list']['items'] AS $oi){
                    $table_data[] = [
                        'format'=>'normal',  
                        'data'=>[
                            'Produkt'=>$oi['id'], 
                            'Ks'=> $oi['price'],
                            'Cena'=>' ',
                        ],
                    ];
                    
                    //pr($oi);die();
                }
                $table_data[] = [
                    'format'=>'normal',  
                    'data'=>[
                        'Produkt'=>'Celkem', 
                        'Ks'=> '',
                        'Cena'=>$this->price_format($data['bon_list']['total']),
                    ],
                ];
                    
            }
        } else {
            
            if (isset($data['distributor_sum_list']) && !empty($data['distributor_sum_list'])){
                $table_data[] = [
                    'format'=>'bold',  
                    'data'=>[
                        'Produkt'=>' ', 
                        'Ks'=> ' ',
                        'Cena'=>' ',
                    ],
                ];
                
                $table_data[] = [
                    'format'=>'bold',  
                    'data'=>[
                        'Produkt'=>'Seznam rozvozců', 
                        'Ks'=> ' ',
                        'Cena'=>'Cena',
                    ],
                ];
                // separator line
                $table_data[] = [
                    'format'=>'normal',  
                    'data'=>[
                        'Produkt'=>'--------------', 
                        'Ks'=> '----',
                        'Cena'=>'-------',
                    ],
                ];
                $delivery_total = 0;
                foreach($data['distributor_sum_list'] AS $oi){
                    
                    $table_data[] = [
                        'format'=>'normal',  
                        'data'=>[
                            'Produkt'=>strtr($oi['name'],['Ń'=>'N']), 
                            'Ks'=> $oi['total'],
                            'Cena'=>' ',
                        ],
                    ];
                    $delivery_total += $oi['total'];
                    
                    //pr($oi);die();
                }
                $table_data[] = [
                    'format'=>'normal',  
                    'data'=>[
                        'Produkt'=>'Celkem', 
                        'Ks'=> '',
                        'Cena'=>$this->price_format($delivery_total),
                    ],
                ];
                    
            }
        }


        // platba kartou
        if (isset($data['payed_cards']) && !empty($data['payed_cards']['order_items'])){
            // new line
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>' ', 
                    'Ks'=> ' ',
                    'Cena'=>' ',
                ],
            ];
            
            $table_data[] = [
                'format'=>'bold',  
                'data'=>[
                    'Produkt'=>'Platba kartou', 
                    'Ks'=> 'Bon',
                    'Cena'=>'Cena',
                ],
            ];
            // separator line
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>'--------------', 
                    'Ks'=> '----',
                    'Cena'=>'-------',
                ],
            ];
            
            foreach($data['payed_cards']['order_items'] AS $oi){
                $table_data[] = [
                    'format'=>'normal',  
                    'data'=>[
                        'Produkt'=>$oi['source_name'], 
                        'Ks'=> $oi['id'],
                        'Cena'=>$this->price_format($oi['total_price']),
                    ],
                ];
                
                //pr($oi);die();
            }
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>'Celkem', 
                    'Ks'=> ' ',
                    'Cena'=>$this->price_format($data['payed_cards']['total_price']),
                ],
            ];
           
        }

        // storna
        if (isset($data['sum_total_storno']) && !empty($data['sum_total_storno']['order_items'])){
            // new line
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>' ', 
                    'Ks'=> ' ',
                    'Cena'=>' ',
                ],
            ];
            
            $table_data[] = [
                'format'=>'bold',  
                'data'=>[
                    'Produkt'=>'Storno', 
                    'Ks'=> 'Bon',
                    'Cena'=>'Cena',
                ],
            ];
            // separator line
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>'--------------', 
                    'Ks'=> '----',
                    'Cena'=>'-------',
                ],
            ];
            
            foreach($data['sum_total_storno']['order_items'] AS $oi){
                $table_data[] = [
                    'format'=>'normal',  
                    'data'=>[
                        'Produkt'=>$oi['source_name'], 
                        'Ks'=> $oi['id'],
                        'Cena'=>$this->price_format($oi['total_price']),
                    ],
                ];
                
                //pr($oi);die();
            }
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>'Celkem', 
                    'Ks'=> ' ',
                    'Cena'=>$this->price_format($data['sum_total_storno']['total_price']),
                ],
            ];
           
        }

        // kupony
        //pr($data['cupon']);die();
        if (isset($data['cupon']) && !empty($data['cupon']['order_items'])){
            // new line
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>' ', 
                    'Ks'=> ' ',
                    'Cena'=>' ',
                ],
            ];
            
            $table_data[] = [
                'format'=>'bold',  
                'data'=>[
                    'Produkt'=>'Slevové kupony a kredity', 
                    'Ks'=> 'Ks',
                    'Cena'=>'Cena',
                ],
            ];
            // separator line
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>'--------------', 
                    'Ks'=> '----',
                    'Cena'=>'-------',
                ],
            ];
            foreach($data['cupon']['order_items'] AS $oi){
                $table_data[] = [
                    'format'=>'normal',  
                    'data'=>[
                        'Produkt'=>$oi['id'], 
                        'Ks'=> ' ',
                        'Cena'=>$this->price_format($oi['price']),
                    ],
                ];
                
                //pr($oi);die();
            }
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>'Celkem', 
                    'Ks'=> $data['cupon']['orders'],
                    'Cena'=>$this->price_format($data['cupon']['total_price']),
                ],
            ];
            //pr($data['cupon']);die();
        }
        

        // spropitne
        if (isset($data['spropitne']) && !empty($data['spropitne']['order_items'])){
            //die('a');
            // new line
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>' ', 
                    'Ks'=> ' ',
                    'Cena'=>' ',
                ],
            ];
            
            $table_data[] = [
                'format'=>'bold',  
                'data'=>[
                    'Produkt'=>'Spropitné', 
                    'Ks'=> 'Ks',
                    'Cena'=>'Cena',
                ],
            ];
            // separator line
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>'--------------', 
                    'Ks'=> '----',
                    'Cena'=>'-------',
                ],
            ];
            
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>'Celkem', 
                    'Ks'=> $data['spropitne']['orders'],
                    'Cena'=>$this->price_format($data['spropitne']['total_price']),
                ],
            ];
        }
        
        if ($data['sum_transport']['orders'] > 0){
            //$table_data[] = ['format'=>'normal','data'=>['Produkt'=>'------------------------','Ks'=> ' ','Cena'=>' ']];    
            // new line
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>' ', 
                    'Ks'=> ' ',
                    'Cena'=>' ',
                ],
            ];
            
            $table_data[] = [
                'format'=>'bold',  
                'data'=>[
                    'Produkt'=>'Počet dopravy', 
                    'Ks'=> ' ',
                    'Cena'=>'Cena',
                ],
            ];
            // separator line
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>'--------------', 
                    'Ks'=> '----',
                    'Cena'=>'-------',
                ],
            ];
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>$data['sum_transport']['orders'].'x', 
                    'Ks'=> ' ',
                    'Cena'=>$this->price_format($data['sum_transport']['price']),
                ],
            ];
            
            //pr($oi);die();
        }
        
        
        // produkty
        if (isset($data['products_list']))
        foreach($data['products_list'] AS $prodGroup){
            // new line
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>' ', 
                    'Ks'=> ' ',
                    'Cena'=>' ',
                ],
            ];
            $table_data[] = [
                'format'=>'bold',  
                'data'=>[
                    'Produkt'=>$prodGroup['name'], 
                    'Ks'=> $prodGroup['total_units'],
                    'Cena'=>$this->price_format($prodGroup['total_price']),
                ],
            ];

            // separator line
            $table_data[] = [
                'format'=>'normal',  
                'data'=>[
                    'Produkt'=>'--------------', 
                    'Ks'=> '----',
                    'Cena'=>'-------',
                ],
            ];
            //pr($prodGroup);die();
            foreach($prodGroup['items'] AS $items){
                //pr($print_type);die('a');
                if ($print_type != 'show'){
                    //pr($print_type);die('aa');
                } else {
                $table_data[] = [
                    'format'=>'normal',  
                    'data'=>[
                        'Produkt'=>$items['name'], 
                        'Ks'=> $items['units'],
                        'Cena'=>$this->price_format($items['price']),
                    ],
                ];
                }
                
            }
            
            //pr($table_data);die();
            //pr($prodGroup);die();
        }

      
        $this->getSettings();
        //pr($this->settings);die();
        //pr($data);die();    
        // foreach($table_data AS $k=>$d){
        //     if ($k > 4){
        //         unset($table_data[$k]);
        //     }
        // }
        
        //pr($table_data);die();

        $json_print_data = [
            'printer_name'=>$this->settings['printer'],
            'is_logo'=>false,
            'deadline'=>true,
            'header'=>[
                
                [
                    'font'=>'B',
                    'align'=>'center',
                    'format'=>'bold',
                    'data'=>$data['header']['title'],
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>$data['header']['titleDate'],
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>$this->settings['name'],
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>' ',
                ],
                
                
            ],
            // 'footer'=>[
            //     [
            //         'align'=>'left',
            //         'format'=>'normal',
            //         'data'=>$this->settings->data->print_bottom,
            //     ],
            // ],
            'table_data'=>$table_data,
            'table_align'=>[
                'Produkt'=>'left',
                'Cena'=>'right',
                'Ks'=>'left',	
            ],
        ];
        if (isset($data['header']['delivery_name'])){
            $json_print_data['header'][] = [
                'font'=>'B',
                'align'=>'center',
                'format'=>'bold',
                'data'=>'Rozvozce: '.$data['header']['delivery_name']
            ];
            $json_print_data['header'][] = [
                'font'=>'A',
                'align'=>'left',
                'format'=>'',
                'data'=>' ',
            ];
        }
        return $json_print_data;
    }


    function price_format($price){
        return number_format($price, 2, '.', ' ');
    }

    private function getOrderData($type,$order){
        $units = 0;
        $this->plusFreeOrder = false;
        foreach($order->order_items AS $oi){
            
            $units += $oi->count;
            if ($order->operating != 1) $order->operating = 0;
            $deadlineType = $order->operating;
            //pr($deadlineType);
            //pr($order);die();
            // pokud je doprava pripocti
            if ($oi->code =='dop'){
                //die('a');
                $this->resultDeadline[$deadlineType]['sum_transport']['price'] += $oi->price;
                $this->resultDeadline[$deadlineType]['sum_transport']['orders'] += 1;
            }

            //pr($oi->product_group_id);
            //if ($oi->product_group_id == 1000)
            if ($order->close_order != 1)
            if (isset($this->resultDeadline[$deadlineType]['products_list'][$oi->product_group_id])){
                if ($oi->code == 'dop'){
                    continue;
                } 
                if ($oi->code == 'k'){
                    $this->resultDeadline[$deadlineType]['cupon']['total_price'] += $oi->price;
                    $this->resultDeadline[$deadlineType]['cupon']['orders'] += 1;
                    $this->resultDeadline[$deadlineType]['cupon']['order_items'][] = ['id'=>$order->id,'price'=>$oi->price];
                    // cupon
                    continue;
                    //die('a');
                    
                }
                
                if ($oi->code == 's'){
              //      die('a');
                    $this->resultDeadline[$deadlineType]['spropitne']['total_price'] += $oi->price;
                    $this->resultDeadline[$deadlineType]['spropitne']['orders'] += 1;
                    $this->resultDeadline[$deadlineType]['spropitne']['order_items'][] = $order->id;
                    // cupon
                    continue;
                    //die('a');
                    
                }
                
                $this->resultDeadline[$deadlineType]['products_list'][$oi->product_group_id]['total_price'] += $oi->price;
                $this->resultDeadline[$deadlineType]['products_list'][$oi->product_group_id]['total_units'] += $oi->count;
                //pr($oi);die();
                
                //sum_total_free
                if (isset($oi->free) && $oi->free == 1){
                    $this->plusFreeOrder = true;
                    $this->resultDeadline[$deadlineType]['sum_total_free']['total_price'] += $oi->price_default;
                    $this->resultDeadline[$deadlineType]['sum_total_free']['items'] += 1;
                    //pr($oi);
                    //die('a');
                }
                
                if (!isset($this->resultDeadline[$deadlineType]['products_list'][$oi->product_group_id]['items'][$oi->code])){
                    $this->resultDeadline[$deadlineType]['products_list'][$oi->product_group_id]['items'][$oi->code] = [
                        'name'=>mb_substr(strtr($oi->name,['ó'=>'o','Ž'=>'Z']),0,22),
                        'price'=>$oi->price,
                        'units'=>$oi->count,
                    ];
                } else {
                    $this->resultDeadline[$deadlineType]['products_list'][$oi->product_group_id]['items'][$oi->code]['price'] += $oi->price;
                    $this->resultDeadline[$deadlineType]['products_list'][$oi->product_group_id]['items'][$oi->code]['units'] += $oi->count;
                }

                $this->resultDeadline[$deadlineType]['total_price_products']['total_price'] += $oi->price;
                $this->resultDeadline[$deadlineType]['total_price_products']['products_count'] += $oi->count;
            }
            //pr($this->plusFreeOrder);
            
        
        }
        // pr($this->resultDeadline[$deadlineType]['products_list']);die();
        if (isset($this->plusFreeOrder)){
            //$this->resultDeadline['sum_total_free']['orders'] ++;
        }
        if (!isset($deadlineType)){
            $deadlineType = 0;
        }
        
        if ($order->storno == 1){
            $this->resultDeadline[$deadlineType]['sum_total_storno']['order_items'][] = [
                'id'=>$order->id,
                'source_id'=>$order->source_id,
                'source_name'=>$this->source_list[$order->source_id],
                'total_price'=>$order->total_price,
            ];
            //pr($this->resultDeadline);die();
        }
        
        // platba kartou
        if ($order->payment_id == 2){
            $this->resultDeadline[$deadlineType]['payed_cards']['orders'] += 1;
            $this->resultDeadline[$deadlineType]['payed_cards']['total_price'] += $order->total_price;
            $this->resultDeadline[$deadlineType]['payed_cards']['order_items'][] = [
                'id'=>$order->id,
                'source_id'=>$order->source_id,
                'source_name'=>$this->source_list[$order->source_id],
                'total_price'=>$order->total_price,
            ];
        }
        //pr($order->operating);
        
        //pr($this->resultDeadline);die();
        if ($this->resultDeadline[$deadlineType][$type]){
            if ($type == 'asum_total_complete'){
                pr($this->resultDeadline[$deadlineType][$type]);    
                pr('id'.$order->id);    
                pr('a'.count($order->order_items));    
            }
            if (empty($order->total_price)){
                $order->total_price = 0;
                foreach($order->order_items AS $oi){
                    $order->total_price += $oi->price;
                }
                
                //die();
            }
            $this->resultDeadline[$deadlineType][$type]['orders'] += 1;
            $this->resultDeadline[$deadlineType][$type]['total_price'] += $order->total_price;
            $this->resultDeadline[$deadlineType][$type]['items'] += count($order->order_items);
            $this->resultDeadline[$deadlineType][$type]['units'] += $units;
        }
        // pr($order);
        //pr($this->resultDeadline);die();
        // pr($order);die();
    }

    public function sendToCloud(){
        $data = $this->Deadlines->find()
            ->select([])
            ->where(['type_id'=>1,'sended'=>0])
            ->order('id DESC')
            ->hydrate(false)
            ->toArray();    
        ;
        //pr(count($data));
        if (!$data){
            die(json_encode($result = ['result'=>false,'message'=>'Není žádná uzávěrka']));
        }
        //pr($data);
        $this->sendedIds = [];
        foreach($data AS $k=>$d){
            
            $data[$k]['pokladna_id'] = $data[$k]['id'];
            $data[$k]['created'] = $data[$k]['created']->format('Y-m-d H:i:s');
            $data[$k]['modified'] = $data[$k]['modified']->format('Y-m-d H:i:s');
            $this->sendedIds[] = $data[$k]['id']; 
            unset($data[$k]['id']);
            
        }
        

        
        $ch = curl_init();
        $post = $data;
        //pr($post);die();
		$curlUrl = API_URL.'api/deadlines/save/';
        //pr($curlUrl);
        curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		//curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
        //pr($result);die();
        //pr('curl result');
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro uložení objednávek, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
		} else {
           // pr($result);
			$result = json_decode($result,true);
            
            if (json_encode($result) == null){
                pr($result);
            } else {
                
                $this->Deadlines->updateAll(
                    ['sended' => 1,
                    ], // fields
                    ['id IN' => $this->sendedIds]
                );
                
                
                //pr($this->sendedIds);
                
            }
        }
        die(json_encode($result));
    }

    
    
}
