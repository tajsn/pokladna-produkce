<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

class AttendancesController extends AppController{
    
    

    /**
     * ulozeni dochazky
     */
    public function save($code=null){
        $this->autoRender = false;
        //pr($code);die();
        // $this->request->data['code'] = '001';
        // $this->request->data['type'] = 'enter';
        // $this->request->data['system_id'] = 1;
        //pr($this->request->data);die();
        if ($code){
            if (!isset($this->settings)){
                $this->getSettings();
            }
            //pr($this->settings['data']->system_id);die();
            $this->request->data['code'] = $code;
            $this->request->data['type'] = 'exit';
            $this->request->data['system_id'] = $this->settings['data']->system_id;
            $this->directClose = true;
        }
        $con = [
            'code'=>$this->request->data['code'],
        ];
        //pr($con);
        $this->loadModel('Deliverys');
        $find = $this->Deliverys->find()
        ->select(['id','name','code'])
        ->where($con)
        ->first();
        if (!$find){
            $results = [
                'result'=>false,
                'message'=>__('Kód rozvozce nenalezen'),
            ];
            die(json_encode($results));  
    
        } else {
           // pr($find);
            $post = [
                'type'=>$this->request->data['type'],
                'name'=>$find->name,
                'code'=>$find->code,
                'delivery_id'=>$find->id,
                'system_id'=>$this->request->data['system_id'],
            ];
            //pr($post);
            $result = $this->sendApi($post);
            return $result;
        }
    }

    private function sendApi($post){
        // $post = [
		// 	'type'=>'exit',
		// 	//'type'=>'enter',
		// 	'name'=>'Kuba',
		// 	'code'=>'001',
		// 	'system_id'=>1,
        // ];
        //pr(API_URL);die();
        $ch = curl_init();
		$curlUrl = API_URL.'api/attendances/save/';
		curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
        //pr($result);
        //pr('curl result');
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro uložení docházky, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
		} else {
		
			$result = json_decode($result,true);
        }
        //pr($result);
        //if (!$this->directClose){
            die(json_encode($result));  
        /*
        } else {
            return $result['connectedDeliverys'];
        }
        */
        
        //pr($result);
		curl_close ($ch);
		
    }
}