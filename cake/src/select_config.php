<?php
$select_config = [
    'close_order_list'=>[1=>'Nedokončeno',0=>'Dokončeno'],
    'yes_no'=>[1=>'Ano',0=>'Ne'],
    'no_yes'=>[0=>'Ne',1=>'Ano'],
    'storno_type_list'=>[1=>'Upečeno',2=>'Neupečeno'],
    'source_list'=>[1=>'Pokladna',2=>'Web',3=>'DJ',4=>'mPizza'],
    'source_list_convert'=>[
        1=>2, //'Web',
        2=>4, //'mPizza',
        3=>3, //'Dáme jídlo',
    ],
    
    'payment_list'=>[
        2=>'Gopay',
        3=>'Body',
        1=>'Hotově',
    ],
    'user_group_list'=>[1=>'Administrátor'],
];
?>
