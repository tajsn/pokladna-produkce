
var iconv = require('iconv-lite'),
	cmds = require('./commands');
var Image = require('./image');


/**
 * Creates a new Printjob object that can process escpos commands
 * which can then be printed with an attached USB escpos printer.
 *
 * @class
 */
var Printjob = function () {

	this._queue = [];

}



Printjob.prototype = {

	/**
	  * Add a string of text to the printjob.
	  *
	  * @param {string} text Text string to be added to the current printjob.
	  */
	encode: function () {
		
		var buf = new Buffer([0x1b, 0x52, 0x0C]);
		/**/
		//this._queue.push( iconv.encode(text, 'cp437') );
		//this._queue.push(buf);
		//this._queue.push( text );

		return this;

	},
	
	fontSize: function () {
		
		var buf = new Buffer([0x1b, 0x4d, 0x01]);
		
		var buf = new Buffer([0x1d, 0x21, 1,0]);
		//console.log(buf);
		/**/
		//this._queue.push( iconv.encode(text, 'cp437') );
		this._queue.push(buf);
		//this._queue.push( text );

		return this;

	},
	
	text: function (text,width,type) {
		// console.log('TEST',text);
		/** PRICE FORMAT */
		function priceFormat(price){
				price = price.toFloat();
				return price
				.toFixed(2) // always two decimal digits
				.replace(".", ",") // replace decimal point character with ,
				.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + ",-" // use . as a separator
			
			//return price;
		}
		
		if (width && width > 0 && text.length){
			if (type == 'price'){
				text = priceFormat(text);
			
			}
			//console.log(width);
			//console.log(text.length);
			//console.log(text);
			rozdil = width - text.length;
			//console.log('r',rozdil);
			space = '';
			if (rozdil > 0){
				for (i = 1; i <= rozdil; i++){
					space += ' ';
				}
				text += space + '  ';
			} else {
				text = text.substr(0,text.length + rozdil)+'  ';
			}
		} else {
			if (type == 'price'){
				text = priceFormat(text);
			}
		}
		/**/
		//console.log(text);
		//buff = Buffer.from(text);
		//ěščřžýáíé ŠČŘŽÝÁÍÉ
		specialCharacters = {
			  "Ď": 210,
			  "ď": 212,
			  "ě": 216,
			  "Ě": 183,
			  "Ř": 252,
			  "ř": 253,
			  "Ý": 237,
			  "ý": 236,
			  "á": 160,
			  "Á": 181,
			  "Č": 172,
			  "č": 159,
			  "Š": 230,
			  "š": 231,
			  "Ž": 166,
			  "ž": 167,
			  "é": 130,
			  "Í": 214,
			  "í": 161,
			  "É": 144,
			  "Đ": 209,
			  "đ": 208,
			  "Ć": 143,
			  "ć": 134,
			  "ß": 225,
			  "ẞ": 225,
			  "ö": 148,
			  "Ö": 153,
			  "Ä": 142,
			  "ä": 132,
			  "ü": 129,
			  "Ü": 154,
			  "é": 130,
			  "ť": 156,
			  "ů": 133,
			  "ý": 236,
			  "Ň": 213,
			  "ň": 229,
			  "|": 0x09,
		};
		if (typeof text != 'undefined' && text && text != '')
		buff = text.toString();
		//console.log('AAAAA',buff.toString());
		//console.log('AAAAA',buff.length);
		
		var endBuff = null;
		for(var i=0; i<buff.length; i++){
			
			var value = buff[i];
			var tempBuff = new Buffer(value);
			for(var key in specialCharacters){
				if(value == key){
					//console.log(key,specialCharacters[key],tempBuff);
					
					//tempBuff = new Buffer([specialCharacters[key]]);
					tempBuff = new Buffer([specialCharacters[key]]);
					// console.log('string',tempBuff.toString());
					// console.log(specialCharacters[key],tempBuff,endBuff);
					// console.log('TMP',new Buffer([specialCharacters[key]]));
					break;
				} else {
				
					
				}
			}
			//console.log('TEMP BUFF',tempBuff);
			//console.log(endBuff);
			if(endBuff) {
				
				endBuff = Buffer.concat([endBuff,tempBuff]);
			} else {
				endBuff = tempBuff;
			}
				//endBuff = tempBuff
		}
		text_convert = endBuff;
		//console.log('con',text_convert);
		//this._queue.push( iconv.encode(text, 'cp1250') );
		//this._queue.push( iconv.encode(text, 'utf8') );
		if (text_convert)
		this._queue.push( text_convert );

		return this;

	},
	

	/**
	  * Add new line(s) to the printjob. 
	  *
	  * @param {number} [count=1] Number of new lines to be added to the current printjob.
	  */
	newLine: function (count) {

		// DEFAULTS
		count = count || 1;

		var buf = new Buffer([ cmds.CTL_CR, cmds.CTL_LF ]);
		for (var i = 0; i < count; i++) {
			if (buf)
			this._queue.push(buf);
		}

		return this;

	},
	
	newLine2: function (count) {

		// DEFAULTS
		count = count || 1;

		var buf = new Buffer([cmds.CTL_LF ]);
		for (var i = 0; i < count; i++) {
			if (buf)
			this._queue.push(buf);
		}

		return this;

	},

	pad: function (count) {

		// DEFAULTS
		count = count || 1;

		var buf = new Buffer([ 0x1b, 0x4a, 0xff, 0x1b, 0x4a, 0xff ]);
		for (var i = 0; i < count; i++) {
			if (buf)
			this._queue.push(buf);
		}

		return this;

	},

	/**
	  * Set text formatting for the current printjob.
	  *
	  * @param {string} [format='normal'] Text format (one of: 'normal', 'tall', 'wide')
	  */
	setTextFormat: function (format) {

		// DEFAULTS
		format = format.toLowerCase() || 'normal';

		var formats = {
			normal: 	cmds.TXT_NORMAL,
			tall: 		cmds.TXT_2HEIGHT,
			wide: 		cmds.TXT_2WIDTH
		};

		var cmd = formats[ format ];

		if (cmd) {
			var buf = new Buffer(cmd);
			if (buf)
			this._queue.push(buf);
		}
		else {
			throw new Error('Text format must be one of: ', Object.keys(formats).join(', '));
		}

		return this;

	},

	/**
	  * Set text alignment for the current printjob.
	  *
	  * @param {string} [count='left'] Text alignment (one of: 'left', 'center', 'right')
	  */
	setTextAlignment: function (align) {

		// DEFAULTS
		align = align.toLowerCase() || 'left';

		var aligns = {
			left: 		cmds.TXT_ALIGN_LT,
			center: 	cmds.TXT_ALIGN_CT,
			right: 		cmds.TXT_ALIGN_RT
		};

		var cmd = aligns[ align ];

		if (cmd) {
			var buf = new Buffer(cmd);
			if (buf)
			this._queue.push(buf);
		}
		else {
			throw new Error('Text alignment must be one of: ', Object.keys(aligns).join(', '));
		}

		return this;

	},

	/**
	  * Set underline for the current printjob.
	  *
	  * @param {boolean} [underline=true] Enables/disables underlined text
	  */
	setUnderline: function (underline) {

		// DEFAULTS
		if (typeof underline !== 'boolean') {
			underline = true;
		}

		var cmd = underline ? cmds.TXT_UNDERL_ON : cmds.TXT_UNDERL_OFF;
		var buf = new Buffer(cmd);
		if (buf)
		this._queue.push(buf);

		return this;

	},

	setTab: function(){
		
		var cmd = cmds.CTL_HT;
		var buf = new Buffer(cmd);
		if (buf)
		this._queue.push(buf);

		return this;
	},
	
	/**
	  * Set text bold for the current printjob.
	  *
	  * @param {boolean} [bold=true] Enables/disables bold text
	  */
	setBold: function (bold) {

		// DEFAULTS
		if (typeof bold !== 'boolean') {
			bold = false;
		}

		var cmd = bold ? cmds.TXT_BOLD_ON : cmds.TXT_BOLD_OFF;
		var buf = new Buffer(cmd);
		if (buf)
		this._queue.push(buf);

		return this;

	},
	qrimage: function(content, options, callback){
	  var self = this;
	  if(typeof options == 'function'){
		callback = options;
		options = null;
	  }
	  options = options || { type: 'png', mode: 'dhdw' };
	  var buffer = qr.imageSync(content, options);
	  var type = [ 'image', options.type ].join('/');
	  getPixels(buffer, type, function (err, pixels) {
		if(err) return callback && callback(err);
		self.raster(new Image(pixels), options.mode);
		callback && callback.call(self, null, self);
	  });
	  return this;
	},
	
	
	imageAdd: function(logo){
		
		//console.log(logo);
		//console.log('a',JSON.encode(logo));
		//var fs = require("fs");
		//var PNG = require('node-png').PNG;
		//var _this = this;
				// Get pixel rgba in 2D array
				  var pixels = [];
				  for (var i = 0; i < logo.height; i++) {
					var line = [];
					for (var j = 0; j < logo.width; j++) {
					  var idx = (logo.width * i + j) << 2;
					  line.push({
						r: logo.data[idx],
						g: logo.data[idx + 1],
						b: logo.data[idx + 2],
						a: logo.data[idx + 3]
					  });
					}
					pixels.push(line);
				}
				//console.log(pixels);
				var imageBuffer = new Buffer([]);
				for (var i = 0; i < logo.height; i++) {
						for (var j = 0; j < parseInt(logo.width/8); j++) {
						  var byte = 0x0;
						  for (var k = 0; k < 8; k++) {
							var pixel = pixels[i][j*8 + k];
							if(pixel.a > 126){ // checking transparency
							  grayscale = parseInt(0.2126 * pixel.r + 0.7152 * pixel.g + 0.0722 * pixel.b);

							  if(grayscale < 128){ // checking color
								var mask = 1 << 7-k; // setting bitwise mask
								byte |= mask; // setting the correct bit to 1
							  }
							}
						  }
						//console.log(byte);
						  imageBuffer = Buffer.concat([imageBuffer, new Buffer([byte])]);
						}
				}
				  // Print raster bit image
				  // GS v 0
				  // 1D 76 30	m	xL xH	yL yH d1...dk
				  // xL = (this.width >> 3) & 0xff;
				  // xH = 0x00;
				  // yL = this.height & 0xff;
				  // yH = (this.height >> 8) & 0xff;
				  // https://reference.epson-biz.com/modules/ref_escpos/index.php?content_id=94
				img = [];
				  //console.log(logo.height);
				  this._queue.push(new Buffer ([0x1d, 0x76, 0x30, 0x00]));
				  this._queue.push(new Buffer ([(logo.width >> 3) & 0xff]));
				  this._queue.push(new Buffer ([0x00]));
				  this._queue.push(new Buffer ([logo.height & 0xff]));
				  this._queue.push(new Buffer ([(logo.height >> 8) & 0xff]));

				// append data
				//console.log('img',imageBuffer);
				this._queue.push(imageBuffer);
				
				//console.log(this._queue);
				//console.log(img);
		
		return this;
	},
	
	/**
	 * [image description]
	 * @param  {[type]} image   [description]
	 * @param  {[type]} density [description]
	 * @return {[type]}         [description]
	 */
	image : function(image, density){
	  if(!(image instanceof Image)) 
		throw new TypeError('Only escpos.Image supported');
	  density = density || 'd24';
	  var n = !!~[ 'd8', 's8' ].indexOf(density) ? 1 : 3;
	  
	  //var header = _.BITMAP_FORMAT['BITMAP_' + density.toUpperCase()];
	  var header = cmds.S_RASTER_N;
	  var bitmap = image.toBitmap(n * 8);
	  var self = this;
	  bitmap.data.forEach(function (line) {
		
		this._queue.push(new Buffer( header));
		this._queue.push(new Buffer(line.length / n));
		this._queue.push(new Buffer(line));
		this._queue.push(new Buffer('_.EOL'));
		
		/*
		buf.write(header);
		buf.writeUInt16LE(line.length / n);
		buf.write(line);
		buf.write(_.EOL);
		*/
	  }.bind(this));
		//console.log(this.queue);
	  //console.log(buf);
	  // restore line spacing to default
	//this.newLine(1);
	 // return this.lineSpace();
	 return this;
	},

	/**
	  * Set text font for the current printjob.
	  *
	  * @param {string} [font='A'] Text font (one of: 'A', 'B')
	  */
	setEncode: function(){
		var ESC = '0x1b';
		//var cmd = [ESC, 'a', 18];
		var buf = new Buffer(cmds.ENCODE);
		this._queue.push(buf);
		//console.log('encode ',buf);
		return this; 
	},

	setFont: function (font) {

		// DEFAULTS
		font = font.toUpperCase() || 'A';

		var fonts = {
			A: 	cmds.TXT_NORMAL,
			B: 	cmds.TXT_2HEIGHT
		};

		var cmd = fonts[ font ];

		if (cmd) {
			var buf = new Buffer(cmd);
			this._queue.push(buf);
		}
		else {
			throw new Error('Font must be one of: ', Object.keys(fonts).join(', '));
		}

		return this;

	},

	separator: function () {

		var i = 0
		var line = ''
		var width = 42;
		while (i < width) {
			line += '-'
			i++
		}

		return this.text(line);

	},

	/**
	  * Cuts paper on the current printjob.
	  */
	cut: function () {

		var buf = new Buffer( cmds.PAPER_FULL_CUT );
		this._queue.push(buf);

		return this;

	},

	/**
	  * Kicks cash drawer on the current printjob.
	  * Default parameters are for Epson TM-88V from: http://keyhut.com/popopen.htm
	  *
	  * @param {number} [pin=2] Pin number used to send the pulse (one of: 2, 5)
	  * @param {number} [t1=110] Pulse ON time in ms (0 <= t1 <= 510)
	  * @param {number} [t2=242] Pulse OFF time in ms (0 <= t2 <= 510)
	  */
	cashdraw: function (pin, t1, t2) {

		// DEFAULTS
		pin = pin || 2;
		t1 = t1 || 110;
		t2 = t2 || 242;

		var buf = new Buffer(5);

		if (pin == 2) {
			new Buffer( cmds.CD_KICK_2 ).copy(buf);
		}
		else if (pin == 5) {
			new Buffer( cmds.CD_KICK_5 ).copy(buf);
		}
		else {
			throw new Error('Pin must be one of: 2, 5');
		}

		if (t1 >= 0 && t2 >= 0 && t1 <= 242 && t2 <= 242) {
			// Pulse ON/OFF times in 2ms increments
			buf.writeUInt8(t1/2, 3);
			buf.writeUInt8(t2/2, 4);
		}
		else {
			throw new Error('Pulse timings must be between 0 and 242 inclusive.');
		}

		this._queue.push(buf);

		return this;

	},

	printData: function () {
		// var init = new Buffer( cmds.HW_INIT ,"utf-8");
		var init = new Buffer( cmds.HW_INIT );
		
		var queue = this._queue.slice(0);	// Clone queue
		queue.unshift(init);				// Prepend init command
		//console.log(queue);
		// console.log(queue.toString("utf-8", 0, 12));
		//console.log('q',queue);
		//console.log('qq',queue.toString());
		if (queue.toString().length != ''){
			return Buffer.concat(queue );
			
		}

	}

}



module.exports = Printjob;



